﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CHAXWebApp.Models.DBContexts;
using CHAXWebApp.Models.DBTables;

namespace CHAXWebApp.Classes
{
    public class CHAX
    {
        public CHAX_Context ChaxCon() {
            CHAX_Context cc = new CHAX_Context();
            return cc;
        }

        public class DeleteFileAttribute : ActionFilterAttribute
        {
            public override void OnResultExecuted(ResultExecutedContext filterContext)
            {
                filterContext.HttpContext.Response.Flush();

                //convert the current filter context to file and get the file path
                string filePath = (filterContext.Result as FilePathResult).FileName;

                //delete the file after download
                System.IO.File.Delete(filePath);
            }
        }
    }
}