﻿using System.Web;
using System.Web.Optimization;

namespace CHAXWebApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js")); 

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-social.css",
                      "~/Content/font-awesome.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/material-dashboard/assets/css").Include(
                      "~/Content/material-dashboard/assets/css/bootstrap.min.css",
                      "~/Content/material-dashboard/assets/css/demo.css",
                      "~/Content/material-dashboard/assets/css/material-dashboard.css"));

            bundles.Add(new StyleBundle("~/Content/check-style/css").Include(
          "~/Content/check-style/Check.css"));

            bundles.Add(new ScriptBundle("~/Content/material-dashboard/assets/js").Include(
                      "~/Content/material-dashboard/assets/js/jquery-3.2.1.min.js",
                      "~/Scripts/DatePicker/jquery-ui.min.js",
                      "~/Content/material-dashboard/assets/js/bootstrap.min.js",
                      "~/Content/material-dashboard/assets/js/material.min.js",
                      "~/Content/material-dashboard/assets/js/chartist.min.js",
                      "~/Content/material-dashboard/assets/js/arrive.min.js",
                      "~/Content/material-dashboard/assets/js/perfect-scrollbar.jquery.min.js",
                      "~/Content/material-dashboard/assets/js/bootstrap-notify.js",
                      "~/Content/material-dashboard/assets/js/material-dashboard.js",
                      "~/Content/material-dashboard/assets/js/demo.js", 
                      "~/Scripts/jquery.unobtrusive-ajax.js"));
            
            bundles.Add(new ScriptBundle("~/Content/bootstrap-login/assets/js").Include(
                      "~/Content/bootstrap-login/assets/bootstrap/js/bootstrap.min.js",
                      "~/Content/bootstrap-login/assets/js/jquery-1.11.1.min.js",
                      "~/Content/bootstrap-login/assets/js/jquery.backstretch.min.js",
                      "~/Content/bootstrap-login/assets/js/scripts.js"));

            bundles.Add(new ScriptBundle("~/Scripts/js").Include(
                      "~/Scripts/Chax.js",
                      "~/Scripts/print.min.js"
                      ));

            bundles.Add(new ScriptBundle("~/Content/datatable-jquery/js").Include(
                      "~/Content/datatable-jquery/datatables.min.js"));

            bundles.Add(new StyleBundle("~/Content/datatable-jquery/css").Include(
                      "~/Content/datatable-jquery/datatables.css",
                      "~/Content/datepicker/jquery-ui.css"));

        }
        
    }
}
