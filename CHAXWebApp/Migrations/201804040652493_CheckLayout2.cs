namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CheckLayout2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CheckLayout",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LayoutDescription = c.String(),
                        LayoutCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CheckLayout");
        }
    }
}
