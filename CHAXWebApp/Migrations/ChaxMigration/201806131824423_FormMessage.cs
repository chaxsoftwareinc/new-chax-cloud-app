namespace CHAXWebApp.Migrations.ChaxMigration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FormMessage : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountRelation",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserEmail = c.String(),
                        InvitedEmail = c.String(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Bank",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BankName = c.String(),
                        Branch = c.String(),
                        BankAddress = c.String(),
                        BankCityStateZip = c.String(),
                        RoutingNumber = c.String(),
                        TransitSymbol = c.String(),
                        FileDate = c.String(),
                        Type = c.String(),
                        BranchCode = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CheckLayout",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LayoutDescription = c.String(),
                        LayoutCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.FormMessage",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Payee",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PhoneNumber = c.String(),
                        Address = c.String(),
                        AddressLine1 = c.String(),
                        AddressLine2 = c.String(),
                        CityState = c.String(),
                        ZipCode = c.String(),
                        Default = c.String(),
                        RoutingNumber = c.String(),
                        BankAccount = c.String(),
                        SendingCompanyName = c.String(),
                        DiscretionaryData = c.String(),
                        ACHClassEntry = c.String(),
                        EntryDescription = c.String(),
                        CRoutingNumberCheckDigit = c.Int(nullable: false),
                        BatchNumber = c.Int(nullable: false),
                        DefaultACHClass = c.String(),
                        ClientID = c.Int(nullable: false),
                        IsDefaultPayee = c.Boolean(nullable: false),
                        CompanyID = c.Int(nullable: false),
                        LastDateUsed = c.DateTime(),
                        UserEmail = c.String(),
                        EmailAddress = c.String(),
                        Memo = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Payer",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                        AddressLine1 = c.String(),
                        AddressLine2 = c.String(),
                        ZipCode = c.String(),
                        PhoneNumber = c.String(),
                        BankAccount = c.String(),
                        BankID = c.Int(nullable: false),
                        TransitNumber = c.String(),
                        LastDate = c.DateTime(),
                        LastCheck = c.String(),
                        LastAmount = c.Double(nullable: false),
                        FrequencyType = c.String(),
                        FrequencyDays = c.Int(nullable: false),
                        AccountType = c.String(),
                        ACHRoutingNumber = c.String(),
                        ACHAccountNumber = c.String(),
                        CameFromACH = c.String(),
                        ClientID = c.Int(nullable: false),
                        CityState = c.String(),
                        CompanyID = c.Int(nullable: false),
                        LastDateUsed = c.DateTime(),
                        UserEmail = c.String(),
                        EmailAddress = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PrintedCheck",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CheckNumber = c.String(),
                        CheckLayout = c.String(),
                        PayerName = c.String(),
                        PayerAddress = c.String(),
                        PayerAddressLine1 = c.String(),
                        PayerAddressLine2 = c.String(),
                        CityState = c.String(),
                        ZipCode = c.String(),
                        PayerPhone = c.String(),
                        BankName = c.String(),
                        RoutingNumber = c.String(),
                        AccountNumber = c.String(),
                        Payee = c.String(),
                        Date = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        Amount = c.String(),
                        AmountInWords = c.String(),
                        Memo = c.String(),
                        PayeeName = c.String(),
                        IsMemorized = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        BankAddress = c.String(),
                        BankCityState = c.String(),
                        BatchCount = c.Int(nullable: false),
                        UserEmail = c.String(),
                        IsPrinted = c.Boolean(nullable: false),
                        PayerEmail = c.String(),
                        PayeePhone = c.String(),
                        IsGenerated = c.Boolean(nullable: false),
                        IsMarkedOk = c.Boolean(nullable: false),
                        IsMarkedBanned = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PrintedCheck");
            DropTable("dbo.Payer");
            DropTable("dbo.Payee");
            DropTable("dbo.FormMessage");
            DropTable("dbo.CheckLayout");
            DropTable("dbo.Bank");
            DropTable("dbo.AccountRelation");
        }
    }
}
