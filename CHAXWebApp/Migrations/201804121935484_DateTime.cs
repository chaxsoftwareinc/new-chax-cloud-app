namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrintedCheck", "DateCreated", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PrintedCheck", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PrintedCheck", "Date", c => c.DateTime());
            DropColumn("dbo.PrintedCheck", "DateCreated");
        }
    }
}
