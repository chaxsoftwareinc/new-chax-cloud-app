namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Bank : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bank",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BankName = c.String(),
                        Branch = c.String(),
                        Address = c.String(),
                        CityStateZip = c.String(),
                        RoutingNumber = c.String(),
                        TransitSymbol = c.String(),
                        FileDate = c.String(),
                        Type = c.String(),
                        BranchCode = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Bank");
        }
    }
}
