namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class payeepayer03062018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Payee", "CityState", c => c.String());
            AddColumn("dbo.Payer", "AddressLine1", c => c.String());
            AddColumn("dbo.Payer", "AddressLine2", c => c.String());
            AddColumn("dbo.Payer", "ZipCode", c => c.String());
            DropColumn("dbo.Payee", "City");
            DropColumn("dbo.Payee", "State");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Payee", "State", c => c.String());
            AddColumn("dbo.Payee", "City", c => c.String());
            DropColumn("dbo.Payer", "ZipCode");
            DropColumn("dbo.Payer", "AddressLine2");
            DropColumn("dbo.Payer", "AddressLine1");
            DropColumn("dbo.Payee", "CityState");
        }
    }
}
