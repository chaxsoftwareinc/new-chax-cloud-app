namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserEmail2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Payee", "UserEmail", c => c.String());
            AddColumn("dbo.Payer", "UserEmail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Payer", "UserEmail");
            DropColumn("dbo.Payee", "UserEmail");
        }
    }
}
