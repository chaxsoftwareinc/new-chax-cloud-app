namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PrintedChecks : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PrintedCheck",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CheckNumber = c.String(),
                        CheckLayout = c.String(),
                        PayerName = c.String(),
                        PayerAddress = c.String(),
                        PayerAddressLine1 = c.String(),
                        PayerAddressLine2 = c.String(),
                        CityState = c.String(),
                        ZipCode = c.String(),
                        PayerPhone = c.String(),
                        BankName = c.String(),
                        RoutingNumber = c.String(),
                        AccountNumber = c.String(),
                        Payee = c.String(),
                        Date = c.String(),
                        Amount = c.String(),
                        AmountInWords = c.String(),
                        Memo = c.String(),
                        IsMemorized = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PrintedCheck");
        }
    }
}
