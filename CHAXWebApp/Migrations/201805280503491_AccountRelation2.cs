namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccountRelation2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountRelation", "UserEmail", c => c.String());
            AddColumn("dbo.AccountRelation", "InvitedEmail", c => c.String());
            DropColumn("dbo.AccountRelation", "User");
            DropColumn("dbo.AccountRelation", "Invited");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AccountRelation", "Invited", c => c.String());
            AddColumn("dbo.AccountRelation", "User", c => c.String());
            DropColumn("dbo.AccountRelation", "InvitedEmail");
            DropColumn("dbo.AccountRelation", "UserEmail");
        }
    }
}
