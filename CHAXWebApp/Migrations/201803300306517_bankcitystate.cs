namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class bankcitystate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrintedCheck", "BankCityState", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrintedCheck", "BankCityState");
        }
    }
}
