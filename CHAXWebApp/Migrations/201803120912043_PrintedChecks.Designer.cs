// <auto-generated />
namespace CHAXWebApp.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class PrintedChecks : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(PrintedChecks));
        
        string IMigrationMetadata.Id
        {
            get { return "201803120912043_PrintedChecks"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
