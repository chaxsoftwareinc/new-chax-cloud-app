namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PayerEmail2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrintedCheck", "PayerEmail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrintedCheck", "PayerEmail");
        }
    }
}
