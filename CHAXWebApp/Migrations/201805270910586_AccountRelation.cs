namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccountRelation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountRelation",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        User = c.String(),
                        Invited = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AccountRelation");
        }
    }
}
