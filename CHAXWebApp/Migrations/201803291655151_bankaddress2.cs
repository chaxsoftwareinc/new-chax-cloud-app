namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class bankaddress2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrintedCheck", "BankAddress", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrintedCheck", "BankAddress");
        }
    }
}
