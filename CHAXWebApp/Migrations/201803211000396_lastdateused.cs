namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lastdateused : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Payee", "LastDateUsed", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Payee", "LastDateUsed");
        }
    }
}
