namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PrintedChecksDate1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PrintedCheck", "Date", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PrintedCheck", "Date", c => c.DateTime(nullable: false));
        }
    }
}
