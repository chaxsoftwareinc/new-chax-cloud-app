namespace CHAXWebApp.Migrations.UsersMigration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccessLevel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "UserLevel", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "ParentUserName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "ParentUserName");
            DropColumn("dbo.AspNetUsers", "UserLevel");
        }
    }
}
