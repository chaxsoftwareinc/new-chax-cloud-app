namespace CHAXWebApp.Migrations.UsersMigration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UsersAlignment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "CheckHor", c => c.Single(nullable: false));
            AddColumn("dbo.AspNetUsers", "CheckVer", c => c.Single(nullable: false));
            AddColumn("dbo.AspNetUsers", "MICRHor", c => c.Single(nullable: false));
            AddColumn("dbo.AspNetUsers", "MICRVer", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "MICRVer");
            DropColumn("dbo.AspNetUsers", "MICRHor");
            DropColumn("dbo.AspNetUsers", "CheckVer");
            DropColumn("dbo.AspNetUsers", "CheckHor");
        }
    }
}
