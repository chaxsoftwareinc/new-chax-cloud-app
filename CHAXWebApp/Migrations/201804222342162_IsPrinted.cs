namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsPrinted : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrintedCheck", "IsPrinted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrintedCheck", "IsPrinted");
        }
    }
}
