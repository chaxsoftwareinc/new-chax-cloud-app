namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BatchCount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrintedCheck", "BatchCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrintedCheck", "BatchCount");
        }
    }
}
