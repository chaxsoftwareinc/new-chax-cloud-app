namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lastdateusedpayer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Payer", "LastDateUsed", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Payer", "LastDateUsed");
        }
    }
}
