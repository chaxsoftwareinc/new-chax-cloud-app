// <auto-generated />
namespace CHAXWebApp.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Premium : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Premium));
        
        string IMigrationMetadata.Id
        {
            get { return "201804300156509_Premium"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
