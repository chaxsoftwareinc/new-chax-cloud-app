namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserEmail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrintedCheck", "UserEmail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrintedCheck", "UserEmail");
        }
    }
}
