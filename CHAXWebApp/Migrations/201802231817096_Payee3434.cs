namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Payee3434 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Payee", "AddressLine1", c => c.String());
            AddColumn("dbo.Payee", "AddressLine2", c => c.String());
            AddColumn("dbo.Payee", "City", c => c.String());
            AddColumn("dbo.Payee", "State", c => c.String());
            AddColumn("dbo.Payee", "ZipCode", c => c.String());
            DropColumn("dbo.Payee", "CityState");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Payee", "CityState", c => c.String());
            DropColumn("dbo.Payee", "ZipCode");
            DropColumn("dbo.Payee", "State");
            DropColumn("dbo.Payee", "City");
            DropColumn("dbo.Payee", "AddressLine2");
            DropColumn("dbo.Payee", "AddressLine1");
        }
    }
}
