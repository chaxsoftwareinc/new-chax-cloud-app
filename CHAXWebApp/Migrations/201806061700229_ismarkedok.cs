namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ismarkedok : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrintedCheck", "IsMarkedOk", c => c.Boolean(nullable: false));
            AddColumn("dbo.PrintedCheck", "IsMarkedBanned", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrintedCheck", "IsMarkedBanned");
            DropColumn("dbo.PrintedCheck", "IsMarkedOk");
        }
    }
}
