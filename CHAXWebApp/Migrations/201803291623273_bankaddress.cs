namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class bankaddress : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bank", "BankAddress", c => c.String());
            AddColumn("dbo.Bank", "BankCityStateZip", c => c.String());
            DropColumn("dbo.Bank", "Address");
            DropColumn("dbo.Bank", "CityStateZip");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Bank", "CityStateZip", c => c.String());
            AddColumn("dbo.Bank", "Address", c => c.String());
            DropColumn("dbo.Bank", "BankCityStateZip");
            DropColumn("dbo.Bank", "BankAddress");
        }
    }
}
