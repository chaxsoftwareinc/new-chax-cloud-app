namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PrintedChecksPayeeName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrintedCheck", "PayeeName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrintedCheck", "PayeeName");
        }
    }
}
