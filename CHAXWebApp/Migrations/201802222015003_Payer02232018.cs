namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Payer02232018 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Payer", "BankID", c => c.Int(nullable: false));
            DropColumn("dbo.Payer", "BankName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Payer", "BankName", c => c.String());
            DropColumn("dbo.Payer", "BankID");
        }
    }
}
