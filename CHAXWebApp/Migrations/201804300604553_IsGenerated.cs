namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsGenerated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrintedCheck", "IsGenerated", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrintedCheck", "IsGenerated");
        }
    }
}
