namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Payee : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Payee", "Default", c => c.String());
            AddColumn("dbo.Payee", "RoutingNumber", c => c.String());
            AddColumn("dbo.Payee", "BankAccount", c => c.String());
            AddColumn("dbo.Payee", "SendingCompanyName", c => c.String());
            AddColumn("dbo.Payee", "DiscretionaryData", c => c.String());
            AddColumn("dbo.Payee", "ACHClassEntry", c => c.String());
            AddColumn("dbo.Payee", "EntryDescription", c => c.String());
            AddColumn("dbo.Payee", "CRoutingNumberCheckDigit", c => c.Int(nullable: false));
            AddColumn("dbo.Payee", "BatchNumber", c => c.Int(nullable: false));
            AddColumn("dbo.Payee", "DefaultACHClass", c => c.String());
            AddColumn("dbo.Payee", "ClientID", c => c.Int(nullable: false));
            AddColumn("dbo.Payee", "CityState", c => c.String());
            AddColumn("dbo.Payee", "IsDefaultPayee", c => c.Boolean(nullable: false));
            AddColumn("dbo.Payee", "CompanyID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Payee", "CompanyID");
            DropColumn("dbo.Payee", "IsDefaultPayee");
            DropColumn("dbo.Payee", "CityState");
            DropColumn("dbo.Payee", "ClientID");
            DropColumn("dbo.Payee", "DefaultACHClass");
            DropColumn("dbo.Payee", "BatchNumber");
            DropColumn("dbo.Payee", "CRoutingNumberCheckDigit");
            DropColumn("dbo.Payee", "EntryDescription");
            DropColumn("dbo.Payee", "ACHClassEntry");
            DropColumn("dbo.Payee", "DiscretionaryData");
            DropColumn("dbo.Payee", "SendingCompanyName");
            DropColumn("dbo.Payee", "BankAccount");
            DropColumn("dbo.Payee", "RoutingNumber");
            DropColumn("dbo.Payee", "Default");
        }
    }
}
