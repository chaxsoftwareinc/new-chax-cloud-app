namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PrintedChecksDate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PrintedCheck", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PrintedCheck", "Date", c => c.String());
        }
    }
}
