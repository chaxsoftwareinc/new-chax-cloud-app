namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class payeephone : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PrintedCheck", "PayeePhone", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PrintedCheck", "PayeePhone");
        }
    }
}
