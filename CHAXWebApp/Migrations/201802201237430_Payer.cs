namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Payer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Payer",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                        PhoneNumber = c.String(),
                        BankAccount = c.String(),
                        BankName = c.String(),
                        TransitNumber = c.String(),
                        LastDate = c.DateTime(),
                        LastCheck = c.String(),
                        LastAmount = c.Double(nullable: false),
                        FrequencyType = c.String(),
                        FrequencyDays = c.Int(nullable: false),
                        AccountType = c.String(),
                        ACHRoutingNumber = c.String(),
                        ACHAccountNumber = c.String(),
                        CameFromACH = c.String(),
                        ClientID = c.Int(nullable: false),
                        CityState = c.String(),
                        CompanyID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Payer");
        }
    }
}
