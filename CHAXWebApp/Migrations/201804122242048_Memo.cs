namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Memo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Payee", "Memo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Payee", "Memo");
        }
    }
}
