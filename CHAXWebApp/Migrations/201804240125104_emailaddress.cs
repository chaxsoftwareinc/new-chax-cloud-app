namespace CHAXWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class emailaddress : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Payee", "EmailAddress", c => c.String());
            AddColumn("dbo.Payer", "EmailAddress", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Payer", "EmailAddress");
            DropColumn("dbo.Payee", "EmailAddress");
        }
    }
}
