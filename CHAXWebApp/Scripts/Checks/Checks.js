﻿var $j = jQuery.noConflict();

$(document).ready(function () {
	triggerActive("liChecks"); //setting the class active for sidebar
	$("#inLayout").val('Middle');

	$("input[name='optradio']").on("click", function () {
		$("#inLayout").val($("input:checked").val());
	});

	forPayer();
	forPayee();
	getMessage();

	$("textarea")
		.on('keypress', function (event) {
			var textarea = $(this),
				text = textarea.val(),
				numberOfLines = (text.match(/\n/g) || []).length + 1,
				maxRows = parseInt(textarea.attr('rows'));

			if (event.which === 13 && numberOfLines === maxRows) {
				return false;
			}
		});
});

$(function () {
	var input = document.getElementById('inAmountInDigits');
	var output = document.getElementById('inAmountInWords');
	output.value = 'Please enter amount in check.';
	$("#divAmount").removeClass("is-empty");
	input.onkeyup = function SetMaxLength(e) {
		var val = this.value;
		if (val.length === 0) {
			output.value = 'Please enter amount in check.';
			$("#divAmountInWords").removeClass("is-empty");
			return;
		}
		val1 = parseInt(val, 36);
		if (isNaN(val1)) {
			output.value = 'Invalid input.';
			$("#divAmountInWords").removeClass("is-empty");
			return;
		}
		if (e.keyCode === 190) {
			input.maxLength = 10;
			return;
		}
		output.value = ConvertToWords(val);
		$("#divAmountInWords").removeClass("is-empty");
	};
});

function ConvertToHundreds(num, i) {
	aTens = ["Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"];
	aOnes = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine",
		"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen",
		"Nineteen"];
	var cNum, nNum;
	var cWords = "";

	num %= 1000;
	if (num > 99) {
		/* Hundreds. */
		cNum = String(num);
		nNum = Number(cNum.charAt(0));
		cWords += aOnes[nNum] + " Hundred";
		num %= 100;
		if (num > 0) {
			if (i > 0) {
				cWords += " and ";
			}
			else {
				cWords += " ";
			}
		}
	}

	if (num > 19) {
		/* Tens. */
		cNum = String(num);
		nNum = Number(cNum.charAt(0));
		cWords += aTens[nNum - 2];
		num %= 10;
		if (num > 0)
			cWords += "-";
	}

	if (num > 0) {
		/* Ones and teens. */
		nNum = Math.floor(num);
		cWords += aOnes[nNum];
	}

	return cWords;
}

function ConvertToWords(num) {
	var aUnits = ["Thousand", "Million", "Billion", "Trillion", "Quadrillion"];
	var cWords = "";
	var nLeft = Math.floor(num);
	for (var i = 0; nLeft > 0; i++) {
		if (nLeft % 1000 > 0) {
			var str = cWords;
			cWords = str.replace("Dollars", "");
			if (i !== 0)
				cWords = ConvertToHundreds(nLeft, i) + " " + aUnits[i - 1] + " " + cWords + "Dollars";
			else
				cWords = ConvertToHundreds(nLeft, i) + " " + cWords + "Dollars";
		}
		nLeft = Math.floor(nLeft / 1000);
	}
	num = Math.round(num * 100) % 100;
	if (num > 0)
		cWords += " and " + num + "/100 Cents";

	var pos = cWords.lastIndexOf(" and ");


	return cWords;
}

$("#btnSelectPayer").click(function () {
	loadTablePayer();
});

$("#btnSelectPayee").click(function () {
	loadTablePayee();
});

function loadTablePayer() {
	if ($.fn.DataTable.isDataTable("#tblPayer")) {
		$('#tblPayer').DataTable().clear().destroy();
	}

	var t = $('#tblPayer').DataTable({
		"autoWidth": false,
		destroy: true,
		"ajax": {
			"url": "/Checks/GetAllPayer",
			"dataSrc": ""
		},
		"columnDefs": [
			{ "visible": true, "title": "NAME", "targets": 0 },
			{ "visible": true, "title": "ADDRESS", "targets": 1 },
			{ "visible": true, "title": "PHONE NUMBER", "targets": 2 },
			{ "visible": true, "title": "BANK NAME", "targets": 3 },
			{ "visible": true, "title": "ACCOUNT NUMBER", "targets": 4 },
			{ "visible": true, "title": "", "targets": 5 }
		],
		"columns": [
			{ "data": "Name" },
			{
				"render": function (data, type, full, meta) {
					return full.AddressLine1 + " " + full.AddressLine2 + " " + full.CityState + " " + full.ZipCode;
				}
			},
			{ "data": "PhoneNumber" },
			{ "data": "BankName" },
			{
				"render": function (data, type, full, meta) {
					return full.RoutingNumber + full.BankAccount;
				}
			},
			{
				"render": function (data, type, full, meta) {
					name = full.Name;
					accountnumber = full.BankAccount;
					bankname = full.BankName;
					routing = full.RoutingNumber;
					add1 = full.AddressLine1;
					add2 = full.AddressLine2;
					city = full.CityState;
					zip = full.ZipCode;
					phone = full.PhoneNumber;
					bankadd = full.BankAddress;
					bankcity = full.BankCityStateZip;
					bankid = full.BankID;
					address = full.PayerAddress;
					email = full.EmailAddress;
					var ID = full.ID;
					return '<button type="submit" id="btnChoosePayer' + ID + '" class="btn btn-sm btn-white" onclick="populateFieldsPayer(\'' + name + '\', \'' + accountnumber + '\', \'' + bankname + '\', \'' + routing + '\', \'' + add1 + '\', \'' + add2 + '\', \'' + city + '\', \'' + zip + '\', \'' + phone + '\', \'' + bankadd + '\', \'' + bankcity + '\', ' + bankid + ', ' + ID + ', \'' + email + '\')"><i class="fa fa-check-circle-o"></i> Choose</button>';
				}
			}
		],
		"initComplete": function (settings, json) {
			$(this).removeClass("dataTable");
		}
	});
}

function loadTablePayee() {
	if ($.fn.DataTable.isDataTable("#tblPayee")) {
		$('#tblPayee').DataTable().clear().destroy();
	}
	var ti = $('#tblPayee').DataTable({
		"autoWidth": false,
		"ajax": {
			"url": "/Checks/GetAllPayee",
			"dataSrc": ""
		},
		"columnDefs": [
			{ "visible": false, "targets": 0 },
			{ "visible": true, "title": "NAME", "targets": 1 },
			{ "visible": true, "title": "ADDRESS", "targets": 2 },
			{ "visible": true, "title": "PHONE NUMBER", "targets": 3 },
			{ "visible": true, "title": "", "targets": 4 }
		],
		"columns": [
			{ "data": "ID" },
			{ "data": "Name" },
			{
				"render": function (data, type, full, meta) {
					return full.AddressLine1 + " " + full.AddressLine2 + " " + full.CityState + " " + full.ZipCode;
				}
			},
			{ "data": "PhoneNumber" },
			{
				"render": function (data, type, full, meta) {
					var ID = full.ID;
					var name = full.Name;
					var line1 = full.AddressLine1;
					var line2 = full.AddressLine2;
					var city = full.CityState;
					var zip = full.ZipCode;
					var phone = full.PhoneNumber;
					var memo = full.Memo;
					var address = full.Address;
					return '<button type="submit" id="btnChoosePayee' + ID + '" class="btn btn-sm btn-white" onclick="populateFieldsPayee(\'' + name + '\', \'' + line1 + '\', \'' + line2 + '\', \'' + city + '\', \'' + zip + '\', \'' + phone + '\', \'' + memo + '\', ' + ID + ')"><i class="fa fa-check-circle-o"></i> Choose</button>';
				}
			}
		],
		"initComplete": function (settings, json) {
			$(this).removeClass("dataTable");
		}
	});
}

function populateFieldsPayer(name, accountnumber, bankname, routing, add1, add2, city, zip, phone, bankaddress, bankcitystatezip, bankid, id, email) {
	$('#inPayerName').val(name);
	$('#divPayerName').removeClass("is-empty");
	$('#inAccountNumber').val(accountnumber);
	$('#divAccountNumber').removeClass("is-empty");
	$('#inBankName').val(bankname);
	$('#divBankName').removeClass("is-empty");
	$('#taBankAddress').val(bankaddress + "\n" + bankcitystatezip);
	$('#inBankAddressLine1').val(bankaddress);
	$('#inBankAddressLine2').val(bankcitystatezip);
	$('#divBankAddress').removeClass("is-empty");
	$('#inRoutingNumber').val(routing);
	$('#divRoutingNumber').removeClass("is-empty");
	$('#inPayerEmail').val(email);
	$('#divPayerEmail').removeClass("is-empty");
	$('#inAddress').val(add1 + "\n" + add2 + "\n" + city + "\n" + zip);
	$('#divAddress').removeClass("is-empty");
	$('#inPayerPhone').val(phone);
	$('#divPayerPhone').removeClass("is-empty");
	$('#inBankID').val(bankid);
	$('#inPayerID').val(id);
	$("#btnCancelPayer").click();
	removeLinesPayer();
}

function populateFieldsPayee(name, line1, line2, city, zip, phone, memo, id) {

	$('#taDrawnToPayTo').val(line1 + "\n" + line2 + "\n" + city + "\n" + zip);
	$('#divDrawnToPayTo').removeClass("is-empty");
	$('#inPayeeName').val(name);
	$('#divPayeeName').removeClass("is-empty");
	$('#inPayeeID').val(id);

	if (memo === null || memo === 'null' || memo === '') {
		memo = "";
		$('#inMemo').val(memo);
		$('#divMemo').addClass("is-empty");
	} else {
		$('#inMemo').val(memo);
		$('#divMemo').removeClass("is-empty");
	}

	if (phone === null || phone === 'null' || phone === '') {
		phone = "";
		$('#inPayeePhone').val(phone);
		$('#divPayeePhone').addClass("is-empty");
	} else {
		$('#inPayeePhone').val(phone);
		$('#divPayeePhone').removeClass("is-empty");
	}

	$("#btnCancelPayee").click();
	removeLinesPayee();
}

function getBank(id) {
	$.ajax({
		url: '/Dashboard/GetBank',
		type: 'POST',
		data: { 'id': id },
		dataType: 'json',
		success: function (a) {
			for (i = 0; i < a.length; i++) {
				$('#inBankName').val(a[i].BankName);
				$('#divBankName').removeClass("is-empty");
				$('#inRoutingNumber').val(a[i].RoutingNumber);
				$('#divRoutingNumber').removeClass("is-empty");
				bankid = a[i].BankID;
			}
		}
	}).done(function () {
		$("#btnCancelPayer").click();
	});
}
$('#btnCreate').click(function () {
	$("#inProcessType").val("Save");
	validateData();
});


$('#btnDownload').click(function () {

});

$('#btnPreview').click(function () {

});

$('#btnSubmit').click(function () {

});

function validateData() {
	var strDrawn = $("#taDrawnToPayTo").val();
	var intDrawn = strDrawn.split('\n').length;
	var strPayerAddress = $("#inAddress").val();
	var intPayerAddress = strPayerAddress.split('\n').length;
	//addSpacesPayee();
	//addSpacesPayer();
	if (intDrawn > 6) {
		alert("The payee name and address max number of lines is five.");
	} else if (intPayerAddress > 5) {
		alert("The payer address max number of lines is five.");
	} else if ($("#inPayeeName").val().trim() === "") {
		//moveToFirstLine();
		alert("Payee is required.");
	} else if ($("#inAddress").val().trim() === "") {
		moveToFirstLine();
		alert("Payer is required.");
	} else {
		$('#btnSubmit').click();
	}
}

//function getFirstLine() {
//	var lines = "";
//	var xstr = "";
//	if ($('taDrawnToPayTo').val() !== undefined) {
//		if ($('taDrawnToPayTo').val().length > 1) {
//			lines = $('taDrawnToPayTo').val().split('\n');
//			for (var i = 0; i < lines.length; i++) {
//				//code here using lines[i] which will give you each line
//				if (i === 0) {
//					xstr = lines[i];
//				}
//			}
//		} else {
//			lines = $('taDrawnToPayTo').val();
//			xstr = lines;
//		}
//		$('#inHiddenPayeeName').val(xstr);
//	}

//}

function validateRouting() {
	if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);
}

function moveToFirstLine() {
	if ($("#inAddress").val().trim() === "") {
		$("#inAddress").val("");
	}
	if ($("#taDrawnToPayTo").val().trim() === "") {
		$("#taDrawnToPayTo").val("");
	}
}

function addSpacesPayee() {
	var str = $("#taDrawnToPayTo").val().trim();
	var xint = str.split('\n').length;
	var xint2 = str.match('').length;
	if (xint < 5) {
		var xx = 5 - xint;
		for (x = 0; x < xx; x++) {
			str = str + "\n ";
		}
	}
	$("#taDrawnToPayTo").val(str);
}

function addSpacesPayer() {
	var strrep = $("#inAddress").val().replace(" ", "");
	var str = $("#inAddress").val().trim();
	var xint = str.split('\n').length;
	if (xint < 5) {
		var xx = 5 - xint;
		for (x = 0; x < xx; x++) {
			str = str + "\n ";
		}
	}
	$("#inAddress").val(str);
}

function save() {
	var layout = $("#inLayout").val();
	var cnumber = $("#inCheckNumber").val();
	var viewmodel = {};
	$.ajax({
		url: '/Check/PreviewCheck',
		type: 'POST',
		data: { cnumber: cnumber },
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Downloading a check...");
		},
		success: function (a) {
			for (i = 0; i < a.length; i++) {
				var xstrdate = a[i].Date;
				viewmodel = {
					CheckNumber: a[i].CheckNumber,
					PayerName: a[i].PayerName,
					PayerAddress: a[i].PayerAddress,
					PayerPhone: a[i].PayerPhone,
					BankName: a[i].BankName,
					BankAddress: a[i].BankAddress,
					BankCityStateZip: a[i].BankCityState,
					RoutingNumber: a[i].RoutingNumber,
					AccountNumber: a[i].AccountNumber,
					Amount: a[i].Amount,
					AmountInWords: a[i].AmountInWords,
					Date: timeConverter(xstrdate),
					PayeeName: a[i].PayeeName,
					PayeeAddress: a[i].Payee,
					Payee: a[i].Payee,
					Memo: a[i].Memo,
					BatchCount: a[i].BatchCount,
					CheckLayout: layout,
					ProcessType: 'Save Dashboard'
				};
			}
		}
	}).done(function () {
		var ci = viewmodel;
		$.ajax({
			url: '/Check/Pdf',
			type: 'POST',
			data: ci,
			dataType: 'json',
			complete: function () {
				$('#divSpinner').addClass("hidden");
			},
			success: function (a) {

			}
		}).done(function (a) {
			window.location.href = "/Check/Download?file=" + a;
		});

	});
}

function preview() {
	var layout = $("#inLayout").val();
	var cnumber = $("#inCheckNumber").val();
	var viewmodel = {};
	$.ajax({
		url: '/Check/PreviewCheck',
		type: 'POST',
		data: { cnumber: cnumber },
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Previewing a check...");
		},
		success: function (a) {
			for (i = 0; i < a.length; i++) {
				var xstrdate = a[i].Date;
				viewmodel = {
					CheckNumber: a[i].CheckNumber,
					PayerName: a[i].PayerName,
					PayerAddress: a[i].PayerAddress,
					PayerPhone: a[i].PayerPhone,
					BankName: a[i].BankName,
					BankAddress: a[i].BankAddress,
					BankCityStateZip: a[i].BankCityState,
					RoutingNumber: a[i].RoutingNumber,
					AccountNumber: a[i].AccountNumber,
					Amount: a[i].Amount,
					AmountInWords: a[i].AmountInWords,
					Date: timeConverter(xstrdate),
					PayeeName: a[i].PayeeName,
					PayeeAddress: a[i].Payee,
					Payee: a[i].Payee,
					Memo: a[i].Memo,
					BatchCount: a[i].BatchCount,
					CheckLayout: layout,
					ProcessType: 'Preview Dashboard'
				};
			}
		}
	}).done(function () {
		var ci = viewmodel;
		$.ajax({
			url: '/Check/Pdf',
			type: 'POST',
			data: ci,
			dataType: 'json',
			complete: function () {
				$('#divSpinner').addClass("hidden");
			},
			success: function (a) {

			}
		}).done(function (a) {
			window.open("/Check/Preview?file=" + a);
		});

	});
}

function timeConverter(createdAt) {
	var d = new Date(parseInt(createdAt.substr(6)));
	return d.toLocaleDateString("en-US");
}

function loadBank() {
	if ($.fn.DataTable.isDataTable("#tblBankChecks")) {
		$('#tblBankChecks').DataTable().clear().destroy();
	}

	var t = $('#tblBankChecks').DataTable({
		//"processing": true,
		//dom: 'Bfrtip',	
		"ajax": {
			"url": "/Checks/GetAllBank",
			"dataSrc": ""
		},
		autoWidth: false,
		"order": [[1, "asc"]],
		"columnDefs": [
			{ "visible": false, "targets": 0 },
			{ "visible": true, "title": "BANK NAME", "targets": 1 },
			{ "visible": true, "title": "ADDRESS", "targets": 2 },
			{ "visible": true, "title": "CITY/STATE/ZIPCODE", "targets": 3 },
			{ "visible": true, "title": "ROUTING NUMBER", "targets": 4 },
			{ "visible": true, "title": "SELECT", "targets": 5 }
		],
		"columns": [
			{ "data": "ID" },
			{ "data": "BankName" },
			{ "data": "BankAddress" },
			{ "data": "BankCityStateZip" },
			{ "data": "RoutingNumber" },
			{
				"render": function (data, type, full, meta) {
					var ID = full.ID;
					var bank = full.BankName;
					var bankaddress = full.BankAddress;
					var bankcitystate = full.BankCityStateZip;
					var routing = full.RoutingNumber;
					return '<button type="button" rel="tooltip" title="Choose" class="btn btn-info btn-xs btn-simple" id="btnChoose' + ID + '" onclick="selectBank(\'' + bank + '\', \'' + bankaddress + '\', \'' + bankcitystate + '\', \'' + routing + '\')">' +
						'<i class="material-icons">check_circle</i>' +
						'</button>';
				}
			}
		]
	});
}

function selectBank(bank, bankaddress, bankcitystate, routing) {
	$('#inBankName').val(bank);
	$('#taBankAddress').val(bankaddress + "\n" + bankcitystate);
	$('#inBankAddressLine1').val(bankaddress);
	$('#inBankAddressLine2').val(bankcitystate);
	$('#inRoutingNumber').val(routing);



	if (bank === "" || bank === null) {
		$("#divBankName").addClass("is-empty");
	} else {
		$("#divBankName").removeClass("is-empty");
	}


	if (bankaddress === "" || bankaddress === null) {
		$("#divBankAddress").addClass("is-empty");
	} else {
		$("#divBankAddress").removeClass("is-empty");
	}

	if (routing === "" || routing === null) {
		$("#divRoutingNumber").addClass("is-empty");
	} else {
		$("#divRoutingNumber").removeClass("is-empty");
	}

	$('#btnCloseBank').click();
}

$('input:radio[name="optradio"]').change(
	function () {
		if ($(this).is(':checked') && $(this).val() === 'Batch') {
			$('#inBatchCount').prop("disabled", false);
		} else {
			$('#inBatchCount').val("2");
			$('#divCheckCount').removeClass("is-empty");
			$('#inBatchCount').prop("disabled", true);
		}
	});


function GetJsonResult(data) {
	//alert("yow");
	if (data.result === "2") {
		demo.showNotificationDashboard('top', 'center', 'Check successfully created.', 2);
		savePayer();
		savePayee();
	} else if (data.result === "true") {
		demo.showNotificationDashboard('top', 'center', 'The check number is already used with this account number. Please delete the existing to use the checknumber.', 3);
	}
	//else {
	//	demo.showNotificationDashboard('top', 'center', data.result, 4);
	//}
}

function getSpecificBank() {
	var xrouting = $("#inRoutingNumber").val();
	if (xrouting.length === 9) {
		var xstr = "";
		$.ajax({
			url: '/Checks/GetSpecificBank',
			type: 'POST',
			data: { routing: xrouting },
			beforeSend: function () {
				$('#divSpinner').removeClass("hidden");
				$('#lblLoading').html("Retreiving Bank information...");
			},
			complete: function () {
				$('#divSpinner').addClass("hidden");
			},
			dataType: 'json',
			success: function (a) {

			}
		}).done(function (a) {
			var xint = a.length;
			if (xint === 0) {
				var bank2 = $("#inBankName").val("");
				var bankaddress2 = "";
				var bankcitystate2 = "";
				var bankid2 = "1";
				$("#divBankName").addClass("is-empty");
				$('#taBankAddress').val(bankaddress2 + "\n" + bankcitystate2);
				$('#inBankAddressLine1').val(bankaddress2);
				$('#inBankAddressLine2').val(bankcitystate2);
				$('#inBankID').val(bankid2);
				demo.showNotification('top', 'center', 'Invalid Routing Number!', 4);
			} else {
				for (i = 0; i < a.length; i++) {
					var bank = $("#inBankName").val(a[i].BankName);
					var bankaddress = a[i].BankAddress;
					var bankcitystate = a[i].BankCityStateZip;
					var bankid = a[i].ID;
					if (bank === "" || bank === null) {
						$("#divBankName").addClass("is-empty");
					} else {
						$("#divBankName").removeClass("is-empty");
					}
					$('#inBankID').val(bankid);
					$('#taBankAddress').val(bankaddress + "\n" + bankcitystate);
					$('#inBankAddressLine1').val(bankaddress);
					$('#inBankAddressLine2').val(bankcitystate);
				}
			}
		});
	}

}

function onBeginAJAX() {
	$('#divSpinner').removeClass("hidden");
	$('#lblLoading').html("Creating check...");
	$('#btnCreate').prop("disabled", true);
}

function onCompleteAJAX(xhr) {
	$('#divSpinner').addClass("hidden");
	var json = JSON.parse(xhr.responseText);
	var yourData = json.result; // or json["Data"]
	if (yourData === "2") {
		clearPayee();
		clearPayer();
		$('#btnCreate').prop("disabled", false);
	}

}

$(".readonly1").on('keydown paste', function (e) {
	e.preventDefault();
});

function clearPayer() {
	$("#inCheckNumber").val("");
	$("#inPayerName").val("");
	$("#inAddress").val("");
	$("#inPayerEmail").val("");
	$("#inAccountNumber").val("");
	$("#inRoutingNumber").val("");
	$("#inBankName").val("");
	$("#inAmountInDigits").val("");
	$("#inAmountInWords").val("");
	$("#inDate").val("");
	$("#inPayerID").val("0");
	$("#inPayerPhone").val("");


	$("#divCheckNumber").addClass("is-empty");
	$("#divPayerName").addClass("is-empty");
	$("#divAddress").addClass("is-empty");
	$("#divPayerEmail").addClass("is-empty");
	$("#divAccountNumber").addClass("is-empty");
	$("#divRoutingNumber").addClass("is-empty");
	$("#divBankName").addClass("is-empty");
	$("#divAmountInDigits").addClass("is-empty");
	$("#divDate").addClass("is-empty");
	$("#inPayerPhone").addClass("is-empty");
}

function clearPayee() {
	$("#inPayeeName").val("");
	$("#taDrawnToPayTo").val("");
	$("#inMemo").val("");
	$("#inPayeeID").val("0");
	$("#inPayeePhone").val("");

	$("#divPayeeName").addClass("is-empty");
	$("#divDrawnToPayTo").addClass("is-empty");
	$("#divMemo").addClass("is-empty");
	$("#inPayeePhone").addClass("is-empty");
}

function savePayer() {
	var ID = $('#inPayerID').val();
	var name = $('#inPayerName').val();
	var address1 = "";
	var address2 = "";
	var city = "";
	var postal = "";
	var phone = $('#inPayerPhone').val();
	var address = $('#inAddress').val().trim();
	var email = $('#inPayerEmail').val();

	var xint = $('#inAddress').val().trim().split('\n').length;
	if (xint > 1) {
		lines = $('#inAddress').val().split('\n');
		address1 = lines[0];
		address2 = lines[1];
		city = lines[2];
		postal = lines[3];
	} else {
		lines = $('#inAddress').val().trim();
		address = lines;
		address1 = "";
		address2 = "";
		city = "";
		postal = "";
	}

	var bankid = $('#inBankID').val();
	var accountno = $('#inAccountNumber').val();
	var bankname = $('#inBankName').val();
	if (bankname === "") { bankid = 1; }
	$.ajax({
		url: "/Payer/UpdatePayer",
		type: 'POST',
		data: { 'id': ID, 'name': name, 'line1': address1, 'line2': address2, 'city': city, 'zip': postal, 'phone': phone, 'bankid': bankid, 'accountnumber': accountno, 'address': address, 'emailadd': email },
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {

	});

}

function savePayee() {
	var ID = $('#inPayeeID').val();
	var name = $('#inPayeeName').val();
	var address1 = "";
	var address2 = "";
	var city = "";
	var postal = "";
	var phone = $('#inPayeePhone').val();
	var memo = $('#inMemo').val();
	var address = $('#inAddress').val().trim();

	var xint = $('#taDrawnToPayTo').val().trim().split('\n').length;
	if (xint > 1) {
		lines = $('#taDrawnToPayTo').val().split('\n');
		address1 = lines[0];
		address2 = lines[1];
		city = lines[2];
		postal = lines[3];
	} else {
		lines = $('#taDrawnToPayTo').val().trim();
		address = lines;
		address1 = "";
		address2 = "";
		city = "";
		postal = "";
	}

	$.ajax({
		url: "/Payee/UpdatePayeeViaCheck",
		type: 'POST',
		data: { 'id': ID, 'name': name, 'line1': address1, 'line2': address2, 'city': city, 'zip': postal, 'phone': phone, 'memo': memo, 'address': address },
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {

	});

}


function pad(str, max) {
	str = str.toString();
	return str.length < max ? pad("0" + str, max) : str;
}

function handler(e) {
	alert(e.target.value);
	e.target.value = "2012-02-02";
	e.type = "text";
}

$(function () {
	$j("#inDate").datepicker({
		dateFormat: 'mm-dd-yy',
		minDate: new Date(2017, 1 - 1, 2)
	});
});

$('#inAccountNumber').on('input', function () {
	$(this).val($(this).val().replace(/[^0-9 \-]/, ''));
});

function showAddressModal() {
	triggerRadio();
	$("#btnShowAddressModal").click();
}

function showAddressModalPayee() {
	$("#btnShowAddressModalPayee").click();
}

$('#frmAddress input').on('change', function () {
	triggerRadio();
});

$('#frmAddressPayee input').on('change', function () {
	triggerRadioPayee();
});


function triggerRadio() {
	var format = $('input[name=addressformat]:checked').val();
	disableFields();
	if (format === "1") {
		$("#divAddressLine1").addClass("required");
		$("#inAddressLine1").prop("required", true);
		$("#inAddressLine1").prop("disabled", false);
		$("#divCity").addClass("required");
		$("#inCity").prop("required", true);
		$("#inCity").prop("disabled", false);
		$("#divZIPCode").addClass("required");
		$("#inZIPCode").prop("required", true);
		$("#inZIPCode").prop("disabled", false);
		$("#inPhoneNumber").prop("disabled", false);
	} else if (format === "2") {
		$("#divAddressLine1").addClass("required");
		$("#inAddressLine1").prop("required", true);
		$("#inAddressLine1").prop("disabled", false);
		$("#divAddressLine2").addClass("required");
		$("#inAddressLine2").prop("required", true);
		$("#inAddressLine2").prop("disabled", false);
		$("#divCity").addClass("required");
		$("#inCity").prop("required", true);
		$("#inCity").prop("disabled", false);
		$("#divZIPCode").addClass("required");
		$("#inZIPCode").prop("required", true);
		$("#inZIPCode").prop("disabled", false);
		$("#inPhoneNumber").prop("disabled", false);
	} else if (format === "3") {
		$("#divAddressLine1").addClass("required");
		$("#inAddressLine1").prop("required", true);
		$("#inAddressLine1").prop("disabled", false);
		$("#divAddressLine2").addClass("required");
		$("#inAddressLine2").prop("required", true);
		$("#inAddressLine2").prop("disabled", false);
		$("#divCity").addClass("required");
		$("#inCity").prop("required", true);
		$("#inCity").prop("disabled", false);
		$("#divZIPCode").addClass("required");
		$("#inZIPCode").prop("required", true);
		$("#inZIPCode").prop("disabled", false);
		$("#inPhoneNumber").prop("disabled", false);
	}
}
function triggerRadioPayee() {
	var format = $('input[name=addressformatPayee]:checked').val();
	disableFields();
	if (format === "1") {
		$("#divAddressLine1Payee").addClass("required");
		$("#inAddressLine1Payee").prop("required", true);
		$("#inAddressLine1Payee").prop("disabled", false);
		$("#divCityPayee").addClass("required");
		$("#inCityPayee").prop("required", true);
		$("#inCityPayee").prop("disabled", false);
		$("#divZIPCodePayee").addClass("required");
		$("#inZIPCodePayee").prop("required", true);
		$("#inZIPCodePayee").prop("disabled", false);
		$("#inPhoneNumberPayee").prop("disabled", false);
	} else if (format === "2") {
		$("#divAddressLine1Payee").addClass("required");
		$("#inAddressLine1Payee").prop("required", true);
		$("#inAddressLine1Payee").prop("disabled", false);
		$("#divAddressLine2Payee").addClass("required");
		$("#inAddressLine2Payee").prop("required", true);
		$("#inAddressLine2Payee").prop("disabled", false);
		$("#divCityPayee").addClass("required");
		$("#inCityPayee").prop("required", true);
		$("#inCityPayee").prop("disabled", false);
		$("#divZIPCodePayee").addClass("required");
		$("#inZIPCodePayee").prop("required", true);
		$("#inZIPCodePayee").prop("disabled", false);
		$("#inPhoneNumberPayee").prop("disabled", false);
	} else if (format === "3") {
		$("#divAddressLine1Payee").addClass("required");
		$("#inAddressLine1Payee").prop("required", true);
		$("#inAddressLine1Payee").prop("disabled", false);
		$("#divAddressLine2Payee").addClass("required");
		$("#inAddressLine2Payee").prop("required", true);
		$("#inAddressLine2Payee").prop("disabled", false);
		$("#divCityPayee").addClass("required");
		$("#inCityPayee").prop("required", true);
		$("#inCityPayee").prop("disabled", false);
		$("#divZIPCodePayee").addClass("required");
		$("#inZIPCodePayee").prop("required", true);
		$("#inZIPCodePayee").prop("disabled", false);
		$("#inPhoneNumberPayee").prop("disabled", false);
	}
}

function disableFields() {
	$(".formataddress").prop("disabled", true);
	$(".formataddress").prop("required", false);
	$(".formataddressdiv").removeClass("required");
}

function disableFieldsPayee() {
	$(".formataddressPayee").prop("disabled", true);
	$(".formataddressPayee").prop("required", false);
	$(".formataddressdivPayee").removeClass("required");
}

$("#frmAddress").submit(function (event) {
	fillPayer();
	event.preventDefault();
});

$("#frmAddressPayee").submit(function (event) {
	fillPayee();
	event.preventDefault();
});

function fillPayer() {
	var format = $('input[name=addressformat]:checked').val();
	if (format === "1") {
		add1 = $("#inAddressLine1").val();
		city = $("#inCity").val();
		zip = $("#inZIPCode").val();
		phone = $("#inPhoneNumber").val(); 
		$("#inPayerLine1").val(add1);
		$("#inPayerLine2").val("");
		$("#inPayerCity").val(city);
		$("#inPayerZip").val(zip);
		$("#inPayerPhone").val(phone);

		$("#inAddress").val(add1 + "\n" + city + "\n" + zip + "\n" + phone);
		$("#divAddress").removeClass("is-empty");
		$("#btnCancel").click();
	} else if (format === "2") {
		add1 = $("#inAddressLine1").val();
		add2 = $("#inAddressLine2").val();
		city = $("#inCity").val();
		zip = $("#inZIPCode").val();
		phone = $("#inPhoneNumber").val();
		$("#inPayerLine1").val(add1);
		$("#inPayerLine2").val(add2);
		$("#inPayerCity").val(city);
		$("#inPayerZip").val(zip);
		$("#inPayerPhone").val(phone);

		$("#inAddress").val(add1 + "\n" + add2 + "\n" + city + " " + zip + "\n" + phone);
		$("#divAddress").removeClass("is-empty");
		$("#btnCancel").click();
	} else if (format === "3") {
		add1 = $("#inAddressLine1").val();
		add2 = $("#inAddressLine2").val();
		city = $("#inCity").val();
		zip = $("#inZIPCode").val();
		phone = $("#inPhoneNumber").val();
		$("#inPayerLine1").val(add1);
		$("#inPayerLine2").val(add2);
		$("#inPayerCity").val(city);
		$("#inPayerZip").val(zip);
		$("#inPayerPhone").val(phone);

		$("#inAddress").val(add1 + "\n" + add2 + "\n" + city + "\n" + zip + "\n" + phone);
		$("#divAddress").removeClass("is-empty");
		$("#btnCancel").click();
	} else if (format === "4") {
		$("#inPayerLine1").val("");
		$("#inPayerLine2").val("");
		$("#inPayerCity").val("");
		$("#inPayerZip").val("");
		$("#inPayerPhone").val("");

		$("#inAddress").val("");
		$("#divAddress").addClass("is-empty");
		$("#btnCancel").click();
	}

}

function fillPayee() {
	var format = $('input[name=addressformatPayee]:checked').val();
	if (format === "1") {
		add1 = $("#inAddressLine1Payee").val();
		city = $("#inCityPayee").val();
		zip = $("#inZIPCodePayee").val();
		phone = $("#inPhoneNumberPayee").val();
		$("#inPayeeLine1").val(add1);
		$("#inPayeeLine2").val("");
		$("#inPayeeCity").val(city);
		$("#inPayeeZip").val(zip);
		$("#inPayeePhone").val(phone);

		$("#taDrawnToPayTo").val(add1 + "\n" + city + "\n" + zip + "\n" + phone);
		$("#divDrawnToPayTo").removeClass("is-empty");
		$("#btnCancelPayee2").click();
	} else if (format === "2") {
		add1 = $("#inAddressLine1Payee").val();
		add2 = $("#inAddressLine2Payee").val();
		city = $("#inCityPayee").val();
		zip = $("#inZIPCodePayee").val();
		phone = $("#inPhoneNumberPayee").val();
		$("#inPayeeLine1").val(add1);
		$("#inPayeeLine2").val(add2);
		$("#inPayeeCity").val(city);
		$("#inPayeeZip").val(zip);
		$("#inPayeePhone").val(phone);

		$("#taDrawnToPayTo").val(add1 + "\n" + add2 + "\n" + city + " " + zip + "\n" + phone);
		$("#divDrawnToPayTo").removeClass("is-empty");
		$("#btnCancelPayee2").click();
	} else if (format === "3") {
		add1 = $("#inAddressLine1Payee").val();
		add2 = $("#inAddressLine2Payee").val();
		city = $("#inCityPayee").val();
		zip = $("#inZIPCodePayee").val();
		phone = $("#inPhoneNumberPayee").val();
		$("#inPayeeLine1").val(add1);
		$("#inPayeeLine2").val(add2);
		$("#inPayeeCity").val(city);
		$("#inPayeeZip").val(zip);
		$("#inPayeePhone").val(phone);

		$("#taDrawnToPayTo").val(add1 + "\n" + add2 + "\n" + city + "\n" + zip + "\n" + phone);
		$("#divDrawnToPayTo").removeClass("is-empty");
		$("#btnCancelPayee2").click();
	} else if (format === "4") {
		$("#inPayeeLine1").val("");
		$("#inPayeeLine2").val("");
		$("#inPayeeCity").val("");
		$("#inPayeeZip").val("");
		$("#inPayeePhone").val("");

		$("#taDrawnToPayTo").val("");
		$("#divDrawnToPayTo").addClass("is-empty");
		$("#btnCancelPayee2").click();
	}

}

function forPayer() {
	$('#inAddress').focusout(function () {
		var avalue = $('#inAddress').val().trim();
		var newVal = avalue.replace(/^\s*[\r\n]/gm, '');
		//var finalResults = newVal.replace("\n", "");
		$('#inAddress').val(newVal);
	});
}

function forPayee() {
	$('#taDrawnToPayTo').focusout(function () {
		var avalue = $('#taDrawnToPayTo').val().trim();
		var newVal = avalue.replace(/^\s*[\r\n]/gm, '');
		//var finalResults = newVal.replace("\n", "");
		$('#taDrawnToPayTo').val(newVal);
	});
}

function removeLinesPayer() {
	var avalue = $('#inAddress').val().trim();
	var newVal = avalue.replace(/^\s*[\r\n]/gm, '');
	//var finalResults = newVal.replace("\n", "");
	$('#inAddress').val(newVal);
}

function removeLinesPayee() {
	var avalue = $('#taDrawnToPayTo').val().trim();
	var newVal = avalue.replace(/^\s*[\r\n]/gm, '');
	//var finalResults = newVal.replace("\n", "");
	$('#taDrawnToPayTo').val(newVal);
}

function getMessage() {
	var xstr = "";
	var msg = $("#taMessage").val();
	$.ajax({
		url: '/Check/GetMessage',
		type: 'POST',
		dataType: 'json',
		data: { 'message': msg },
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden"); 
			$('#lblLoading').html("Retrieving message...");
			$('#spanMessage').empty(); 
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {

		}
	}).done(function (a) {
		$("#spanMessage").html(a);
	});
}

