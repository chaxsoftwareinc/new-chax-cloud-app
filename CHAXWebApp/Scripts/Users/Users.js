﻿var $j = jQuery.noConflict();

$(document).ready(function () {
	triggerActive("liUsers"); //setting the class active for sidebar
	loadAllUsers();
});

function loadAllUsers() {
	var xstr = "";
	$.ajax({
		url: '/Users/GetAllUsers',
		type: 'POST',
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Loading all users...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {
			$("#tblUsersBody").empty();
			for (i = 0; i < a.length; i++) {
				xstr = xstr + '<tr>' +
					'<td>' + a[i].UserFirstName + '</td>' +
					'<td>' + a[i].UserLastName + '</td>' +
					'<td>' + a[i].UserName + '</td>' +
					'<td>Adminitrator</td>' +
					'</tr>';
			}
		}
	}).done(function () {
		$("#tblUsersBody").append(xstr);
	});
}

function loadAllUsersAll() {
	var xstr = "";
	$.ajax({
		url: '/Users/GetAllUsersAll',
		type: 'POST',
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Loading all Users...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {
			$("#tblAllUsersBody").empty();
			for (i = 0; i < a.length; i++) {
				xstr = xstr + '<tr>' +
					'<td>' + a[i].UserFirstName + '</td>' +
					'<td>' + a[i].UserLastName + '</td>' +
					'<td>' + a[i].UserName + '</td>' +
					'<td>Adminitrator</td>' +
					'<td><button data-toggle="modal" data-target="#exampleModalLongChangeRights" class="btn btn-sm btn-white" title="Click to change rights"><i class="fa fa-window-close"></i> NO ACCESS</button></td>' +
					'</tr>';
			}
		}
	}).done(function () {
		$("#tblAllUsersBody").append(xstr);
	});
}

function sendEmail() {
	emailto = $("#inEmail").val();
	username = $("#inUserName").val();
	$.ajax({
		url: '/Users/SendAccess',
		type: 'POST',
		dataType: 'json',
		data: { 'emailto': emailto, 'username': username, 'password': 'Password@1234' },
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Sending access...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
			loadAllUsers();
		},
		success: function (a) {

		}
	}).done(function (a) {
		if (a === "1") {
			demo.showNotification('top', 'center', 'User successfully added!', 2);
		} else {
			demo.showNotification('top', 'center', 'A problem occured while adding a user!', 4);
		}
	});
}

function addSubUser() {
	emailto = $("#inEmail").val();
	username = $("#inUserName").val();
	$.ajax({
		url: '/Account/AddSubUser',
		type: 'POST',
		dataType: 'json',
		data: { 'username': username, 'Password': 'Password@1234', 'ConfirmPassword': 'Password@1234', 'Email': emailto, 'FirstName': 'First', 'LastName': 'Last' },
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Adding sub user...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {

		}
	}).done(function (a) {
		if (a === "1") {
			sendEmail();
		} else {
			demo.showNotification('top', 'center', 'A problem occured while adding a user!', 4);
		}
	});
}

function returnStatus(str, useremail) {
	if (str === 0) {
		return "Pending";
	} else if (str === 1) {
		return "Accepted";
	} else if (str === 2) {
		return '<button type="submit" id="btnSendInvite" class="btn btn-sm btn-white" onclick="inviteUsers(\'' + useremail + '\');"><i class="fa fa-send-o"></i> Send Invite</button>';
	} else if (str === 3) {
		return "You can't invite yourself";
	}

}

function invitationstatus(str) {
	if (str === 0) {
		return "Pending";
	} else if (str === 1) {
		return "Accepted";
	} else {
		return "Not Defined";
	}
}

$("#frmSendInvite").submit(function (event) {
	//sendEmail();
	addSubUser();
	event.preventDefault();
});


function inviteUsers(invitedemail) {
	var xstr = "";
	$.ajax({
		url: '/Users/AddUsers',
		type: 'POST',
		dataType: 'json',
		data: { 'invitedemail': invitedemail },
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Inviting user...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {

		}
	}).done(function (a) {
		if (a === "1") {
			demo.showNotification('top', 'center', 'Invite successfully sent!', 2);
			loadAllUsers();
			$("#btnCancelSendInvites").click();
		} else if (a === "0") {
			demo.showNotification('top', 'center', 'You have already invited this user!', 3);
			loadAllUsers();
		} else {
			demo.showNotification('top', 'center', a, 4);
			loadAllUsers();
		}
	});
}

function loadAllRequest() {
	var xstr = "";
	$.ajax({
		url: '/Users/GetAllRequests',
		type: 'POST',
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Loading all Requests...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {
			$("#tblAllRequestsBody").empty();
			for (i = 0; i < a.length; i++) {
				xstr = xstr + '<tr>' +
					'<td>' + a[i].UserFirstName + '</td>' +
					'<td>' + a[i].UserLastName + '</td>' +
					'<td>' + a[i].UserEmail + '</td>' +
					'<td><button type="submit" id="accept' + a[i].UserEmail + '" class="btn btn-sm btn-white" onclick="acceptRequest(\'' + a[i].UserEmail + '\')"><i class="fa fa-level-up"></i> Accept</button></td>' +
					'</tr>';
			}
		}
	}).done(function () {
		$("#tblAllRequestsBody").append(xstr);
	});
}

function acceptRequest(useremail) {
	var xstr = "";
	$.ajax({
		url: '/Users/AcceptRequests',
		type: 'POST',
		dataType: 'json',
		data: { 'useremail': useremail },
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Accepting Request...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {

		}
	}).done(function (a) {
		if (a === "1") {
			demo.showNotification('top', 'center', 'Request accepted!', 2);
			loadAllUsers();
			$("#btnCancelAllRequests").click();
		} else {
			demo.showNotification('top', 'center', a, 4);
			loadAllUsers();
		}
	});
}

function emptyTable() {
	$("#tblSearchEmailBody").empty();
	$("#tblSearchEmailBody").append('<td colspan="4" style="width:100%">No results found.</td>');
}