﻿var $j = jQuery.noConflict();

$(document).ready(function () {
	triggerActive("liPayer"); //setting the class active for sidebar; //setting the class active for sidebar

	//loadTable();	
	loadPayer();

	$("textarea")
		.on('keypress', function (event) {
			var textarea = $(this),
				text = textarea.val(),
				numberOfLines = (text.match(/\n/g) || []).length + 1,
				maxRows = parseInt(textarea.attr('rows'));

			if (event.which === 13 && numberOfLines === maxRows) {
				return false;
			}
		});

	$('#payerFullAddress').focusout(function () {
		var avalue = $('#payerFullAddress').val().trim();
		var newVal = avalue.replace(/^\s*[\r\n]/gm, '');
		//var finalResults = newVal.replace("\n", "");
		$('#payerFullAddress').val(newVal);
	});
});

function loadTable() {
	if ($.fn.DataTable.isDataTable("#tblPayer")) {
		$('#tblPayer').DataTable().clear().destroy();
	}

	var t = $('#tblPayer').DataTable({
		dom: 'Bfrtip',
		"ajax": {
			"url": "/Payer/GetAllPayer",
			"dataSrc": ""
		},
		autoWidth: false,
		"order": [[4, "desc"]],
		"columnDefs": [
			{ "visible": false, "targets": 0 },
			{ "visible": true, "title": "NAME", "targets": 1 },
			{ "visible": true, "title": "ADDRESS", "targets": 2 },
			{ "visible": true, "title": "PHONE NUMBER", "targets": 3 },
			{ "visible": true, "title": "LAST DATE USED", "targets": 4 },
			{ "visible": true, "title": "", "targets": 5 }
		],
		"columns": [
			{ "data": "ID" },
			{ "data": "Name" },
			{
				"render": function (data, type, full, meta) {
					return full.AddressLine1 + " " + full.AddressLine2 + " " + full.CityState + " " + full.ZipCode;
				}
			},
			{ "data": "PhoneNumber" },
			{
				"render": function (data, type, full, meta) {
					var d = full.LastDateUsed;
					return timeConverter(d);
				}
			},
			{
				"render": function (data, type, full, meta) {
					var ID = full.ID;
					return '<button type="button" rel="tooltip" title="Edit" class="btn btn-info btn-xs btn-simple" id="btnEdit' + ID + '" onclick="getSelectedPayer(' + ID + ', this.id)" data-toggle="modal" data-target="#exampleModalLong">' +
						'<i class="material-icons">edit</i>' +
						'</button>' +
						'<button type="button" rel="tooltip" title="Delete" class="btn btn-danger btn-xs btn-simple" id="btnDelete' + ID + '" onclick="getSelectedPayer(' + ID + ', this.id)" data-toggle="modal" data-target="#exampleModalLong">' +
						'<i class="material-icons">delete</i>' +
						'</button>';
				}
			}
		],
		"initComplete": function (settings, json) {
			$(this).removeClass("dataTable");
		},
		"buttons": [
			{
				"extend": 'copy',
				"text": '<i class="fa fa-edit"></i> Copy',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				"extend": 'excel',
				"text": '<i class="fa fa-file-excel-o"></i> Excel',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				"extend": 'csv',
				"text": '<i class="fa fa-database"></i> CSV',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				"extend": 'pdf',
				"text": '<i class="fa fa-file-excel-o"></i> PDF',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				"extend": 'print',
				"text": '<i class="fa fa-print"></i> Print',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			}
		]
		//buttons: [
		//	{
		//		text: 'Add Payer',
		//		className: 'btn',
		//		action: function (e, dt, node, config) {
		//			$('#payerID').val('0');
		//			$('#payerName').val('');
		//			$('#divName').addClass('is-empty');
		//			$('#payerAddressLine1').val('');
		//			$('#divAddressLine1').addClass('is-empty');
		//			$('#payerAddressLine2').val('');
		//			$('#divAddressLine2').addClass('is-empty');
		//			$('#payerCityState').val('');
		//			$('#divCityState').addClass('is-empty');
		//			$('#payerPostalCode').val('');
		//			$('#divPostalCode').addClass('is-empty');
		//			$('#payerPhone').val('');
		//			$('#divPhone').addClass('is-empty');
		//			$('#payerAccountNumber').val('');
		//			$('#divAccountNumber').addClass('is-empty');
		//			$('#payerBankID').val('');
		//			$('#payerBankName').val('');
		//			$('#divBankName').addClass('is-empty');
		//			$('#payerRoutingNumber').val('');
		//			$('#divRoutingNumber').addClass('is-empty');
		//			$('#payerEmail').val('');
		//			$('#divEmail').addClass('is-empty');
		//			$('#payerAddress').val('');
		//			$('#divAddress').addClass('is-empty');
		//			$("#btnAddPayer").click();
		//			$("#h4PayerModal").text("Add Payer");
		//			$("#h4SubPayerModal").text("Enter Payer's information");
		//			$("#btnSave1").text("Add");
		//		}
		//	}
		//]
	});
}

function loadPayer() {
	var xstr = "";
	$.ajax({
		url: '/Payer/GetTopPayer',
		type: 'POST',
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Loading all Payers...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {
			$("#tblPayerBody").empty();
			for (i = 0; i < a.length; i++) {
				var xstrdate = a[i].LastDateUsed;
				var amount = parseFloat(a[i].Amount).toFixed(2);
				xstr = xstr + '<tr>' +
					'<td>' + a[i].Name + '</td>' +
					'<td>' + a[i].AddressLine1 + " " + a[i].AddressLine2 + " " + a[i].CityState + " " + a[i].ZipCode + '</td>' +
					'<td>' + a[i].PhoneNumber + '</td>' +
					'<td>' + timeConverter(xstrdate) + '</td>' +
					'</tr>';
			}
		}
	}).done(function () {
		$("#tblPayerBody").append(xstr);
	});
}

function removeLines() {
	var avalue = $('#payerFullAddress').val().trim();
	var newVal = avalue.replace(/^\s*[\r\n]/gm, '');
	//var finalResults = newVal.replace("\n", "");
	$('#payerFullAddress').val(newVal);
}

function getSelectedPayer(id, type) {

	if (type.includes('Edit')) {
		$("#h4PayerModal").text("Edit Payer");
		$("#h4SubPayerModal").text("Edit Payer's information");
		$("#btnSave1").text("Update");
	} else if (type.includes('Delete')) {
		$("#h4PayerModal").text("Delete Payer");
		$("#h4SubPayerModal").text("You sure you want to delete the selected payer?");
		$("#btnSave1").text("Delete");
	}

	$.ajax({
		url: '/Payer/GetSpecificPayer',
		type: 'POST',
		data: { 'id': id },
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {
		for (i = 0; i < a.length; i++) {
			$('#payerID').val(a[i].ID);
			$('#payerName').val(a[i].Name);
			$('#payerAddressLine1').val(a[i].AddressLine1);
			$('#payerAddressLine2').val(a[i].AddressLine2);
			$('#payerCityState').val(a[i].CityState);
			$('#payerPostalCode').val(a[i].ZipCode);
			$('#payerPhone').val(a[i].PhoneNumber);
			$('#payerAccountNumber').val(a[i].BankAccount);
			$('#payerBankID').val(a[i].BankID);
			$('#payerBankName').val(a[i].BankName);
			$('#payerRoutingNumber').val(a[i].RoutingNumber);
			$('#payerAddress').val(a[i].PayerAddress);
			$('#payerEmail').val(a[i].EmailAddress);
			$('#payerFullAddress').val(a[i].AddressLine1 + "\n" + a[i].AddressLine2 + "\n" + a[i].CityState + "\n" + a[i].ZipCode);

			$("#divFullAddress").removeClass("is-empty");

			if (a[i].Name === "" || a[i].Name === null) {
				$("#divName").addClass("is-empty");
			} else {
				$("#divName").removeClass("is-empty");
			}
			if (a[i].AddressLine1 === "" || a[i].AddressLine1 === null) {
				$("#divAddressLine1").addClass("is-empty");
			} else {
				$("#divAddressLine1").removeClass("is-empty");
			}
			if (a[i].AddressLine2 === "" || a[i].AddressLine2 === null) {
				$("#divAddressLine2").addClass("is-empty");
			} else {
				$("#divAddressLine2").removeClass("is-empty");
			}
			if (a[i].CityState === "" || a[i].CityState === null) {
				$("#divCityState").addClass("is-empty");
			} else {
				$("#divCityState").removeClass("is-empty");
			}
			if (a[i].ZipCode === "" || a[i].ZipCode === null) {
				$("#divPostalCode").addClass("is-empty");
			} else {
				$("#divPostalCode").removeClass("is-empty");
			}
			if (a[i].PhoneNumber === "" || a[i].PhoneNumber === null) {
				$("#divPhone").addClass("is-empty");
			} else {
				$("#divPhone").removeClass("is-empty");
			}
			if (a[i].BankAccount === "" || a[i].BankAccount === null) {
				$("#divAccountNumber").addClass("is-empty");
			} else {
				$("#divAccountNumber").removeClass("is-empty");
			}
			if (a[i].BankName === "" || a[i].BankName === null) {
				$("#divBankName").addClass("is-empty");
			} else {
				$("#divBankName").removeClass("is-empty");
			}
			if (a[i].RoutingNumber === "" || a[i].RoutingNumber === null) {
				$("#divRoutingNumber").addClass("is-empty");
			} else {
				$("#divRoutingNumber").removeClass("is-empty");
			}
			if (a[i].PayerAddress === "" || a[i].PayerAddress === null) {
				$("#divAddress").addClass("is-empty");
			} else {
				$("#divAddress").removeClass("is-empty");
			}
			if (a[i].EmailAddress === "" || a[i].EmailAddress === null) {
				$("#divEmail").addClass("is-empty");
			} else {
				$("#divEmail").removeClass("is-empty");
			}
		}
		removeLines();
	});
}

$("#frmInfo").submit(function (event) {
	confirmSave();
	event.preventDefault();
});

function confirmSave() {
	var ID = $('#payerID').val();
	var name = $('#payerName').val();
	//var address1 = $('#payerAddressLine1').val(); 
	//var address2 = $('#payerAddressLine2').val();
	//var city = $('#payerCityState').val();
	//var postal = $('#payerPostalCode').val();
	var address1 = "";
	var address2 = "";
	var city = "";
	var postal = "";
	var phone = $('#payerPhone').val();
	var bankid = $('#payerBankID').val();
	var accountno = $('#payerAccountNumber').val();
	var bankname = $('#payerBankName').val();
	var address = "";
	var emailadd = $('#payerEmail').val();
	if (bankname === "") { bankid = 1; }
	var dat = "";
	var url = "";
	var proc = $('#btnSave1').text();

	var xint = $('#payerFullAddress').val().trim().split('\n').length;
	if (xint > 0) {
		lines = $('#payerFullAddress').val().split('\n');
		address1 = lines[0];
		address2 = lines[1];
		city = lines[2];
		postal = lines[3];
	} else {
		lines = $('#payerFullAddress').val().trim();
		address1 = "";
		address2 = "";
		city = "";
		postal = "";
	}

	if (name !== "" && accountno !== "") {
		if (proc === "Update" || proc === "Add") {
			url = "/Payer/UpdatePayer";
			dat = { 'id': ID, 'name': name, 'line1': address1, 'line2': address2, 'city': city, 'zip': postal, 'phone': phone, 'bankid': bankid, 'accountnumber': accountno, 'address': address, 'emailadd': emailadd };
		} else if (proc === "Delete") {
			url = "/Payer/DeletePayer";
			dat = { 'id': ID };
		}
		$.ajax({
			url: url,
			type: 'POST',
			data: dat,
			dataType: 'json',
			beforeSend: function () {
				$('#divSpinner').removeClass("hidden");
				$('#lblLoading').html("Saving payer information...");
			},
			complete: function () {
				$('#divSpinner').addClass("hidden");
			},
			success: function (a) {
				if (a === '2') {
					demo.showNotification('top', 'center', 'Payer sucessfully added!', 2);
				} else if (a === '1') {
					demo.showNotification('top', 'center', 'Payer sucessfully updated!', 2);
				} else if (a === '3') {
					demo.showNotification('top', 'center', 'Payer sucessfully deleted!', 2);
				} else {
					demo.showNotification('top', 'center', a, 4);
				}
			}
		}).done(function () {
			$("#btnClose").click();
			loadTable();
			loadPayer();
		});
	} else {
		demo.showNotification('top', 'center', 'Please provide all required fields(*)', 4);
		return;
	}

}

function timeConverter(createdAt) {
	var d = new Date(parseInt(createdAt.substr(6)));
	//return d.toLocaleDateString("en-US");
	return pad(d.getMonth() + 1, 2) + "-" + pad(d.getDate(), 2) + "-" + d.getFullYear();
}

function pad(str, max) {
	str = str.toString();
	return str.length < max ? pad("0" + str, max) : str;
}

function loadBank() {
	if ($.fn.DataTable.isDataTable("#tblBank")) {
		$('#tblBank').DataTable().clear().destroy();
	}

	var t = $('#tblBank').DataTable({
		//"processing": true,
		//dom: 'Bfrtip',	
		"ajax": {
			"url": "/Payer/GetAllBank",
			"dataSrc": ""
		},
		autoWidth: false,
		"order": [[1, "asc"]],
		"columnDefs": [
			{ "visible": false, "targets": 0 },
			{ "visible": true, "title": "BANK NAME", "targets": 1 },
			{ "visible": true, "title": "ADDRESS", "targets": 2 },
			{ "visible": true, "title": "CITY/STATE/ZIPCODE", "targets": 3 },
			{ "visible": true, "title": "ROUTING NUMBER", "targets": 4 },
			{ "visible": true, "title": "SELECT", "targets": 5 }
		],
		"columns": [
			{ "data": "ID" },
			{ "data": "BankName" },
			{ "data": "BankAddress" },
			{ "data": "BankCityStateZip" },
			{ "data": "RoutingNumber" },
			{
				"render": function (data, type, full, meta) {
					var ID = full.ID;
					var bank = full.BankName;
					var routing = full.RoutingNumber;
					return '<button type="button" rel="tooltip" title="Choose" class="btn btn-info btn-xs btn-simple" id="btnChoose' + ID + '" onclick="selectBank(' + ID + ', \'' + bank + '\', \'' + routing + '\')">' +
						'<i class="material-icons">check_circle</i>' +
						'</button>';
				}
			}
		]
	});
}

function selectBank(id, bank, routing) {
	$('#payerBankID').val(id);
	$('#payerBankName').val(bank);
	$('#payerRoutingNumber').val(routing);
	if (bank === "" || bank === null) {
		$("#divBankName").addClass("is-empty");
	} else {
		$("#divBankName").removeClass("is-empty");
	}
	if (routing === "" || routing === null) {
		$("#divRoutingNumber").addClass("is-empty");
	} else {
		$("#divRoutingNumber").removeClass("is-empty");
	}

	if ($('#payerBankID').val() !== "" && $('#payerBankID').val() !== null) {
		$('#btnCloseBank').click();
	}
}

function getSpecificBank() {
	var xrouting = $("#payerRoutingNumber").val();
	if (xrouting.length === 9) {
		var xstr = "";
		$.ajax({
			url: '/Checks/GetSpecificBank',
			type: 'POST',
			data: { routing: xrouting },
			beforeSend: function () {
				$('#divSpinner').removeClass("hidden");
				$('#lblLoading').html("Retreiving Bank information...");
			},
			complete: function () {
				$('#divSpinner').addClass("hidden");
			},
			dataType: 'json',
			success: function (a) {

			}
		}).done(function (a) {
			var xint = a.length;
			if (xint === 0) {
				var bank2 = $("#payerBankName").val("");
				var bankaddress2 = "";
				var bankcitystate2 = "";
				var bankid2 = "1";
				$("#divBankName").addClass("is-empty");
				$('#payerBankID').val(bankid2);
				demo.showNotification('top', 'center', 'Invalid Routing Number!', 4);
			} else {
				for (i = 0; i < a.length; i++) {
					var bank = $("#payerBankName").val(a[i].BankName);
					var bankaddress = a[i].BankAddress;
					var bankcitystate = a[i].BankCityStateZip;
					var bankid = a[i].ID;
					if (bank === "" || bank === null) {
						$("#divBankName").addClass("is-empty");
					} else {
						$("#divBankName").removeClass("is-empty");
					}
					$('#payerBankID').val(bankid);
				}
			}
		});
	}

}

function addPayer() {
	$('#payerID').val('0');
	$('#payerName').val('');
	$('#divName').addClass('is-empty');
	$('#payerAddressLine1').val('');
	$('#divAddressLine1').addClass('is-empty');
	$('#payerAddressLine2').val('');
	$('#divAddressLine2').addClass('is-empty');
	$('#payerCityState').val('');
	$('#divCityState').addClass('is-empty');
	$('#payerPostalCode').val('');
	$('#divPostalCode').addClass('is-empty');
	$('#payerPhone').val('');
	$('#divPhone').addClass('is-empty');
	$('#payerAccountNumber').val('');
	$('#divAccountNumber').addClass('is-empty');
	$('#payerBankID').val('');
	$('#payerBankName').val('');
	$('#divBankName').addClass('is-empty');
	$('#payerRoutingNumber').val('');
	$('#divRoutingNumber').addClass('is-empty'); 
	$('#payerEmail').val('');
	$('#divEmail').addClass('is-empty');
	$('#payerFullAddress').val('');
	$('#divFullAddress').addClass('is-empty');
	$('#payerAddress').val('');
	$('#divAddress').addClass('is-empty');
	$("#btnAddPayer").click();
	$("#h4PayerModal").text("Enter Payee's Information");
	$("#btnSave1").text("Add");
}