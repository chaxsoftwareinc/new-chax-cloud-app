﻿function triggerActive(li) {
	$(document).ready(function () {
		$("#liDashboard").removeClass('active');
		$("#liPayee").removeClass('active');
		$("#liPayer").removeClass('active');
		$("#liSetup").removeClass('active');
		$("#liUsers").removeClass('active');
		$("#liTransmitACH").removeClass('active');
		$("#liReports").removeClass('active');
		$("#liArchive").removeClass('active');
		$("#liSettings").removeClass('active');
		$("#" + li).addClass('active');
		getTotalCount();
		premiumSubscription();
		setSideBtnActive();
	});
}



function getPayee() {
	$.ajax({
		url: '/Dashboard/getPayee',
		type: 'POST',
		data: { 'name': "Venny Martinez" },
		dataType: 'json',
		success: function (a) {
			alert(a);
		}
	});
}

function setSideBtnActive() {
	$("#btnAlignment").removeClass("btn-info");
	$("#btnAlignment").addClass("btn-white");
}

function getPayer() {
	$.ajax({
		url: '/Payer/GetPayer',
		type: 'POST',
		data: { 'name': "Venny Martinez" },
		dataType: 'json',
		success: function (a) {
			alert(a);
		}
	});
}

function getTotalCount() {
	$.ajax({
		url: '/Layout/GetTotalCount',
		type: 'POST',
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {
		if (a === null || a === "" || a === "0") {
			$("#spanNotif").addClass("hidden");
		} else {
			$("#spanNotif").removeClass("hidden");
			$("#spanNotif").html(a);
		}

	});
}

function getNotification() {
	var xstr = "";
	$.ajax({
		url: '/Layout/GetNotification',
		type: 'POST',
		dataType: 'json',
		success: function (a) {
			if (a.length === 0 || a === null) {
				xstr = '<li> <a href="#">No check to print.</a></li>';
			} else {

				for (i = 0; i < a.length; i++) {
					amount = parseFloat(a[i].Amount).toFixed(2);
					xstr = xstr + '<li> <a href="#" data-toggle="modal" data-target="#exampleModalLongAllUnprinted" onclick="notSpicificCheck(' + a[i].ID + ')"><span class="fa fa-print"></span> | Ready to print Check No. ' + a[i].CheckNumber + ' for ' + a[i].PayeeName + ' with amount of $' + amount + '</a></li>';
				}
			}
		}
	}).done(function (a) {
		$("#ulNotif").empty();
		$("#ulNotif").append(xstr);
	});
}

function notSpicificCheck(id) {
	var xstr = "";
	$.ajax({
		url: '/Layout/GetUnprintedCheck',
		type: 'POST',
		data: { id: id },
		dataType: 'json',
		complete: function () {
			$('#divSpinner').addClass("hidden");
			getSubscriptionNotification();
		},
		success: function (a) {
			if (a.length === 0 || a === null) {
				xstr = 'Nothing to print.';
			} else {
				for (i = 0; i < a.length; i++) {
					var xstrdate = a[i].DateCreated;
					var xstrdate2 = a[i].Date;
					var amount = parseFloat(a[i].Amount).toFixed(2);
					var xmemorized = "";
					var ymemorized = "";
					if (a[i].IsPrinted === true) {
						isprintedclass = "hidden";
						isprintedtitle = "Printed";
					} else {
						isprintedclass = "btn btn-info btn-simple btn-xs";
						isprintedtitle = "Print";
					}
					xstr = xstr + '<tr>' +
						'<td>' + a[i].CheckNumber + '</td>' +
						'<td>' + a[i].BankName + '</td>' +
						'<td>' + a[i].PayerName + '</td>' +
						'<td>' + a[i].PayeeName + '</td>' +
						'<td> $' + amount + '</td>' +
						'<td>' + timeConverter(xstrdate) + '</td>' +
						'<td>' + timeConverter(xstrdate2) + '</td>' +
						'<td id="tdLayoutNotification" class="disableclick" style="width: 250px;">' + layoutNotification(a[i].CheckLayout, a[i].ID, a[i].IsGenerated) + '</td>' +
						'<td class="text-info">' + statusNotification(a[i].IsMemorized) + '</td>' +
						'<td class="">' +
						'<button type="button" rel="tooltip" title="' + isprintedtitle + '" id="btnSaveNotification' + a[i].ID + '" class="' + isprintedclass + '" onclick="previewNotification(' + a[i].ID + ')">' +
						'<i class="material-icons">print</i> Print this check' +
						'</button>' +
						'</td>' +
						'</tr>';
				} 
			}
		}
	}).done(function (a) {
		$("#tblAllUnprintedBody").empty();
		$("#tblAllUnprintedBody").append(xstr);
	});
}

function timeConverter(createdAt) {
	var d = new Date(parseInt(createdAt.substr(6)));
	//return d.toLocaleDateString("en-US");
	return pad(d.getMonth() + 1, 2) + "-" + pad(d.getDate(), 2) + "-" + d.getFullYear();
}

function pad(str, max) {
	str = str.toString();
	return str.length < max ? pad("0" + str, max) : str;
}

function previewNotification(id) {
	//var not = demo.showNotificationDashboard('top', 'center', 'Previewing Check...', 2);
	var layout = $("#inLayoutNotification" + id).val();
	var batchcount = $("#inBatchCountNotification" + id).val();
	var viewmodel = {};
	$.ajax({
		url: '/Dashboard/PreviewCheck',
		type: 'POST',
		data: { id: id },
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Previewing a check...");
		},
		success: function (a) {
			for (i = 0; i < a.length; i++) {
				var xstrdate = a[i].Date;
				viewmodel = {
					CheckNumber: a[i].CheckNumber,
					PayerName: a[i].PayerName,
					PayerAddress: a[i].PayerAddress,
					BankName: a[i].BankName,
					BankAddress: a[i].BankAddress,
					BankCityStateZip: a[i].BankCityState,
					RoutingNumber: a[i].RoutingNumber,
					AccountNumber: a[i].AccountNumber,
					Amount: a[i].Amount,
					AmountInWords: a[i].AmountInWords,
					Date: timeConverter(xstrdate),
					PayeeName: a[i].PayeeName,
					PayeeAddress: a[i].Payee,
					Payee: a[i].Payee,
					Memo: a[i].Memo,
					CheckLayout: layout,
					BatchCount: batchcount,
					ProcessType: 'Preview Dashboard'
				};
			}
		}
	}).done(function () {
		printCheckNotification(id);
		var ci = viewmodel;
		$.ajax({
			url: '/Check/Pdf',
			type: 'POST',
			data: ci,
			dataType: 'json',
			complete: function () {
				$('#divSpinner').addClass("hidden");
				appendTableBodyNotification();
				notSpicificCheck(id);
			},
			success: function (a) {

			}
		}).done(function (a) {
			window.open("/Check/Preview?file=" + a);
			getTotalCount();
			getNotification();
		});

	});
}

function saveNotification(id, proc) {
	var layout = $("#inLayoutNotification" + id).val();
	var batchcount = $("#inBatchCountNotification" + id).val();
	var viewmodel = {};
	$.ajax({
		url: '/Dashboard/PreviewCheck',
		type: 'POST',
		data: { id: id },
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Downloading a check...");
		},
		success: function (a) {
			for (i = 0; i < a.length; i++) {
				var xstrdate = a[i].Date;
				viewmodel = {
					CheckNumber: a[i].CheckNumber,
					PayerName: a[i].PayerName,
					PayerAddress: a[i].PayerAddress,
					BankName: a[i].BankName,
					BankAddress: a[i].BankAddress,
					BankCityStateZip: a[i].BankCityState,
					RoutingNumber: a[i].RoutingNumber,
					AccountNumber: a[i].AccountNumber,
					Amount: a[i].Amount,
					AmountInWords: a[i].AmountInWords,
					Date: timeConverter(xstrdate),
					PayeeName: a[i].PayeeName,
					PayeeAddress: a[i].Payee,
					Payee: a[i].Payee,
					Memo: a[i].Memo,
					CheckLayout: layout,
					BatchCount: batchcount,
					ProcessType: 'Save Dashboard'
				};
			}
		}
	}).done(function () {
		var ci = viewmodel;
		$.ajax({
			url: '/Check/Pdf',
			type: 'POST',
			data: ci,
			dataType: 'json',
			complete: function () {
				$('#divSpinner').addClass("hidden");
			},
			success: function (a) {

			}
		}).done(function (a) {
			window.location.href = "/Check/Download?file=" + a;
		});

	});
}

function layoutNotification(layout, id, isgen) {
	if (isgen === true) {
		var txtCount = '<div class="form-group label-floating" id="divCheckCountNotification' + id + '">' +
			'<label class="control-label"> Check Count(Batch) </label>' +
			'<input id="inBatchCountNotification' + id + '" type="number" class="form-control" min="2" max="100" value="2" required/>' +
			'</div >';
		if (layout === "1") {
			return ' Top <input class="hidden" id="inLayoutNotification' + id + '" type="text" value="Top">';
		}
		else if (layout === "2") {
			return ' Middle <input class="hidden" id="inLayoutNotification' + id + '" type="text" value="Middle">';
		}
		else if (layout === "3") {
			return ' Bottom <input class="hidden" id="inLayoutNotification' + id + '" type="text" value="Bottom">';
		}
		else if (layout === "4") {
			return ' <input class="hidden" id="inLayoutNotification' + id + '" type="text" value="Batch">' + txtCount;
		}
	}

	//var txtCount = '<div class="form-group label-floating hidden" id="divCheckCountNotification' + id + '">' +
	//	'<label class="control-label"> Number of Checks (Batch)</label>' +
	//	'<input id="inBatchCountNotification' + id + '" type="number" class="form-control" min="2" max="100" value="2" required/>' +
	//	'</div >';
	//if (layout === "1") {
	//	return '<input type="button" id="btnTopNotification' + id + '" class="btn btn-info btn-xs" value="Top" onclick="filllayoutNotification(' + id + ', \'1\'); onLayoutClickNotification(this.id, ' + id + '); hideBatchInputNotification(' + id + ');"><input type="button" id="btnMiddleNotification' + id + '" class="btn btn-default btn-xs" value="Mid" onclick="filllayoutNotification(' + id + ', \'2\'); onLayoutClickNotification(this.id, ' + id + '); hideBatchInputNotification(' + id + ');"><input type="button" id="btnBottomNotification' + id + '" class="btn btn-default btn-xs" value="Bot" onclick="filllayoutNotification(' + id + ', \'3\'); onLayoutClickNotification(this.id, ' + id + '); hideBatchInputNotification(' + id + ');"><input type="button" id="btnBatchNotification' + id + '" class="btn btn-default btn-xs" value="Bat" onclick="filllayoutNotification(' + id + ', \'4\'); onLayoutClickNotification(this.id, ' + id + '); showBatchInputNotification(' + id + ');"><input class="hidden" id="inLayoutNotification' + id + '" type="text" value="Top">' + txtCount;
	//}
	//else if (layout === "2") {
	//	return '<input type="button" id="btnTopNotification' + id + '" class="btn btn-default btn-xs" value="Top" onclick="filllayoutNotification(' + id + ', \'1\'); onLayoutClickNotification(this.id, ' + id + '); hideBatchInputNotification(' + id + ');"><input type="button" id="btnMiddleNotification' + id + '" class="btn btn-info btn-xs" value="Mid" onclick="filllayoutNotification(' + id + ', \'2\'); onLayoutClickNotification(this.id, ' + id + '); hideBatchInputNotification(' + id + ');"><input type="button" id="btnBottomNotification' + id + '" class="btn btn-default btn-xs" value="Bot" onclick="filllayoutNotification(' + id + ', \'3\'); onLayoutClickNotification(this.id, ' + id + '); hideBatchInputNotification(' + id + ');"><input type="button" id="btnBatchNotification' + id + '" class="btn btn-default btn-xs" value="Bat" onclick="filllayoutNotification(' + id + ', \'4\'); onLayoutClickNotification(this.id, ' + id + '); showBatchInputNotification(' + id + ');"><input class="hidden" id="inLayoutNotification' + id + '" type="text" value="Middle">' + txtCount;
	//}
	//else if (layout === "3") {
	//	return '<input type="button" id="btnTopNotification' + id + '" class="btn btn-default btn-xs" value="Top" onclick="filllayoutNotification(' + id + ', \'1\'); onLayoutClickNotification(this.id, ' + id + '); hideBatchInputNotification(' + id + ');"><input type="button" id="btnMiddleNotification' + id + '" class="btn btn-default btn-xs" value="Mid" onclick="filllayoutNotification(' + id + ', \'2\'); onLayoutClickNotification(this.id, ' + id + '); hideBatchInputNotification(' + id + ');"><input type="button" id="btnBottomNotification' + id + '" class="btn btn-info btn-xs" value="Bot" onclick="filllayoutNotification(' + id + ', \'3\'); onLayoutClickNotification(this.id, ' + id + '); hideBatchInputNotification(' + id + ');"><input type="button" id="btnBatchNotification' + id + '" class="btn btn-default btn-xs" value="Bat" onclick="filllayoutNotification(' + id + ', \'4\'); onLayoutClickNotification(this.id, ' + id + '); showBatchInputNotification(' + id + ');"><input class="hidden" id="inLayoutNotification' + id + '" type="text" value="Bottom">' + txtCount;
	//}
	//else if (layout === "4") {
	//	return '<input type="button" id="btnTopNotification' + id + '" class="btn btn-default btn-xs" value="Top" onclick="filllayoutNotification(' + id + ', \'1\'); onLayoutClickNotification(this.id, ' + id + '); hideBatchInputNotification(' + id + ');"><input type="button" id="btnMiddleNotification' + id + '" class="btn btn-default btn-xs" value="Mid" onclick="filllayoutNotification(' + id + ', \'2\'); onLayoutClickNotification(this.id, ' + id + '); hideBatchInputNotification(' + id + ');"><input type="button" id="btnBottomNotification' + id + '" class="btn btn-default btn-xs" value="Bot" onclick="filllayoutNotification(' + id + ', \'3\'); onLayoutClickNotification(this.id, ' + id + '); hideBatchInputNotification(' + id + ');"><input type="button" id="btnBatchNotification' + id + '" class="btn btn-info btn-xs" value="Bat" onclick="filllayoutNotification(' + id + ', \'4\'); onLayoutClickNotification(this.id, ' + id + '); showBatchInputNotification(' + id + ');"><input class="hidden" id="inLayoutNotification' + id + '" type="text" value="Batch">' + txtCount;
	//}
}

function filllayoutNotification(id, layout, btnid) {
	if (layout === "1") {
		$("#inLayoutNotification" + id).val("Top");
	}
	else if (layout === "2") {
		$("#inLayoutNotification" + id).val("Middle");
	}
	else if (layout === "3") {
		$("#inLayoutNotification" + id).val("Bottom");
	}
	else if (layout === "4") {
		$("#inLayoutNotification" + id).val("Batch");
	}
}

function onLayoutClickNotification(btnid, id) {
	$("#btnTopNotification" + id).removeClass("btn-default");
	$("#btnTopNotification" + id).removeClass("btn-info");
	$("#btnMiddleNotification" + id).removeClass("btn-default");
	$("#btnMiddleNotification" + id).removeClass("btn-info");
	$("#btnBottomNotification" + id).removeClass("btn-default");
	$("#btnBottomNotification" + id).removeClass("btn-info");
	$("#btnBatchNotification" + id).removeClass("btn-default");
	$("#btnBatchNotification" + id).removeClass("btn-info");

	$("#" + btnid).addClass("btn-info");
}

function hideBatchInputNotification(id) {
	$("#divCheckCountNotification" + id).addClass("hidden");
}

function showBatchInputNotification(id) {
	$("#divCheckCountNotification" + id).removeClass("hidden");
}

function statusNotification(stat) {
	if (stat === false) {
		return "Ready to print";
	} else if (stat === true) {
		return "Memorized";
	}
}

function getSubscriptionNotification() {
	$.ajax({
		url: '/Layout/GetSubscription',
		type: 'POST',
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {
		if (a === 0) {
			$("#tdLayoutNotification").addClass("disableclick");
		} else {
			$("#tdLayoutNotification").removeClass("disableclick");
		}
	});
}


function premiumSubscription() {
	$.ajax({
		url: '/Premium/GetSubscription',
		type: 'POST',
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {
		if (a > 0) {
			$("#pPremium").html("PREMIUM");
			$("#pPremium").html("PREMIUM");
		}
	});
}

function printCheckNotification(id) {
	$.ajax({
		url: '/Dashboard/PrintCheck',
		type: 'POST',
		data: { id: id },
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {
		//if (a === "1") {
		//	demo.showNotification('top', 'center', 'Check successfully generated and can now be printed!', 2);
		//} else {
		//	demo.showNotification('top', 'center', a, 4);
		//}
	});
	//else {
	//	demo.showNotification('top', 'center', 'This check is already generated and is ready to be printed!', 4);
	//}
}

function appendTableBodyNotification() {
	var xstr = "";
	$.ajax({
		url: '/Dashboard/GetTopPrintedChecks',
		type: 'POST',
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Loading all downloaded Checks...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
			getSubscription();
		},
		success: function (a) {
			$("#tblPrintedChecks").empty();
			for (i = 0; i < a.length; i++) {
				var xstrdate = a[i].DateCreated;
				var xstrdate2 = a[i].Date;
				var amount = parseFloat(a[i].Amount).toFixed(2);
				var xmemorized = "";
				var ymemorized = "";
				if (a[i].IsMemorized === true) {
					xmemorized = "btn-default";
					ymemorized = "Tagged as Memorized";
				} else {
					xmemorized = "btn-info";
					ymemorized = "Memorize";
				}
				if (a[i].IsPrinted === true) {
					isprintedclass = "btn btn-default btn-simple btn-xs hidden";
					isprintedtitle = "This check is already printed";
				} else {
					isprintedclass = "btn btn-info btn-simple btn-xs";
					isprintedtitle = "Print";
				}
				xstr = xstr + '<tr>' +
					'<td>' + a[i].CheckNumber + '</td>' +
					'<td>' + a[i].BankName + '</td>' +
					'<td>' + a[i].PayerName + '</td>' +
					'<td>' + a[i].PayeeName + '</td>' +
					'<td> $' + amount + '</td>' +
					'<td>' + timeConverter(xstrdate) + '</td>' +
					'<td>' + timeConverter(xstrdate2) + '</td>' +
					'<td id="tdLayout" class="disableclick">' + layout(a[i].CheckLayout, a[i].ID) + '</td>' +
					'<td class="text-info"><a href="#" title="Generate to print the check" onclick = "generateCheck(' + a[i].ID + ', ' + a[i].IsGenerated + ', ' + a[i].IsPrinted + ')">' + status(a[i].IsMemorized, a[i].IsGenerated, a[i].IsPrinted) + '</a></td>' +
					'<td class="">' +
					'<button type="button" rel="tooltip" title="' + ymemorized + '" class="btn ' + xmemorized + ' btn-simple btn-xs" id="btnMemorizeDashboard' + a[i].ID + '" onclick="memorizeCheck(' + a[i].ID + ', ' + a[i].CheckNumber + ')">' +
					'<i class="material-icons">star</i>' +
					'</button>' +
					'<button type="button" rel="tooltip" title="Save a copy" id="btnSaveDashboard' + a[i].ID + '" class="btn btn-info btn-simple btn-xs hidden" onclick="save(' + a[i].ID + ', \'d\')">' +
					'<i class="material-icons">save</i>' +
					'</button>' +
					'<button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs" data-toggle="modal" data-target="#exampleModalLongConfirmDelete" onclick="getIdToDelete(' + a[i].ID + ', \'' + a[i].CheckNumber + '\')">' +
					'<i class="material-icons">delete</i>' +
					'</button>' +
					'<button type="button" rel="tooltip" title="' + isprintedtitle + '" id="btnPreviewDashboard' + a[i].ID + '" class="' + isprintedclass + '" onclick="preview(' + a[i].ID + ', \'d\', ' + a[i].IsGenerated + ')">' +
					'<i class="material-icons">print</i>' +
					'</button>' +
					'</td>' +
					'</tr>';
			}
		}
	}).done(function () {
		$("#tblPrintedChecks").append(xstr);
	});
}