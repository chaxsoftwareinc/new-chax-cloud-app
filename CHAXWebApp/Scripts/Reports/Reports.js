﻿var $j = jQuery.noConflict();

$(document).ready(function () {
	triggerActive("liReports"); //setting the class active for sidebar
	getTotalPrinted();
	getTotalAmount();
	appendTableRecentPayers();
	appendTableRecentPayees();
	appendTableChecks();
});

function appendTableRecentPayers() {
	var xstr = "";
	$.ajax({
		url: '/Reports/GetRecentPayer',
		type: 'POST',
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Loading all tables");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {
			$("#tblRecentlyUsedPayers").empty();
			for (i = 0; i < a.length; i++) {
				xstr = xstr + '<tr>' +
					'<td>' + a[i].PayerName + '</td>' +
					'<td>' + a[i].CheckNumber + '</td>' +
					'<td>' + a[i].Amount + '</td>' +
					'<td>' + a[i].BankName + '</td>' +
					'<td>' + a[i].AccountNumber + '</td>' +
					'</tr>';
			}
		}
	}).done(function () {
		$("#tblRecentlyUsedPayers").append(xstr);
	});
}

function appendTableRecentPayees() {
	var xstr = "";
	$.ajax({
		url: '/Reports/GetRecentPayee',
		type: 'POST',
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Loading all tables");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {
			$("#tblRecentlyUsedPayees").empty();
			for (i = 0; i < a.length; i++) {
				xstr = xstr + '<tr>' +
					'<td>' + a[i].PayeeName + '</td>' +
					'<td>' + a[i].CheckNumber + '</td>' +
					'<td>' + a[i].Amount + '</td>' +
					'<td>' + a[i].BankName + '</td>' +
					'<td>' + a[i].AccountNumber + '</td>' +
					'<td>' + a[i].PayerName + '</td>' +
					'</tr>';
			}
		}
	}).done(function () {
		$("#tblRecentlyUsedPayees").append(xstr);
	});
}

function appendTableChecks() {
	var xstr = "";
	$.ajax({
		url: '/Reports/GetReportByMonth',
		type: 'POST',
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Loading all tables");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {
			$("#tblChecks").empty();
			for (i = 0; i < a.length; i++) {
				xstr = xstr + '<tr>' +
					'<td>' + a[i].Month + '</td>' +
					'<td>' + a[i].TotalAmount + '</td>' +
					'<td>' + a[i].TotalCount + '</td>' +
					'</tr>';
			}
		}
	}).done(function () {
		$("#tblChecks").append(xstr);
	});
}

function loadTableAllPayers() {
	if ($.fn.DataTable.isDataTable("#tblAllPayers")) {
		$('#tblAllPayers').DataTable().clear().destroy();
	}

	var t = $('#tblAllPayers').DataTable({
		//"processing": true,
		dom: 'Bfrtip',
		"ajax": {
			"url": "/Reports/GetAllPayer",
			"dataSrc": ""
		},
		"autoWidth": false,
		"columnDefs": [
			{ "visible": false, "targets": 0 },
			{ "visible": true, "title": "PAYER NAME", "targets": 1 },
			{ "visible": true, "title": "CHECK NUMBER", "targets": 2 },
			{ "visible": true, "title": "AMOUNT", "targets": 3 },
			{ "visible": true, "title": "BANK", "targets": 4 },
			{ "visible": true, "title": "ACCOUNT NUMBER", "targets": 5 }
		],
		"columns": [
			{ "data": "ID" },
			{ "data": "PayerName" },
			{ "data": "CheckNumber" },
			{ "data": "Amount" },
			{ "data": "BankName" },
			{ "data": "AccountNumber" }
		],
		"initComplete": function (settings, json) {
			$(this).removeClass("dataTable");
		},
		"buttons": [
			{
				"extend": 'copy',
				"text": '<i class="fa fa-edit"></i> Copy',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				"extend": 'excel',
				"text": '<i class="fa fa-file-excel-o"></i> Excel',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				"extend": 'csv',
				"text": '<i class="fa fa-database"></i> CSV',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				"extend": 'pdf',
				"text": '<i class="fa fa-file-excel-o"></i> PDF',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				"extend": 'print',
				"text": '<i class="fa fa-print"></i> Print',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			}
		]
	});
}


function loadTableAllPayees() {
	if ($.fn.DataTable.isDataTable("#tblAllPayees")) {
		$('#tblAllPayees').DataTable().clear().destroy();
	}

	var t = $('#tblAllPayees').DataTable({
		//"processing": true,
		dom: 'Bfrtip',
		"ajax": {
			"url": "/Reports/GetAllPayee",
			"dataSrc": ""
		},
		"autoWidth": false,
		"columnDefs": [
			{ "visible": false, "targets": 0 },
			{ "visible": true, "title": "PAYEE NAME", "targets": 1 },
			{ "visible": true, "title": "CHECK NUMBER", "targets": 2 },
			{ "visible": true, "title": "AMOUNT", "targets": 3 },
			{ "visible": true, "title": "BANK", "targets": 4 },
			{ "visible": true, "title": "ACCOUNT NUMBER", "targets": 5 },
			{ "visible": true, "title": "PAYER", "targets": 6 }
		],
		"columns": [
			{ "data": "ID" },
			{ "data": "PayeeName" },
			{ "data": "CheckNumber" },
			{ "data": "Amount" },
			{ "data": "BankName" },
			{ "data": "AccountNumber" },
			{ "data": "PayerName" }
		],
		"initComplete": function (settings, json) {
			$(this).removeClass("dataTable");
		},
		"buttons": [
			{
				"extend": 'copy',
				"text": '<i class="fa fa-edit"></i> Copy',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				"extend": 'excel',
				"text": '<i class="fa fa-file-excel-o"></i> Excel',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				"extend": 'csv',
				"text": '<i class="fa fa-database"></i> CSV',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				"extend": 'pdf',
				"text": '<i class="fa fa-file-excel-o"></i> PDF',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				"extend": 'print',
				"text": '<i class="fa fa-print"></i> Print',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			}
		]
	});
}