﻿$(document).ready(function () {
	$.ajax({
		url: '/CProfile/GetCurrentUserId',
		type: 'POST',
		dataType: 'json',
		success: function (a) {
			$("#txtUserName").val(a.UserName);
			$("#txtEmailAddress").val(a.EmailAddress);
			$("#txtFirstName").val(a.FirstName);
			$("#txtLastName").val(a.LastName);
			$("#h4FullName").text(a.FirstName + ' ' + a.LastName);
		}
	}).done(function () {
		$("#divUserName").removeClass("is-empty");
		$("#divEmailAddress").removeClass("is-empty");
		$("#divFirstName").removeClass("is-empty");
		$("#divLastName").removeClass("is-empty");
	});

	getTotalCount();
});

$("#btnUpdateProfile").click(function () {
	username = $("#txtUserName").val();
	firstname = $("#txtFirstName").val();
	lastname = $("#txtLastName").val();
	email = $("#txtEmailAddress").val();
	dat = { "username": username, "firstname": firstname, "lastname": lastname, "emailaddress": email };
	url = "/Manage/UpdateUser";
	$.ajax({
		url: url,
		type: 'POST',
		data: dat,
		async: false,
		dataType: 'json',
		success: function (a) {
			if (a === '0') {
				demo.showNotification('top', 'center', 'Profile sucessfully updated!', 2);
			} else {
				demo.showNotification('top', 'center', a, 3);
			}
		}
	});
});