﻿var $j = jQuery.noConflict();

$(document).ready(function () {
	triggerActive("upgrade_to_pro"); //setting the class active for sidebar; //setting the class active for sidebar
	premiumSubscription2();
});

function upgradeToPremium() {
	$.ajax({
		url: '/Premium/UpgradeToPremium',
		type: 'POST',
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {
		if (a === "1") {
			demo.showNotification('top', 'center', 'Successfully Subscribed!', 2);
			premiumSubscription2();
		} else {
			demo.showNotification('top', 'center', 'Please check subscription!', 2);
		}
	});
}

function premiumSubscription2() {
	$.ajax({
		url: '/Premium/GetSubscription',
		type: 'POST',
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {
		if (a > 0) {
			$("#aUpgrade").html('<span class="fa fa-check"></span> Premium');
			$("#aUpgrade").addClass("disabled");
		} else {			
			$("#aUpgrade").html('<span class="fa fa-upload"></span> Upgrade to PRO');
			$("#aUpgrade").removeClass("disabled");
		}
	});
}

