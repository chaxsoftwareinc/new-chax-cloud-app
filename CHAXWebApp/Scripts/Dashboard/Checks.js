﻿var $j = jQuery.noConflict();

$(document).ready(function () {
	$("#inLayout").val('Top');

	$("input[name='optradio']").on("click", function () {
		$("#inLayout").val($("input:checked").val());
	});

	$("textarea")
		.on('keypress', function (event) {
			var textarea = $(this),
				text = textarea.val(),
				numberOfLines = (text.match(/\n/g) || []).length + 1,
				maxRows = parseInt(textarea.attr('rows'));

			if (event.which === 13 && numberOfLines === maxRows) {
				return false;
			}
		});
});

$(function () {
	var input = document.getElementById('inAmountInDigits');
	var output = document.getElementById('inAmountInWords');
	output.value = 'Please enter amount in check.';
	$("#divAmount").removeClass("is-empty");
	input.onkeyup = function SetMaxLength(e) {
		var val = this.value;
		if (val.length === 0) {
			output.value = 'Please enter amount in check.';
			$("#divAmountInWords").removeClass("is-empty");
			return;
		}
		val1 = parseInt(val, 36);
		if (isNaN(val1)) {
			output.value = 'Invalid input.';
			$("#divAmountInWords").removeClass("is-empty");
			return;
		}
		if (e.keyCode === 190) {
			input.maxLength = 10;
			return;
		}
		output.value = ConvertToWords(val);
		$("#divAmountInWords").removeClass("is-empty");
	};
});

function ConvertToHundreds(num, i) {
	aTens = ["Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"];
	aOnes = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine",
		"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen",
		"Nineteen"];
	var cNum, nNum;
	var cWords = "";

	num %= 1000;
	if (num > 99) {
		/* Hundreds. */
		cNum = String(num);
		nNum = Number(cNum.charAt(0));
		cWords += aOnes[nNum] + " Hundred";
		num %= 100;
		if (num > 0) {
			if (i > 0) {
				cWords += " and ";
			}
			else {
				cWords += " ";
			}
		}
	}

	if (num > 19) {
		/* Tens. */
		cNum = String(num);
		nNum = Number(cNum.charAt(0));
		cWords += aTens[nNum - 2];
		num %= 10;
		if (num > 0)
			cWords += "-";
	}

	if (num > 0) {
		/* Ones and teens. */
		nNum = Math.floor(num);
		cWords += aOnes[nNum];
	}

	return cWords;
}

function ConvertToWords(num) {
	var aUnits = ["Thousand", "Million", "Billion", "Trillion", "Quadrillion"];
	var cWords = "";
	var nLeft = Math.floor(num);
	for (var i = 0; nLeft > 0; i++) {
		if (nLeft % 1000 > 0) {
			var str = cWords;
			cWords = str.replace("Dollars", "");
			if (i !== 0)
				cWords = ConvertToHundreds(nLeft, i) + " " + aUnits[i - 1] + " " + cWords + "Dollars";
			else
				cWords = ConvertToHundreds(nLeft, i) + " " + cWords + "Dollars";
		}
		nLeft = Math.floor(nLeft / 1000);
	}
	num = Math.round(num * 100) % 100;
	if (num > 0)
		cWords += " and " + num + "/100 Cents";

	var pos = cWords.lastIndexOf(" and ");


	return cWords;
}

$("#btnSelectPayer").click(function () {
	loadTablePayer();
});

$("#btnSelectPayee").click(function () {
	loadTablePayee();
});

function loadTablePayer() {
	if ($.fn.DataTable.isDataTable("#tblPayer")) {
		$('#tblPayer').DataTable().clear().destroy();
	}

	var t = $('#tblPayer').DataTable({
		"autoWidth": false,
		destroy: true,
		"ajax": {
			"url": "/Checks/GetAllPayer",
			"dataSrc": ""
		},
		"columnDefs": [
			{ "visible": true, "title": "NAME", "targets": 0 },
			{ "visible": true, "title": "ADDRESS LINE 1", "targets": 1 },
			{ "visible": true, "title": "ADDRESS LINE 2", "targets": 2 },
			{ "visible": true, "title": "CITY/STATE", "targets": 3 },
			{ "visible": true, "title": "POSTAL CODE", "targets": 4 },
			{ "visible": true, "title": "PHONE NUMBER", "targets": 5 },
			{ "visible": true, "title": "BANK NAME", "targets": 6 },
			{ "visible": true, "title": "ROUTING NUMBER", "targets": 7 },
			{ "visible": true, "title": "ROUTING NUMBER", "targets": 8 },
			{ "visible": true, "title": "", "targets": 9 }
		],
		"columns": [
			{ "data": "Name" },
			{ "data": "AddressLine1" },
			{ "data": "AddressLine2" },
			{ "data": "CityState" },
			{ "data": "ZipCode" },
			{ "data": "PhoneNumber" },
			{ "data": "BankName" },
			{ "data": "RoutingNumber" },
			{ "data": "BankAccount" },
			{
				"render": function (data, type, full, meta) {
					name = full.Name;
					accountnumber = full.BankAccount;
					bankname = full.BankName;
					routing = full.RoutingNumber;
					add1 = full.AddressLine1;
					add2 = full.AddressLine2;
					city = full.CityState;
					zip = full.ZipCode;
					phone = full.PhoneNumber;
					var ID = full.ID;
					return '<button type="submit" id="btnChoosePayer' + ID + '" class="btn btn-sm btn-primary" onclick="populateFieldsPayer(\'' + name + '\', \'' + accountnumber + '\', \'' + bankname + '\', \'' + routing + '\', \'' + add1 + '\', \'' + add2 + '\', \'' + city + '\', \'' + zip + '\', \'' + phone + '\')">Choose</button>';
				}
			}
		]
	});
}

function loadTablePayee() {
	if ($.fn.DataTable.isDataTable("#tblPayee")) {
		$('#tblPayee').DataTable().clear().destroy();
	}
	var ti = $('#tblPayee').DataTable({
		"autoWidth": false,
		"ajax": {
			"url": "/Checks/GetAllPayee",
			"dataSrc": ""
		},
		"columnDefs": [
			{ "visible": false, "targets": 0 },
			{ "visible": true, "title": "NAME", "targets": 1 },
			{ "visible": true, "title": "ADDRESS LINE 1", "targets": 2 },
			{ "visible": true, "title": "ADDRESS LINE 2", "targets": 3 },
			{ "visible": true, "title": "CITY/STATE", "targets": 4 },
			{ "visible": true, "title": "POSTAL CODE", "targets": 5 },
			{ "visible": true, "title": "PHONE NUMBER", "targets": 6 },
			{ "visible": true, "title": "", "targets": 7 }
		],
		"columns": [
			{ "data": "ID" },
			{ "data": "Name" },
			{ "data": "AddressLine1" },
			{ "data": "AddressLine2" },
			{ "data": "CityState" },
			{ "data": "ZipCode" },
			{ "data": "PhoneNumber" },
			{
				"render": function (data, type, full, meta) {
					var ID = full.ID;
					var name = full.Name;
					var line1 = full.AddressLine1;
					var line2 = full.AddressLine2;
					var city = full.CityState;
					var zip = full.ZipCode;
					var phone = full.PhoneNumber;
					return '<button type="submit" id="btnChoosePayee' + ID + '" class="btn btn-sm btn-primary" onclick="populateFieldsPayee(\'' + name + '\', \'' + line1 + '\', \'' + line2 + '\', \'' + city + '\', \'' + zip + '\', \'' + phone + '\')">Choose</button>';
				}
			}
		]
	});
}

function populateFieldsPayer(name, accountnumber, bankname, routing, add1, add2, city, zip, phone, bankaddress, bankcitystatezip) {
	$('#inPayerName').val(name);
	$('#divPayerName').removeClass("is-empty");
	$('#inAccountNumber').val(accountnumber);
	$('#divAccountNumber').removeClass("is-empty");
	$('#inBankName').val(bankname);
	$('#divBankName').removeClass("is-empty");
	$('#inRoutingNumber').val(routing);
	$('#divRoutingNumber').removeClass("is-empty");
	$('#inAddress').val(add1 + "\n" + add2 + "\n" + city + "\n" + zip + "\n" + phone);
	$('#divAddress').removeClass("is-empty");
	$("#btnCancelPayer").click();
}

function populateFieldsPayee(name, line1, line2, city, zip, phone) {
	$('#taDrawnToPayTo').val(line1 + "\n" + line2 + "\n" + city + "\n" + zip + "\n" + phone);
	$('#divDrawnToPayTo').removeClass("is-empty");
	$('#inPayeeName').val(name);
	$('#divPayeeName').removeClass("is-empty");
	$("#btnCancelPayee").click();
}

function getBank(id) {
	$.ajax({
		url: '/Dashboard/GetBank',
		type: 'POST',
		data: { 'id': id },
		dataType: 'json',
		success: function (a) {
			for (i = 0; i < a.length; i++) {
				$('#inBankName').val(a[i].BankName);
				$('#divBankName').removeClass("is-empty");
				$('#inRoutingNumber').val(a[i].RoutingNumber);
				$('#divRoutingNumber').removeClass("is-empty");
				bankid = a[i].BankID;
			}
		}
	}).done(function () {
		$("#btnCancelPayer").click();
	});
}

$('#btnDownload').click(function () {
	$("#inProcessType").val("Download");
	validateData();
});

$('#btnPreview').click(function () {
	$("#inProcessType").val("Preview");
	validateData();
});
$('#btnSave').click(function () {
	$("#inProcessType").val("Save");
	validateData();
});

$('#btnSubmit').click(function () {

});

function validateData() {	
	var strDrawn = $("#taDrawnToPayTo").val();
	var intDrawn = strDrawn.split('\n').length;
	var strPayerAddress = $("#inAddress").val();
	var intPayerAddress = strPayerAddress.split('\n').length;
	addSpacesPayee();
	addSpacesPayer();
	if (intDrawn > 6) {
		alert("The payee name and address max number of lines is five.");
	} else if (intPayerAddress > 5) {
		alert("The payer address max number of lines is five.");
	} else if ($("#inPayeeName").val().trim() === "") {
		//moveToFirstLine();
		alert("Payee is required.");
	} else if ($("#inAddress").val().trim() === "") {
		moveToFirstLine();
		alert("Payer is required.");
	} else {
		$('#btnSubmit').click();
	}
}

function getFirstLine() {
	var lines = $('taDrawnToPayTo').val().split('\n');
	var xstr = "";
	for (var i = 0; i < lines.length; i++) {
		//code here using lines[i] which will give you each line
		if (i === 0) {
			xstr = lines[i];
		}
	}
	$('#inHiddenPayeeName').val(xstr);
}

function validateRouting() {
	if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);
}

function moveToFirstLine() {
	if ($("#inAddress").val().trim() === "") {
		$("#inAddress").val("");
	}
	if ($("#taDrawnToPayTo").val().trim() === "") {
		$("#taDrawnToPayTo").val("");
	}
}

function addSpacesPayee() {
	var str = $("#taDrawnToPayTo").val().trim();
	var xint = str.split('\n').length;
	var xint2 = str.match('').length;
	if (xint < 5) {
		var xx = 5 - xint;
		for (x = 0; x < xx; x++) {
			str = str + "\n ";
		}
	}
	$("#taDrawnToPayTo").val(str);
}

function addSpacesPayer() {
	var str = $("#inAddress").val().trim();
	var xint = str.split('\n').length;
	if (xint < 5) {
		var xx = 5 - xint;
		for (x = 0; x < xx; x++) {
			str = str + "\n ";
		}
	}
	$("#inAddress").val(str);
}