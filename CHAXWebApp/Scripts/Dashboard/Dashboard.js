﻿var $j = jQuery.noConflict();

$(document).ready(function () {
	triggerActive("liDashboard"); //setting the class active for sidebar
	$("#navMenus").removeClass("disableclick");
	getTotalAmountThisMonth();
	getTotalChecksThisMonth();
	getTotalChecks();
	getTotalAmount();
	appendTableBody();
});

function appendTableBody() {
	var xstr = "";
	$.ajax({
		url: '/Dashboard/GetTopPrintedChecks',
		type: 'POST',
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Loading all downloaded Checks...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
			getSubscription();
		},
		success: function (a) {
			$("#tblPrintedChecks").empty();
			for (i = 0; i < a.length; i++) {

				var xstrdate = a[i].DateCreated;
				var xstrdate2 = a[i].Date;
				var amount = parseFloat(a[i].Amount).toFixed(2);
				var xmemorized = "";
				var ymemorized = "";
				if (addExpirationDate(xstrdate2) === "false") {
					if (a[i].IsMemorized === true) {
						xmemorized = "btn-default";
						ymemorized = "Tagged as Memorized";
					} else {
						xmemorized = "btn-info";
						ymemorized = "Memorize";
					}
					if (a[i].IsPrinted === true) {
						isprintedclass = "btn btn-default btn-simple btn-xs hidden";
						isprintedtitle = "This check is already printed";
					} else {
						isprintedclass = "btn btn-info btn-simple btn-xs";
						isprintedtitle = "Print";
					}
					xstr = xstr + '<tr>' +
						'<td>' + a[i].CheckNumber + '</td>' +
						'<td>' + a[i].BankName + '</td>' +
						'<td>' + a[i].PayerName + '</td>' +
						'<td>' + a[i].PayeeName + '</td>' +
						'<td> $' + amount + '</td>' +
						'<td>' + timeConverter(xstrdate) + '</td>' +
						'<td>' + timeConverter(xstrdate2) + '</td>' +
						'<td id="" class="disableclick tdLayout" style="width: 250px;">' + layout(a[i].CheckLayout, a[i].ID, a[i].IsGenerated) + '</td>' +
						'<td class="text-info"><a href="#" title="Generate to print the check" onclick = "generateCheck(' + a[i].ID + ', ' + a[i].IsGenerated + ', ' + a[i].IsPrinted + ')">' + status(a[i].IsMemorized, a[i].IsGenerated, a[i].IsPrinted) + '</a></td>' +
						'<td class="dropdown">' +
						//'<td class="" style="overflow: visible !important;">' +
						//'<button type="button" rel="tooltip" title="' + ymemorized + '" class="btn ' + xmemorized + ' btn-simple btn-xs" id="btnMemorizeDashboard' + a[i].ID + '" onclick="memorizeCheck(' + a[i].ID + ', ' + a[i].CheckNumber + ')">' +
						//'<i class="material-icons">star</i>' +
						//'</button>' +
						//'<button type="button" rel="tooltip" title="Save a copy" id="btnSaveDashboard' + a[i].ID + '" class="btn btn-info btn-simple btn-xs hidden" onclick="save(' + a[i].ID + ', \'d\')">' +
						//'<i class="material-icons">save</i>' +
						//'</button>' +
						//'<button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs" data-toggle="modal" data-target="#exampleModalLongConfirmDelete" onclick="getIdToDelete(' + a[i].ID + ', \'' + a[i].CheckNumber + '\')">' +
						//'<i class="material-icons">delete</i>' +
						//'</button>' +
						//'<button type="button" rel="tooltip" title="' + isprintedtitle + '" id="btnPreviewDashboard' + a[i].ID + '" class="' + isprintedclass + '" onclick="preview(' + a[i].ID + ', \'d\', ' + a[i].IsGenerated + ')">' +
						//'<i class="material-icons">print</i>' +
						//'</button>' +
						'<a class="dropdown-toggle btn-sm btn btn-info" data-toggle="dropdown" href="#" role="button" id="dropdownMenuLink" aria-haspopup="true" aria-expanded="false">' +
						'Actions <span class="caret"></span>' +
						'</a>' +
						'<ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">' +
						'<li><a href="#" data-toggle="modal" data-target="#exampleModalLongPreview" onclick="preview2(\'' + a[i].CheckNumber + '\', \'' + a[i].AccountNumber + '\', \'' + a[i].ID + '\')">Preview</a></li>' +
						'<li><a href="#">Details</a></li>' +
						'<li><a href="#">Mark as OK</a></li>' +
						'<li><a href="#" onclick="print2(\'' + a[i].CheckNumber + '\', \'' + a[i].AccountNumber + '\')">Print</a></li>' +
						'<li><a href="#" onclick="save2(\'' + a[i].CheckNumber + '\', \'' + a[i].AccountNumber + '\')">Download</a></li>' +
						'<li><a href="#">Memorize</a></li>' +
						'<li><a href="#">Repeat Costumer</a></li>' +
						'<li><a href="#">Ban this Check</a></li>' +
						'<li><a href="#">Delete</a></li>' +
						'</ul>' +
						'</td>' +
						'</tr>';
				}

			}
		}
	}).done(function () {
		$("#tblPrintedChecks").append(xstr);
	});
}

function addExpirationDate(dt) {
	var tt = timeConverter(dt);

	var date = new Date(tt);
	var newdate = new Date(date);


	newdate.setDate(newdate.getDate() + 180);

	var dd = newdate.getDate();
	var mm = newdate.getMonth() + 1;
	var y = newdate.getFullYear();

	var someFormattedDate = mm + '-' + dd + '-' + y;

	var datetoday = new Date();
	var expirationdate = new Date(someFormattedDate);
	if (datetoday > expirationdate) {
		return "true";
	} else {
		return "false";
	}
	//alert(someFormattedDate);
}

function getIdToDelete(id, checknumber) {
	$("#inDeleteID").val(id);
	$("#inDeleteCheckNumber").val(checknumber);
	$("#lblCheckNumber").html(checknumber);
}

function layout(layout, id, isgen) {
	if (isgen === true) {
		var txtCount = '<div class="form-group label-floating" id="divCheckCount' + id + '">' +
			'<label class="control-label"> Check Count(Batch) </label>' +
			'<input id="inBatchCount' + id + '" type="number" class="form-control" min="2" max="100" value="2" required/>' +
			'</div >'
		if (layout === "1") {
			return ' Top <input class="hidden" id="inLayout' + id + '" type="text" value="Top">';
		}
		else if (layout === "2") {
			return ' Middle <input class="hidden" id="inLayout' + id + '" type="text" value="Middle">';
		}
		else if (layout === "3") {

			return ' Bottom <input class="hidden" id="inLayout' + id + '" type="text" value="Bottom">';
		}
		else if (layout === "4") {
			return ' <input class="hidden" id="inLayout' + id + '" type="text" value="Batch">' + txtCount;
		}
	} else {
		var txtCount = '<div class="form-group label-floating hidden" id="divCheckCount' + id + '">' +
			'<label class="control-label"> Number of Checks (Batch)</label>' +
			'<input id="inBatchCount' + id + '" type="number" class="form-control" min="2" max="100" value="2" required/>' +
			'</div >';
		if (layout === "1") {
			return '<input type="button" id="btnTop' + id + '" class="btn btn-default btn-xs" value="Top" onclick="filllayout(' + id + ', \'1\'); onLayoutClick(this.id, ' + id + '); hideBatchInput(' + id + ');"><input type="button" id="btnMiddle' + id + '" class="btn btn-white btn-xs" value="Mid" onclick="filllayout(' + id + ', \'2\'); onLayoutClick(this.id, ' + id + '); hideBatchInput(' + id + ');"><input type="button" id="btnBottom' + id + '" class="btn btn-white btn-xs" value="Bot" onclick="filllayout(' + id + ', \'3\'); onLayoutClick(this.id, ' + id + '); hideBatchInput(' + id + ');"><input type="button" id="btnBatch' + id + '" class="btn btn-white btn-xs" value="Bat" onclick="filllayout(' + id + ', \'4\'); onLayoutClick(this.id, ' + id + ');"><input class="hidden" id="inLayout' + id + '" type="text" value="Top">' + txtCount;
		}
		else if (layout === "2") {
			return '<input type="button" id="btnTop' + id + '" class="btn btn-white btn-xs" value="Top" onclick="filllayout(' + id + ', \'1\'); onLayoutClick(this.id, ' + id + '); hideBatchInput(' + id + ');"><input type="button" id="btnMiddle' + id + '" class="btn btn-default btn-xs" value="Mid" onclick="filllayout(' + id + ', \'2\'); onLayoutClick(this.id, ' + id + '); hideBatchInput(' + id + ');"><input type="button" id="btnBottom' + id + '" class="btn btn-white btn-xs" value="Bot" onclick="filllayout(' + id + ', \'3\'); onLayoutClick(this.id, ' + id + '); hideBatchInput(' + id + ');"><input type="button" id="btnBatch' + id + '" class="btn btn-white btn-xs" value="Bat" onclick="filllayout(' + id + ', \'4\'); onLayoutClick(this.id, ' + id + ');"><input class="hidden" id="inLayout' + id + '" type="text" value="Middle">' + txtCount;
		}
		else if (layout === "3") {
			return '<input type="button" id="btnTop' + id + '" class="btn btn-white btn-xs" value="Top" onclick="filllayout(' + id + ', \'1\'); onLayoutClick(this.id, ' + id + '); hideBatchInput(' + id + ');"><input type="button" id="btnMiddle' + id + '" class="btn btn-white btn-xs" value="Mid" onclick="filllayout(' + id + ', \'2\'); onLayoutClick(this.id, ' + id + '); hideBatchInput(' + id + ');"><input type="button" id="btnBottom' + id + '" class="btn btn-default btn-xs" value="Bot" onclick="filllayout(' + id + ', \'3\'); onLayoutClick(this.id, ' + id + '); hideBatchInput(' + id + ');"><input type="button" id="btnBatch' + id + '" class="btn btn-white btn-xs" value="Bat" onclick="filllayout(' + id + ', \'4\'); onLayoutClick(this.id, ' + id + ');"><input class="hidden" id="inLayout' + id + '" type="text" value="Bottom">' + txtCount;
		}
		else if (layout === "4") {
			return '<input type="button" id="btnTop' + id + '" class="btn btn-white btn-xs" value="Top" onclick="filllayout(' + id + ', \'1\'); onLayoutClick(this.id, ' + id + '); hideBatchInput(' + id + ');"><input type="button" id="btnMiddle' + id + '" class="btn btn-white btn-xs" value="Mid" onclick="filllayout(' + id + ', \'2\'); onLayoutClick(this.id, ' + id + '); hideBatchInput(' + id + ');"><input type="button" id="btnBottom' + id + '" class="btn btn-white btn-xs" value="Bot" onclick="filllayout(' + id + ', \'3\'); onLayoutClick(this.id, ' + id + '); hideBatchInput(' + id + ');"><input type="button" id="btnBatch' + id + '" class="btn btn-default btn-xs" value="Bat" onclick="filllayout(' + id + ', \'4\'); onLayoutClick(this.id, ' + id + ');"><input class="hidden" id="inLayout' + id + '" type="text" value="Batch">' + txtCount;
		}
	}
}

function showBatchInput(id) {
	$("#divCheckCount" + id).removeClass("hidden");
}

function hideBatchInput(id) {
	$("#divCheckCount" + id).addClass("hidden");
}

function onLayoutClick(btnid, id) {
	$("#btnTop" + id).removeClass("btn-default");
	$("#btnTop" + id).removeClass("btn-white");
	$("#btnTop" + id).addClass("btn-white");
	$("#btnMiddle" + id).removeClass("btn-default");
	$("#btnMiddle" + id).removeClass("btn-white");
	$("#btnMiddle" + id).addClass("btn-white");
	$("#btnBottom" + id).removeClass("btn-default");
	$("#btnBottom" + id).removeClass("btn-white");
	$("#btnBottom" + id).addClass("btn-white");
	$("#btnBatch" + id).removeClass("btn-default");
	$("#btnBatch" + id).removeClass("btn-white");
	$("#btnBatch" + id).addClass("btn-white");

	$("#" + btnid).removeClass("btn-white");
	$("#" + btnid).addClass("btn-default");
}

function filllayout(id, layout, btnid) {
	if (layout === "1") {
		$("#inLayout" + id).val("Top");
	}
	else if (layout === "2") {
		$("#inLayout" + id).val("Middle");
	}
	else if (layout === "3") {
		$("#inLayout" + id).val("Bottom");
	}
	else if (layout === "4") {
		$("#inLayout" + id).val("Batch");
	}
}

function status(stat, isgen, isprint, dt) {
	if (stat === false) {
		if (isgen === true) {
			if (isprint === true) {
				return "Printed";
			} else {
				return "Ready to print";
			}
		} else {
			return "Ready to generate";
		}
	} else if (stat === true) {
		return "Memorized";
	}

}

function appendTableBodyMemorized() {
	var xstr = "";
	$.ajax({
		url: '/Dashboard/GetTopMemorizedChecks',
		type: 'POST',
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Loading all downloaded Checks...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {
			$("#tblMemorizedChecks").empty();
			for (i = 0; i < a.length; i++) {
				var xstrdate = a[i].Date;
				var amount = parseFloat(a[i].Amount).toFixed(2);
				xstr = xstr + '<tr>' +
					'<td>' + a[i].CheckNumber + '</td>' +
					'<td>' + a[i].BankName + '</td>' +
					'<td>' + a[i].PayerName + '</td>' +
					'<td>' + a[i].PayeeName + '</td>' +
					'<td> $' + amount + '</td>' +
					'<td>' + timeConverter(xstrdate) + '</td>' +
					'<td class="td-actions text-left" style="width:250px;">' +
					'<button type="button" rel="tooltip" data-toggle="modal" data-target="#exampleModalEditCheck" title="Edit Check" class="btn btn-info btn-simple btn-xs" onclick="edit(' + a[i].ID + ', \'top\');">' +
					'<i class="material-icons">mode_edit</i>' +
					'</button>' +
					'<button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs" onclick="unmemorizeCheck(' + a[i].ID + ', ' + a[i].CheckNumber + ', \'top\')">' +
					'<i class="material-icons">delete</i>' +
					'</button>' +
					'<button type="button" rel="tooltip" title="Save a copy" id="btnSaveMemorized' + a[i].ID + '" class="btn btn-info btn-simple btn-xs" onclick="save(' + a[i].ID + ', \'m\')">' +
					'<i class="material-icons">save</i>' +
					'</button>' +
					'<button type="button" rel="tooltip" title="Preview" id="btnPreviewMemorized' + a[i].ID + '" class="btn btn-info btn-simple btn-xs" onclick="preview(' + a[i].ID + ', \'m\')">' +
					'<i class="material-icons">visibility</i>' +
					'</button>' +
					'</td>' +
					'</tr>';
			}
		}
	}).done(function () {
		$("#tblMemorizedChecks").append(xstr);
	});
}

function timeConverter(createdAt) {
	var d = new Date(parseInt(createdAt.substr(6)));
	//return d.toLocaleDateString("en-US");
	return pad(d.getMonth() + 1, 2) + "-" + pad(d.getDate(), 2) + "-" + d.getFullYear();
}

function pad(str, max) {
	str = str.toString();
	return str.length < max ? pad("0" + str, max) : str;
}

function memorizeCheck(id, cnumber) {
	var xstr = "";
	$.ajax({
		url: '/Dashboard/MemorizeCheck',
		type: 'POST',
		data: { ID: id },
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Memorizing Check...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {
			xstr = a;
		}
	}).done(function () {
		if (xstr === "0") {
			demo.showNotification('top', 'center', 'Check number ' + cnumber + ' was successfully memorized!', 2);
			appendTableBody();
		} else {
			demo.showNotification('top', 'center', xstr, 4);
			appendTableBody();
		}
	});
}

function removeCheck(proc) {
	var id = $("#inDeleteID").val();
	var cnumber = $("#inDeleteCheckNumber").val();
	$.ajax({
		url: '/Dashboard/RemoveCheck',
		type: 'POST',
		data: { ID: id },
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Removing check...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function () {

		}
	}).done(function (a) {
		if (a === "0") {
			demo.showNotification('top', 'center', 'Check number ' + cnumber + ' was successfully deleted!', 2);
			if (proc === "all") {
				loadTableAllCreated();
			}
			appendTableBody();
		} else {
			demo.showNotification('top', 'center', a, 4);
			if (proc === "all") {
				loadTableAllCreated();
			}
			appendTableBody();
		}
	});
}

function removeCheck2(id, cnumber, proc) {
	$.ajax({
		url: '/Dashboard/RemoveCheck',
		type: 'POST',
		data: { ID: id },
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Removing check...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function () {

		}
	}).done(function (a) {
		if (a === "0") {
			demo.showNotification('top', 'center', 'Check number ' + cnumber + ' was successfully deleted!', 2);
			if (proc === "all") {
				loadTableAllCreated();
			}
			appendTableBody();
		} else {
			demo.showNotification('top', 'center', a, 4);
			if (proc === "all") {
				loadTableAllCreated();
			}
			appendTableBody();
		}
	});
}

function unmemorizeCheck(id, cnumber, proc) {
	var xstr = "";
	$.ajax({
		url: '/Dashboard/UnmemorizeCheck',
		type: 'POST',
		data: { ID: id },
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Unmemorizing a check...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {
			xstr = a;
		}
	}).done(function () {
		if (xstr === "0") {
			demo.showNotification('top', 'center', 'Check number ' + cnumber + ' was successfully unmemorized!', 2);
			if (proc === "top") {
				appendTableBodyMemorized();
			} else {
				appendTableBodyMemorized();
				loadTableAllMemorized();
			}

		} else {
			demo.showNotification('top', 'center', xstr, 3);
			if (proc === "top") {
				appendTableBodyMemorized();
			} else {
				appendTableBodyMemorized();
				loadTableAllMemorized();
			}
		}
	});
}

function GetJsonResult(data) {
	if (data.result === "1") {
		demo.showNotification('top', 'center', 'Check was successfully updated!', 2);
		$("#btnCancelEdit").click();
		if ($("#inProc").val() === "all") {
			loadTableAllMemorized();
		}
		appendTableBodyMemorized();

	} else {
		demo.showNotification('top', 'center', data.result, 4);
		$("#btnCancelEdit").click();
		if ($("#inProc").val() === "all") {
			loadTableAllMemorized();
		}
		appendTableBodyMemorized();

	}
}

function preview(id, proc, isgen) {
	//var not = demo.showNotificationDashboard('top', 'center', 'Previewing Check...', 2);
	var layout = $("#inLayout" + id).val();
	var batchcount = $("#inBatchCount" + id).val();
	var viewmodel = {};
	if (isgen === true) {
		$.ajax({
			url: '/Dashboard/PreviewCheck',
			type: 'POST',
			data: { id: id },
			dataType: 'json',
			beforeSend: function () {
				$('#divSpinner').removeClass("hidden");
				$('#lblLoading').html("Previewing a check...");
			},
			success: function (a) {
				for (i = 0; i < a.length; i++) {
					var xstrdate = a[i].Date;
					viewmodel = {
						CheckNumber: a[i].CheckNumber,
						PayerName: a[i].PayerName,
						PayerAddress: a[i].PayerAddress,
						PayerPhone: a[i].PayerPhone,
						BankName: a[i].BankName,
						BankAddress: a[i].BankAddress,
						BankCityStateZip: a[i].BankCityState,
						RoutingNumber: a[i].RoutingNumber,
						AccountNumber: a[i].AccountNumber,
						Amount: a[i].Amount,
						AmountInWords: a[i].AmountInWords,
						Date: timeConverter(xstrdate),
						PayeeName: a[i].PayeeName,
						PayeeAddress: a[i].Payee,
						Payee: a[i].Payee,
						Memo: a[i].Memo,
						CheckLayout: layout,
						BatchCount: batchcount,
						ProcessType: 'Preview Dashboard',
						PayeePhone: a[i].PayeePhone
					};
				}
			}
		}).done(function () {
			printCheck(id);
			var ci = viewmodel;
			$.ajax({
				url: '/Check/Pdf',
				type: 'POST',
				data: ci,
				dataType: 'json',
				complete: function () {
					$('#divSpinner').addClass("hidden");
					appendTableBody();

				},
				success: function (a) {

				}
			}).done(function (a) {
				window.open("/Check/Preview?file=" + a);
				getTotalCount();
				getNotification();
			});

		});
	} else {
		demo.showNotification('top', 'center', 'Please generate the check first!', 4);
	}

}

function save(id, proc) {
	var layout = $("#inLayout" + id).val();
	var batchcount = $("#inBatchCount" + id).val();
	var viewmodel = {};
	$.ajax({
		url: '/Dashboard/PreviewCheck',
		type: 'POST',
		data: { id: id },
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Downloading a check...");
		},
		success: function (a) {
			for (i = 0; i < a.length; i++) {
				var xstrdate = a[i].Date;
				viewmodel = {
					CheckNumber: a[i].CheckNumber,
					PayerName: a[i].PayerName,
					PayerAddress: a[i].PayerAddress,
					BankName: a[i].BankName,
					BankAddress: a[i].BankAddress,
					BankCityStateZip: a[i].BankCityState,
					RoutingNumber: a[i].RoutingNumber,
					AccountNumber: a[i].AccountNumber,
					Amount: a[i].Amount,
					AmountInWords: a[i].AmountInWords,
					Date: timeConverter(xstrdate),
					PayeeName: a[i].PayeeName,
					PayeeAddress: a[i].Payee,
					Payee: a[i].Payee,
					Memo: a[i].Memo,
					CheckLayout: layout,
					BatchCount: batchcount,
					ProcessType: 'Save Dashboard'
				};
			}
		}
	}).done(function () {
		var ci = viewmodel;
		$.ajax({
			url: '/Check/Pdf',
			type: 'POST',
			data: ci,
			dataType: 'json',
			complete: function () {
				$('#divSpinner').addClass("hidden");
			},
			success: function (a) {

			}
		}).done(function (a) {
			window.location.href = "/Check/Download?file=" + a;
		});

	});
}

function edit(id, proc) {
	$("#inProc").val(proc);
	var viewmodel = {};
	$.ajax({
		url: '/Dashboard/PreviewCheckForEdit',
		type: 'POST',
		data: { id: id },
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Filling out all fields...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {

		}
	}).done(function (a) {
		for (i = 0; i < a.length; i++) {
			var xstrdate = a[i].Date;
			$("#inCheckID").val(a[i].ID);
			$("#inCheckNumber").val(a[i].CheckNumber);
			$("#inPayerName").val(a[i].PayerName);
			$("#inAddress").val(a[i].PayerAddress);
			$("#inBankName").val(a[i].BankName);
			$("#inRoutingNumber").val(a[i].RoutingNumber);
			$("#inAccountNumber").val(a[i].AccountNumber);
			$("#inAmountInDigits").val(a[i].Amount);
			$("#inAmountInWords").val(a[i].AmountInWords);
			$("#inDate").val(timeConverter(xstrdate));
			$("#inPayeeName").val(a[i].PayeeName);
			$("#taDrawnToPayTo").val(a[i].Payee);
			$("#inMemo").val(a[i].Memo);
			$("#inLayout").val('Top');
			$("#inProcessType").val('Save');
			$("#taBankAddress").val(a[i].BankAddress + "\n" + a[i].BankCityState);
			$("#inBankAddressLine1").val(a[i].BankAddress);
			$("#inBankAddressLine2").val(a[i].BankCityState);

			if ($("#inCheckNumber").val() === "" || $("#inCheckNumber").val() === null) {
				$("#divCheckNumber").addClass("is-empty");
			} else {
				$("#divCheckNumber").removeClass("is-empty");
			}
			if ($("#inPayerName").val() === "" || $("#inPayerName").val() === null) {
				$("#divPayerName").addClass("is-empty");
			} else {
				$("#divPayerName").removeClass("is-empty");
			}
			if ($("#inAddress").val() === "" || $("#inAddress").val() === null) {
				$("#divAddress").addClass("is-empty");
			} else {
				$("#divAddress").removeClass("is-empty");
			}
			if ($("#inBankName").val() === "" || $("#inBankName").val() === null) {
				$("#divBankName").addClass("is-empty");
			} else {
				$("#divBankName").removeClass("is-empty");
			}
			if ($("#inRoutingNumber").val() === "" || $("#inRoutingNumber").val() === null) {
				$("#divRoutingNumber").addClass("is-empty");
			} else {
				$("#divRoutingNumber").removeClass("is-empty");
			}
			if ($("#inAccountNumber").val() === "" || $("#inAccountNumber").val() === null) {
				$("#divAccountNumber").addClass("is-empty");
			} else {
				$("#divAccountNumber").removeClass("is-empty");
			}
			if ($("#inAmountInDigits").val() === "" || $("#inAmountInDigits").val() === null) {
				$("#divAmountInDigits").addClass("is-empty");
			} else {
				$("#divAmountInDigits").removeClass("is-empty");
			}
			if ($("#inAmountInWords").val() === "" || $("#inAmountInWords").val() === null) {
				$("#divAmountInWords").addClass("is-empty");
			} else {
				$("#divAmountInWords").removeClass("is-empty");
			}
			if ($("#inDate").val() === "" || $("#inDate").val() === null) {
				$("#divDate").addClass("is-empty");
			} else {
				$("#divDate").removeClass("is-empty");
			}
			if ($("#inPayeeName").val() === "" || $("#inPayeeName").val() === null) {
				$("#divPayeeName").addClass("is-empty");
			} else {
				$("#divPayeeName").removeClass("is-empty");
			}
			if ($("#taDrawnToPayTo").val() === "" || $("#taDrawnToPayTo").val() === null) {
				$("#divDrawnToPayTo").addClass("is-empty");
			} else {
				$("#divDrawnToPayTo").removeClass("is-empty");
			}
			if ($("#inMemo").val() === "" || $("#inMemo").val() === null) {
				$("#divMemo").addClass("is-empty");
			} else {
				$("#divMemo").removeClass("is-empty");
			}
			if ($("#taBankAddress").val() === "" || $("#taBankAddress").val() === null) {
				$("#divBankAddress").addClass("is-empty");
			} else {
				$("#divBankAddress").removeClass("is-empty");
			}
		}
	});
}

function loadTableAllCreated() {
	if ($.fn.DataTable.isDataTable("#tblAllCreated")) {
		$('#tblAllCreated').DataTable().clear().destroy();
	}

	var xstr0 = "";
	var t = $('#tblAllCreated').DataTable({
		//"processing": true,
		dom: 'Bfrtip',
		"ajax": {
			"url": "/Dashboard/GetAllPrintedChecks",
			"dataSrc": ""
		},
		"autoWidth": false,
		"order": [[8, "desc"]],
		"columnDefs": [
			{ "visible": false, "targets": 0 },
			{ "visible": true, "title": "CHECK NUMBER", "targets": 1 },
			{ "visible": true, "title": "PAYER", "targets": 2 },
			{ "visible": true, "title": "PAYEE", "targets": 3 },
			{ "visible": true, "title": "ACCOUNT NUMBER", "targets": 4 },
			{ "visible": true, "title": "BANK", "targets": 5 },
			{ "visible": true, "title": "ROUTING NUMBER", "targets": 6 },
			{ "visible": true, "title": "AMOUNT", "targets": 7 },
			{ "visible": true, "title": "DATE CREATED", "targets": 8 },
			{ "visible": true, "title": "DATE ON CHECK", "targets": 9 },
			{ "visible": true, "title": "STATUS", "targets": 10 },
			{ className: "text-info", "targets": [10] }
			//{ "visible": true, "title": "", "targets": 10 },
			//{ className: "td-actions text-left", "targets": [10] }
		],
		"columns": [
			{ "data": "ID" },
			{ "data": "CheckNumber" },
			{ "data": "PayerName" },
			{ "data": "PayeeName" },
			{ "data": "AccountNumber" },
			{ "data": "BankName" },
			{ "data": "RoutingNumber" },
			{ "data": "Amount" },
			{
				"render": function (data, type, full, meta) {
					var d = full.DateCreated;
					return timeConverter(d);
				}
			},
			{
				"render": function (data, type, full, meta) {
					var d2 = full.Date;
					return timeConverter(d2);
				}
			},
			{
				"render": function (data, type, full, meta) {
					var str = "";
					if (addExpirationDate(full.Date) === "true") {
						str = "Expired";
						xstr0 = "text-danger";
					} else {
						xstr0 = "text-info";
						if (full.IsMemorized === false) {
							if (full.IsGenerated === true) {
								if (full.IsPrinted === true) {
									str = "Printed";
								} else {
									str = "Generated";
								}
							} else {
								str = "Created";
							}
						} else if (full.IsMemorized === true) {
							str = "Memorized";
						}
					}
					return str;
				}, className: xstr0
			},
			{
				"render": function (data, type, full, meta) {
					var ID = full.ID;
					var cn = full.CheckNumber;
					if (full.IsMemorized === true) {
						xmemorized = "btn-default";
						ymemorized = "Tagged as Memorized";
					} else {
						xmemorized = "btn-info";
						ymemorized = "Memorize";
					}
					//return '<button type="button" rel="tooltip" title="Edit" class="btn btn-info btn-xs btn-simple" id="btnEditPayee' + ID + '" onclick="getSelectedPayee(' + ID + ', this.id)" data-toggle="modal" data-target="#exampleModalLong">' +
					//	'<i class="material-icons">edit</i>' +
					//	'</button>' +
					//	'<button type="button" rel="tooltip" title="Delete" class="btn btn-danger btn-xs btn-simple" id="btnDelete' + ID + '" onclick="getSelectedPayee(' + ID + ', this.id)" data-toggle="modal" data-target="#exampleModalLong">' +
					//	'<i class="material-icons">delete</i>' +
					//	'</button>';
					//return '<button type="button" rel="tooltip" title="' + ymemorized + '" class="btn ' + xmemorized + ' btn-simple btn-xs" id="btnMemorizeDashboard' + ID + '" onclick="memorizeCheckAll(' + ID + ', ' + cn + ')">' +
					//	'<i class="material-icons">star</i>' +
					//	'</button>' +
					//	'<button type="button" rel="tooltip" title="Save a copy" id="btnSaveDashboard' + ID + '" class="btn btn-info btn-simple btn-xs" onclick="save(' + ID + ', \'d\')">' +
					//	'<i class="material-icons">save</i>' +
					//	'</button>' +

					return '<button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs" onclick="removeCheck2(' + ID + ', ' + cn + ', \'all\')">' +
						'<i class="material-icons">delete</i>' +
						'</button>';
					//'<button type="button" rel="tooltip" title="Preview" id="btnPreviewDashboard' + ID + '" class="btn btn-info btn-simple btn-xs" onclick="preview(' + ID + ', \'d\')">' +
					//'<i class="material-icons">visibility</i>' +
					//'</button>';
				}
			}
		],
		buttons: [
			{
				text: '<i class="fa fa-print"></i> Printed',
				className: 'btn btn-white btn-sm',
				action: function (e, dt, node, config) {
					t.search("Printed").draw();
				},
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				text: '<i class="fa fa-columns"></i> Created',
				className: 'btn btn-white btn-sm',
				action: function (e, dt, node, config) {
					t.search("Created").draw();
				},
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				text: '<i class="fa fa-angle-double-right"></i> Generated',
				className: 'btn btn-white btn-sm',
				action: function (e, dt, node, config) {
					t.search("Generated").draw();
				},
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				text: '<i class="fa fa-ban"></i> Expired',
				className: 'btn btn-white btn-sm',
				action: function (e, dt, node, config) {
					t.search("Expired").draw();
				},
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				text: '<i class="fa fa-times"></i> Clear Filter',
				className: 'btn btn-white btn-sm',
				action: function (e, dt, node, config) {
					t.search("").draw();
				},
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			}
		],
		"initComplete": function (settings, json) {
			$(this).removeClass("dataTable");
		}
	});
}

function loadTableAllMemorized() {
	if ($.fn.DataTable.isDataTable("#tblAllMemorized")) {
		$('#tblAllMemorized').DataTable().clear().destroy();
	}

	var t = $('#tblAllMemorized').DataTable({
		//"processing": true,
		dom: 'frtip',
		"ajax": {
			"url": "/Dashboard/GetAllMemorizedChecks",
			"dataSrc": ""
		},
		"autoWidth": false,
		"order": [[8, "desc"]],
		"columnDefs": [
			{ "visible": false, "targets": 0 },
			{ "visible": true, "title": "CHECK NUMBER", "targets": 1 },
			{ "visible": true, "title": "PAYER", "targets": 2 },
			{ "visible": true, "title": "PAYEE", "targets": 3 },
			{ "visible": true, "title": "ACCOUNT NUMBER", "targets": 4 },
			{ "visible": true, "title": "BANK", "targets": 5 },
			{ "visible": true, "title": "ROUTING NUMBER", "targets": 6 },
			{ "visible": true, "title": "AMOUNT", "targets": 7 },
			{ "visible": true, "title": "DATE CREATED", "targets": 8 },
			{ "visible": true, "title": "", "targets": 9 },
			{ className: "td-actions text-left", "targets": [9] }
		],
		"columns": [
			{ "data": "ID" },
			{ "data": "CheckNumber" },
			{ "data": "PayerName" },
			{ "data": "PayeeName" },
			{ "data": "AccountNumber" },
			{ "data": "BankName" },
			{ "data": "RoutingNumber" },
			{ "data": "Amount" },
			{
				"render": function (data, type, full, meta) {
					var d = full.Date;
					return timeConverter(d);
				}
			},
			{
				"render": function (data, type, full, meta) {
					var ID = full.ID;
					var cn = full.CheckNumber;
					if (full.IsMemorized === true) {
						xmemorized = "btn-default";
						ymemorized = "Tagged as Memorized";
					} else {
						xmemorized = "btn-info";
						ymemorized = "Memorize";
					}
					return '<button type="button" rel="tooltip" data-toggle="modal" data-target="#exampleModalEditCheck" title="Edit Check" class="btn btn-info btn-simple btn-xs" onclick="edit(' + ID + ', \'all\');">' +
						'<i class="material-icons">mode_edit</i>' +
						'</button>' +
						'<button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs" onclick="unmemorizeCheck(' + ID + ', ' + cn + ', \'all\')">' +
						'<i class="material-icons">delete</i>' +
						'</button>' +
						'<button type="button" rel="tooltip" title="Save a copy" id="btnSaveMemorized' + ID + '" class="btn btn-info btn-simple btn-xs" onclick="save(' + ID + ', \'m\')">' +
						'<i class="material-icons">save</i>' +
						'</button>' +
						'<button type="button" rel="tooltip" title="Preview" id="btnPreviewMemorized' + ID + '" class="btn btn-info btn-simple btn-xs" onclick="preview(' + ID + ', \'m\')">' +
						'<i class="material-icons">visibility</i>' +
						'</button>';
				}
			}
		]
	});
}

function memorizeCheckAll(id, cnumber) {
	var xstr = "";
	$.ajax({
		url: '/Dashboard/MemorizeCheck',
		type: 'POST',
		data: { ID: id },
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Memorizing Check...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {
			xstr = a;
		}
	}).done(function () {
		if (xstr === "0") {
			demo.showNotification('top', 'center', 'Check number ' + cnumber + ' was successfully memorized!', 2);
			loadTableAllCreated();
			appendTableBody();
		} else {
			demo.showNotification('top', 'center', xstr, 4);
			loadTableAllCreated();
			appendTableBody();
		}
	});
}

function getTotalAmountThisMonth() {
	$.ajax({
		url: '/Dashboard/GetAmountThisMonth',
		type: 'POST',
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {
		$("#h3TotalAmountThisMonth").html("$" + a);
	});
}

function getTotalChecksThisMonth() {
	$.ajax({
		url: '/Dashboard/GetPrintedThisMonth',
		type: 'POST',
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {
		$("#h3TotalChecksThisMonth").html(a + "<small> Checks</small>");
	});
}

function getTotalChecks() {
	$.ajax({
		url: '/Dashboard/GetPrinted',
		type: 'POST',
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {
		$("#h3TotalChecks").html(a + "<small> Checks</small>");
	});
}

function getTotalAmount() {
	$.ajax({
		url: '/Dashboard/GetTotalAmount',
		type: 'POST',
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {
		$("#h3TotalAmount").html("$" + a);
	});
}

function loadBank() {
	if ($.fn.DataTable.isDataTable("#tblBankChecks")) {
		$('#tblBankChecks').DataTable().clear().destroy();
	}

	var t = $('#tblBankChecks').DataTable({
		//"processing": true,
		//dom: 'Bfrtip',	
		"ajax": {
			"url": "/Dashboard/GetAllBank",
			"dataSrc": ""
		},
		autoWidth: false,
		"order": [[1, "asc"]],
		"columnDefs": [
			{ "visible": false, "targets": 0 },
			{ "visible": true, "title": "BANK NAME", "targets": 1 },
			{ "visible": true, "title": "ADDRESS", "targets": 2 },
			{ "visible": true, "title": "CITY/STATE/ZIPCODE", "targets": 3 },
			{ "visible": true, "title": "ROUTING NUMBER", "targets": 4 },
			{ "visible": true, "title": "SELECT", "targets": 5 }
		],
		"columns": [
			{ "data": "ID" },
			{ "data": "BankName" },
			{ "data": "BankAddress" },
			{ "data": "BankCityStateZip" },
			{ "data": "RoutingNumber" },
			{
				"render": function (data, type, full, meta) {
					var ID = full.ID;
					var bank = full.BankName;
					var bankaddress = full.BankAddress;
					var bankcitystate = full.BankCityStateZip;
					var routing = full.RoutingNumber;
					return '<button type="button" rel="tooltip" title="Choose" class="btn btn-info btn-xs btn-simple" id="btnChoose' + ID + '" onclick="selectBank(\'' + bank + '\', \'' + bankaddress + '\', \'' + bankcitystate + '\', \'' + routing + '\')">' +
						'<i class="material-icons">check_circle</i>' +
						'</button>';
				}
			}
		]
	});
}

function selectBank(bank, bankaddress, bankcitystate, routing) {
	$('#inBankName').val(bank);
	$('#taBankAddress').val(bankaddress + "\n" + bankcitystate);
	$('#inBankAddressLine1').val(bankaddress);
	$('#inBankAddressLine2').val(bankcitystate);
	$('#inRoutingNumber').val(routing);


	if (bank === "" || bank === null) {
		$("#divBankName").addClass("is-empty");
	} else {
		$("#divBankName").removeClass("is-empty");
	}


	if (bankaddress === "" || bankaddress === null) {
		$("#divBankAddress").addClass("is-empty");
	} else {
		$("#divBankAddress").removeClass("is-empty");
	}

	if (routing === "" || routing === null) {
		$("#divRoutingNumber").addClass("is-empty");
	} else {
		$("#divRoutingNumber").removeClass("is-empty");
	}

	$('#btnCloseBank').click();
}

function getSubscription() {
	$.ajax({
		url: '/Layout/GetSubscription',
		type: 'POST',
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {
		if (a === 0) {
			$(".tdLayout").addClass("disableclick");
		} else {
			$(".tdLayout").removeClass("disableclick");
		}
	});
}



function generateCheck(id, isgen, isprint) {
	var layout = $("#inLayout" + id).val();
	if (isgen === false) {
		$.ajax({
			url: '/Dashboard/GenerateCheck',
			type: 'POST',
			data: { id: id, layout: layout },
			dataType: 'json',
			success: function (a) {

			}
		}).done(function (a) {
			if (a === "1") {
				demo.showNotification('top', 'center', 'Check successfully generated and can now be printed!', 2);
			} else {
				demo.showNotification('top', 'center', a, 4);
			}
			appendTableBody();
			getTotalCount();
			getNotification();
		});
	} else {
		if (isprint === true) {
			demo.showNotification('top', 'center', 'This check is already generated and printed!', 4);
		} else {
			demo.showNotification('top', 'center', 'This check is already generated and is ready to be printed!', 4);
		}

	}
}

function printCheck(id) {
	$.ajax({
		url: '/Dashboard/PrintCheck',
		type: 'POST',
		data: { id: id },
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {
		//if (a === "1") {
		//	demo.showNotification('top', 'center', 'Check successfully generated and can now be printed!', 2);
		//} else {
		//	demo.showNotification('top', 'center', a, 4);
		//}
	});
	//else {
	//	demo.showNotification('top', 'center', 'This check is already generated and is ready to be printed!', 4);
	//}
}


function deleteImage(file) {
	$.ajax({
		url: '/Check/Preview',
		type: 'GET',
		data: { file: file },
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {

	});
}

function getLayout(str) {
	if (str === "1") {
		return "Top";
	} else if (str === "2") {
		return "Middle";
	} else if (str === "3") {
		return "Bottom";
	} else if (str === "4") {
		return "Batch";
	}
}

function preview2(cnumber, accnum, id) {
	var batchcount = $("#inBatchCount" + id).val();
	var viewmodel = {};
	$.ajax({
		url: '/Check/PreviewCheck',
		type: 'POST',
		data: { cnumber: cnumber, accnum: accnum },
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Previewing...");
			//$("#modalPreviewContent").empty();
			//$('#modalPreviewContent object').attr('data', '');
			document.getElementById("imgPreview").src = '';
		},
		success: function (a) {
			for (i = 0; i < a.length; i++) {
				var xstrdate = a[i].Date;
				viewmodel = {
					CheckNumber: a[i].CheckNumber,
					PayerName: a[i].PayerName,
					PayerAddress: a[i].PayerAddress,
					PayerPhone: a[i].PayerPhone,
					BankName: a[i].BankName,
					BankAddress: a[i].BankAddress,
					BankCityStateZip: a[i].BankCityState,
					RoutingNumber: a[i].RoutingNumber,
					AccountNumber: a[i].AccountNumber,
					Amount: a[i].Amount,
					AmountInWords: a[i].AmountInWords,
					Date: timeConverter(xstrdate),
					PayeeName: a[i].PayeeName,
					PayeeAddress: a[i].Payee,
					Payee: a[i].Payee,
					Memo: a[i].Memo,
					BatchCount: batchcount,
					CheckLayout: getLayout(a[i].CheckLayout),
					ProcessType: 'Preview Dashboard'
				};
			}
		}
	}).done(function () {
		var ci = viewmodel;
		var ss;
		$.ajax({
			url: '/Check/Pdf',
			type: 'POST',
			data: ci,
			dataType: 'json',
			complete: function (a) {
				$('#divSpinner').addClass("hidden");
				deleteImage(ss);				
			},
			success: function (a) {

			}
		}).done(function (a) {
			ss = a;
			//$("#modalPreviewContent").append('<object data="https://localhost:44335/Files/' + a + '" type="image/jpeg" style="width: 100%;"></object >');
			//$('#modalPreviewContent object').attr('data', 'https://localhost:44335/Files/' + a);
			document.getElementById("imgPreview").src = '/Files/' + a + '?t=' + new Date().getTime();
		});

	});
}



function save2(cnumber, accnum) {
	var viewmodel = {};
	$.ajax({
		url: '/Check/PreviewCheck',
		type: 'POST',
		data: { cnumber: cnumber, accnum: accnum },
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Downloading...");
		},
		success: function (a) {
			for (i = 0; i < a.length; i++) {
				var xstrdate = a[i].Date;
				viewmodel = {
					CheckNumber: a[i].CheckNumber,
					PayerName: a[i].PayerName,
					PayerAddress: a[i].PayerAddress,
					PayerPhone: a[i].PayerPhone,
					BankName: a[i].BankName,
					BankAddress: a[i].BankAddress,
					BankCityStateZip: a[i].BankCityState,
					RoutingNumber: a[i].RoutingNumber,
					AccountNumber: a[i].AccountNumber,
					Amount: a[i].Amount,
					AmountInWords: a[i].AmountInWords,
					Date: timeConverter(xstrdate),
					PayeeName: a[i].PayeeName,
					PayeeAddress: a[i].Payee,
					Payee: a[i].Payee,
					Memo: a[i].Memo,
					BatchCount: a[i].BatchCount,
					CheckLayout: getLayout(a[i].CheckLayout),
					ProcessType: 'Save Dashboard'
				};
			}
		}
	}).done(function () {
		var ci = viewmodel;
		$.ajax({
			url: '/Check/Pdf',
			type: 'POST',
			data: ci,
			dataType: 'json',
			complete: function () {
				$('#divSpinner').addClass("hidden");
				//demo.showNotification('top', 'center', 'Download finished!', 2);
			},
			success: function (a) {

			}
		}).done(function (a) {
			window.location.href = "/Check/Download?file=" + a;
		});
	});
}

function print2(cnumber, accnum) {
	var viewmodel = {};
	$.ajax({
		url: '/Check/PreviewCheck',
		type: 'POST',
		data: { cnumber: cnumber, accnum: accnum },
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Processing...");
		},
		success: function (a) {
			for (i = 0; i < a.length; i++) {
				var xstrdate = a[i].Date;
				viewmodel = {
					CheckNumber: a[i].CheckNumber,
					PayerName: a[i].PayerName,
					PayerAddress: a[i].PayerAddress,
					PayerPhone: a[i].PayerPhone,
					BankName: a[i].BankName,
					BankAddress: a[i].BankAddress,
					BankCityStateZip: a[i].BankCityState,
					RoutingNumber: a[i].RoutingNumber,
					AccountNumber: a[i].AccountNumber,
					Amount: a[i].Amount,
					AmountInWords: a[i].AmountInWords,
					Date: timeConverter(xstrdate),
					PayeeName: a[i].PayeeName,
					PayeeAddress: a[i].Payee,
					Payee: a[i].Payee,
					Memo: a[i].Memo,
					BatchCount: a[i].BatchCount,
					CheckLayout: getLayout(a[i].CheckLayout),
					ProcessType: 'Save Dashboard'
				};
			}
		}
	}).done(function () {
		var ci = viewmodel;
		var ss;
		$.ajax({
			url: '/Check/Pdf',
			type: 'POST',
			data: ci,
			dataType: 'json',
			complete: function (a) {
				$('#divSpinner').addClass("hidden");
				deleteImage(ss);
			},
			success: function (a) {

			}
		}).done(function (a) {
			ss = a;
			printJS('/Files/' + a);
			//$("#modalPreviewContent").append('<object data="/Files/' + a + '" type="image/jpeg" style="width: 100%;"></object >');
		});

	});
}