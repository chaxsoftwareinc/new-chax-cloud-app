﻿$(document).ready(function () {
	/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
	getMessage();
	var dropdown = document.getElementsByClassName("dropdown-btn");
	var i;

	for (i = 0; i < dropdown.length; i++) {
		var dropdownContent = dropdown[i].nextElementSibling;
		dropdownContent.style.display = "block";
	}

	$("#btnFormMessages").removeClass("btn-white");
	$("#btnFormMessages").addClass("btn-info");
});


function addMessage() {
	var xstr = "";
	var msg = $("#taMessage").val();
	$.ajax({
		url: '/FormMessages/AddMessage',
		type: 'POST',
		dataType: 'json',
		data: { 'message': msg },
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Updating message...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
			getMessage();
		},
		success: function (a) {

		}
	}).done(function (a) {
		if (a === "1") {
			demo.showNotification('top', 'center', 'Message updated successfully!', 2);
		} else if (a === "0") {
			demo.showNotification('top', 'center', 'User doesnt exist!', 3);
		} else {
			demo.showNotification('top', 'center', a, 4);
		}
	});
}

function getMessage() {
	var xstr = "";
	var msg = $("#taMessage").val();
	$.ajax({
		url: '/FormMessages/GetMessage',
		type: 'POST',
		dataType: 'json',
		data: { 'message': msg },
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Retrieving message...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {

		}
	}).done(function (a) {
		$("#taMessage").val(a);
		$("#divMessage").removeClass("is-empty");
	});
}