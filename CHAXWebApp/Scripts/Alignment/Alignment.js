﻿$(document).ready(function () {
	/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
	var dropdown = document.getElementsByClassName("dropdown-btn");
	var i;

	for (i = 0; i < dropdown.length; i++) {
		var dropdownContent = dropdown[i].nextElementSibling;
		dropdownContent.style.display = "block";
	}

	$("#btnAlignment").removeClass("btn-white");
	$("#btnAlignment").addClass("btn-info");
	getalignment();
});


function getLayout(str) {
	if (str === "1") {
		return "Top";
	} else if (str === "2") {
		return "Middle";
	} else if (str === "3") {
		return "Bottom";
	} else if (str === "4") {
		return "Batch";
	}
}

function saveAlignment() {
	var xstr = "";
	var checkhor = $("#inCheckHor").val();
	var checkver = $("#inCheckVer").val();
	var micrhor = $("#inMICRHor").val();
	var micrver = $("#inMICRVer").val();

	$.ajax({
		url: '/Alignment/SaveAlignment',
		type: 'POST',
		dataType: 'json',
		data: { 'checkhor': checkhor, 'checkver': checkver, 'micrhor': micrhor, 'micrver': micrver },
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Saving...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
			getalignment();
		},
		success: function (a) {

		}
	}).done(function (a) {
		if (a === "1") {
			demo.showNotification('top', 'center', 'Data successfully saved!', 2);
		} else if (a === "0") {
			demo.showNotification('top', 'center', 'Username doesnt exist!', 3);
		} else {
			demo.showNotification('top', 'center', a, 4);
		}
	});
}


function testpreview2(cnumber, accnum) {
	var viewmodel = {};
	var checkhor = $("#inCheckHor").val(); 
	var micrhor = $("#inMICRHor").val();
	var micrver = $("#inMICRVer").val();
	$.ajax({
		url: '/Check/PreviewCheckTest',
		type: 'POST',
		data: { cnumber: cnumber, accnum: accnum },
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Previewing...");
			//$("#modalPreviewContent").empty();
			//$('#modalPreviewContent object').attr('data', '');
			document.getElementById("imgPreviewTest").src = '';
		},
		success: function (a) {
			for (i = 0; i < a.length; i++) {
				var xstrdate = a[i].Date;
				viewmodel = {
					CheckNumber: a[i].CheckNumber,
					PayerName: a[i].PayerName,
					PayerAddress: a[i].PayerAddress,
					PayerPhone: a[i].PayerPhone,
					BankName: a[i].BankName,
					BankAddress: a[i].BankAddress,
					BankCityStateZip: a[i].BankCityState,
					RoutingNumber: a[i].RoutingNumber,
					AccountNumber: a[i].AccountNumber,
					Amount: a[i].Amount,
					AmountInWords: a[i].AmountInWords,
					Date: timeConverter(xstrdate),
					PayeeName: a[i].PayeeName,
					PayeeAddress: a[i].Payee,
					Payee: a[i].Payee,
					Memo: a[i].Memo,
					BatchCount: a[i].BatchCount,
					CheckLayout: getLayout(a[i].CheckLayout),
					ProcessType: 'Preview Dashboard',
					Test: 'test',
					CheckHor: checkhor,
					CheckVer: '0',
					MICRHor: micrhor,
					MICRVer: micrver
				};
			}
		}
	}).done(function () {
		var ci = viewmodel;
		var ss;
		$.ajax({
			url: '/Check/Pdf',
			type: 'POST',
			data: ci,
			dataType: 'json',
			complete: function (a) {
				$('#divSpinner').addClass("hidden");
				deleteImage(ss);				
			},
			success: function (a) {

			}
		}).done(function (a) {
			ss = a;
			document.getElementById("imgPreviewTest").src = '/Files/' + a + '?t=' + new Date().getTime();
		});

	});
}

function getalignment() {
	var xstr = "";
	$.ajax({
		url: '/Alignment/GetAlignment',
		type: 'POST',
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Getting data...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");		
		},
		success: function (a) {
			$("#inCheckHor").val(a.CheckHor);
			$("#inCheckVer").val(a.CheckVer);
			$("#inMICRHor").val(a.MICRHor);
			$("#inMICRVer").val(a.MICRVer);
		}
	}).done(function () {
		$("#tblAllRequestsBody").append(xstr);
	});
}

function deleteImage(file) {
	$.ajax({
		url: '/Check/Preview',
		type: 'GET',
		data: { file: file },
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {

	});
}
