﻿var $j = jQuery.noConflict();

$(document).ready(function () {
	triggerActive("liPayee"); //setting the class active for sidebar

	loadPayee();

	$("textarea")
		.on('keypress', function (event) {
			var textarea = $(this),
				text = textarea.val(),
				numberOfLines = (text.match(/\n/g) || []).length + 1,
				maxRows = parseInt(textarea.attr('rows'));

			if (event.which === 13 && numberOfLines === maxRows) {
				return false;
			}
		});

	$('#payeeFullAddress').focusout(function () {
		var avalue = $('#payeeFullAddress').val().trim();
		var newVal = avalue.replace(/^\s*[\r\n]/gm, '');
		//var finalResults = newVal.replace("\n", "");
		$('#payeeFullAddress').val(newVal);
	});
});

function loadTable() {
	if ($.fn.DataTable.isDataTable("#tblPayee")) {
		$('#tblPayee').DataTable().clear().destroy();
	}

	var t = $('#tblPayee').DataTable({
		//"processing": true,
		dom: 'Bfrtip',
		"ajax": {
			"url": "/Payee/GetAllPayee",
			"dataSrc": ""
		},
		"autoWidth": false,
		"order": [[4, "desc"]],
		"columnDefs": [
			{ "visible": false, "targets": 0 },
			{ "visible": true, "title": "NAME", "targets": 1 },
			{ "visible": true, "title": "ADDRESS", "targets": 2 },
			{ "visible": true, "title": "PHONE NUMBER", "targets": 3 },
			{ "visible": true, "title": "LAST DATE USED", "targets": 4 },
			{ "visible": true, "title": "", "targets": 5 },
			{ className: "td-actions text-left", "targets": [5] }
		],
		"columns": [
			{ "data": "ID" },
			{ "data": "Name" },
			{
				"render": function (data, type, full, meta) {
					return full.AddressLine1 + " " + full.AddressLine2 + " " + full.CityState + " " + full.ZipCode;
				}
			},
			{ "data": "PhoneNumber" },
			{
				"render": function (data, type, full, meta) {
					var d = full.LastDateUsed;
					return timeConverter(d);
				}
			},
			{
				"render": function (data, type, full, meta) {
					var ID = full.ID;
					return '<button type="button" rel="tooltip" title="Edit" class="btn btn-info btn-xs btn-simple" id="btnEditPayee' + ID + '" onclick="getSelectedPayee(' + ID + ', this.id)" data-toggle="modal" data-target="#exampleModalLong">' +
						'<i class="material-icons">edit</i>' +
						'</button>' +
						'<button type="button" rel="tooltip" title="Delete" class="btn btn-danger btn-xs btn-simple" id="btnDelete' + ID + '" onclick="getSelectedPayee(' + ID + ', this.id)" data-toggle="modal" data-target="#exampleModalLong">' +
						'<i class="material-icons">delete</i>' +
						'</button>';
				}
			}
		],
		"initComplete": function (settings, json) {
			$(this).removeClass("dataTable");
		},
		"buttons": [
			{
				"extend": 'copy',
				"text": '<i class="fa fa-edit"></i> Copy', 
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				"extend": 'excel',
				"text": '<i class="fa fa-file-excel-o"></i> Excel',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				"extend": 'csv',
				"text": '<i class="fa fa-database"></i> CSV',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				"extend": 'pdf',
				"text": '<i class="fa fa-file-excel-o"></i> PDF',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			},
			{
				"extend": 'print',
				"text": '<i class="fa fa-print"></i> Print',
				"className": 'btn btn-white btn-sm',
				init: function (api, node, config) {
					$(node).removeClass('dt-button');
				}
			}
		]
		//buttons: [
		//	{
		//		text: 'Add Payee',
		//		className: 'btn',
		//		action: function (e, dt, node, config) {
		//			$('#payeeID').val('0');
		//			$('#payeeName').val('');
		//			$('#divName').addClass('is-empty');
		//			$('#payeeAddressLine1').val('');
		//			$('#divAddressLine1').addClass('is-empty');
		//			$('#payeeAddressLine2').val('');
		//			$('#divAddressLine2').addClass('is-empty');
		//			$('#payeeCityState').val('');
		//			$('#divCityState').addClass('is-empty');
		//			$('#payeePostalCode').val('');
		//			$('#divPostalCode').addClass('is-empty');
		//			$('#payeePhone').val('');
		//			$('#divPhone').addClass('is-empty');
		//			$('#payeeMemo').val('');
		//			$('#divMemo').addClass('is-empty');
		//			$("#btnAddPayee").click();
		//			$("#h4PayeeModal").text("Add Payee");
		//			$("#h4SubPayeeModal").text("Enter Payee's information");
		//			$("#btnSave1").text("Add");
		//		}
		//	}
		//]
	});
}

function loadPayee() {
	var xstr = "";
	$.ajax({
		url: '/Payee/GetTopPayee',
		type: 'POST',
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Loading all Payees...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {
			$("#tblPayeeBody").empty();
			for (i = 0; i < a.length; i++) {
				var xstrdate = a[i].LastDateUsed;
				var amount = parseFloat(a[i].Amount).toFixed(2);
				xstr = xstr + '<tr>' +
					'<td>' + a[i].Name + '</td>' +
					'<td>' + a[i].AddressLine1 + " " + a[i].AddressLine2 + " " + a[i].CityState + " " + a[i].ZipCode + '</td>' +
					'<td>' + a[i].PhoneNumber + '</td>' +
					'<td>' + timeConverter(xstrdate) + '</td>' +
					'</tr>';
			}
		}
	}).done(function () {
		$("#tblPayeeBody").append(xstr);
	});
}

$("#frmInfo").submit(function (event) {
	confirmSave();
	event.preventDefault();
});

function removeLines() {
	var avalue = $('#payeeFullAddress').val().trim();
	var newVal = avalue.replace(/^\s*[\r\n]/gm, '');
	//var finalResults = newVal.replace("\n", "");
	$('#payeeFullAddress').val(newVal);
}
function getSelectedPayee(id, type) {

	if (type.includes('Edit')) {
		$("#h4PayeeModal").text("Edit Payee's information");
		//$("#h4SubPayeeModal").text("Edit Payee's information");
		$("#btnSave1").text("Update");
	} else if (type.includes('Delete')) {
		$("#h4PayeeModal").text("Delete Payee");
		$("#h4SubPayeeModal").text("You sure you want to delete the selected payee?");
		$("#btnSave1").text("Delete");
	}

	$.ajax({
		url: '/Payee/GetSpecificPayee',
		type: 'POST',
		data: { 'id': id },
		dataType: 'json',
		success: function (a) {

		}
	}).done(function (a) {
		$('#payeeID').val(a.ID);
		$('#payeeNameID').val(a.Name);
		$('#payeeName').val(a.Name);
		$('#payeeAddressLine1').val(a.AddressLine1);
		$('#payeeAddressLine2').val(a.AddressLine2);
		$('#payeeCityState').val(a.CityState);
		$('#payeePostalCode').val(a.ZipCode);
		$('#payeePhone').val(a.PhoneNumber);
		$('#payeeAddress').val(a.Address);
		$('#payeeMemo').val(a.Memo);
		$('#payeeFullAddress').val(a.AddressLine1 + "\n" + a.AddressLine2 + "\n" + a.CityState + "\n" + a.ZipCode);

		$("#divPayeeFullAddress").removeClass("is-empty");

		if (a.Name === "" || a.Name === null) {
			$("#divName").addClass("is-empty");
		} else {
			$("#divName").removeClass("is-empty");
		}
		if (a.Address === "" || a.Address === null) {
			$("#divAddress").addClass("is-empty");
		} else {
			$("#divAddress").removeClass("is-empty");
		}
		if (a.AddressLine1 === "" || a.AddressLine1 === null) {
			$("#divAddressLine1").addClass("is-empty");
		} else {
			$("#divAddressLine1").removeClass("is-empty");
		}
		if (a.AddressLine2 === "" || a.AddressLine2 === null) {
			$("#divAddressLine2").addClass("is-empty");
		} else {
			$("#divAddressLine2").removeClass("is-empty");
		}
		if (a.CityState === "" || a.CityState === null) {
			$("#divCityState").addClass("is-empty");
		} else {
			$("#divCityState").removeClass("is-empty");
		}
		if (a.ZipCode === "" || a.ZipCode === null) {
			$("#divPostalCode").addClass("is-empty");
		} else {
			$("#divPostalCode").removeClass("is-empty");
		}
		if (a.PhoneNumber === "" || a.PhoneNumber === null) {
			$("#divPhone").addClass("is-empty");
		} else {
			$("#divPhone").removeClass("is-empty");
		}
		if (a.Memo === "" || a.Memo === null) {
			$("#divMemo").addClass("is-empty");
		} else {
			$("#divMemo").removeClass("is-empty");
		}
		removeLines();
	});
}

function timeConverter(createdAt) {
	var d = new Date(parseInt(createdAt.substr(6)));
	//return d.toLocaleDateString("en-US");
	return pad(d.getMonth() + 1, 2) + "-" + pad(d.getDate(), 2) + "-" + d.getFullYear();
}

function pad(str, max) {
	str = str.toString();
	return str.length < max ? pad("0" + str, max) : str;
}

function confirmSave() {
	var ID = $('#payeeID').val();
	var nameid = $('#payeeNameID').val();
	var name = $('#payeeName').val();
	var address1 = $('#payeeAddressLine1').val();
	var address2 = $('#payeeAddressLine2').val();
	var city = $('#payeeCityState').val();
	var postal = $('#payeePostalCode').val();
	var phone = $('#payeePhone').val();
	var memo = "";
	var address = "";
	var dat = "";
	var url = "";
	var proc = $('#btnSave1').text();

	var xint = $('#payeeFullAddress').val().trim().split('\n').length;
	if (xint > 0) {
		lines = $('#payeeFullAddress').val().split('\n');
		address1 = lines[0];
		address2 = lines[1];
		city = lines[2];
		postal = lines[3];
	} else {
		lines = $('#payeeFullAddress').val().trim();
		address1 = "";
		address2 = "";
		city = "";
		postal = "";
	}

	if (proc === "Update" || proc === "Add") {
		url = "/Payee/UpdatePayee";
		dat = { 'id': ID, 'name': name, 'line1': address1, 'line2': address2, 'city': city, 'zip': postal, 'phone': phone, 'memo': memo, 'payeeid': nameid, 'address': address };
	} else if (proc === "Delete") {
		url = "/Payee/DeletePayee";
		dat = { 'id': ID };
	}

	if (name !== '') {
		$.ajax({
			url: url,
			type: 'POST',
			data: dat,
			dataType: 'json',
			success: function (a) {
				if (a === '2') {
					demo.showNotification('top', 'center', 'Payee sucessfully added!', 2);
				} else if (a === '1') {
					demo.showNotification('top', 'center', 'Payee sucessfully updated!', 2);
				} else if (a === '3') {
					demo.showNotification('top', 'center', 'Payee sucessfully deleted!', 2);
				} else {
					demo.showNotification('top', 'center', a, 4);
				}
			}
		}).done(function (a) {
			$("#btnAddPayee").click();
			loadTable();
			loadPayee();
		});
	} else if (name === '') {
		demo.showNotification('top', 'center', 'Please provide all required fields(*)', 4);
		$('#payeeName').focus();
		return;
	}
}

function addPayee() {
	$('#payeeID').val('0');
	$('#payeeName').val('');
	$('#divName').addClass('is-empty');
	$('#payeeAddressLine1').val('');
	$('#divAddressLine1').addClass('is-empty');
	$('#payeeAddressLine2').val('');
	$('#divAddressLine2').addClass('is-empty');
	$('#payeeCityState').val('');
	$('#divCityState').addClass('is-empty');
	$('#payeePostalCode').val('');
	$('#divPostalCode').addClass('is-empty');
	$('#payeePhone').val('');
	$('#divPhone').addClass('is-empty'); 
	$('#payeeFullAddress').val('');
	$('#divPayeeFullAddress').addClass('is-empty');
	$('#payeeMemo').val('');
	$('#divMemo').addClass('is-empty');
	$("#btnAddPayee").click();
	$("#h4PayeeModal").text("Enter Payee's information");
	//$("#h4SubPayeeModal").text("Enter Payee's information");
	$("#btnSave1").text("Add");
}

