﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CHAXWebApp.Startup))]
namespace CHAXWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
