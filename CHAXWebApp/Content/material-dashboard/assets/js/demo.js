type = ['', 'info', 'success', 'warning', 'danger'];


function getTotalPrinted() {
	var xint = [];
	var xx = [];
	$.ajax({
		url: '/Reports/GetPrinted',
		type: 'POST',
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Loading Reports...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {
			for (i = 0; i < a.length; i++){
				xint.push(a[i]);
			}
		}
	}).done(function (a) {
		localStorage.removeItem("seriesprintedcheck");
		localStorage.setItem("seriesprintedcheck", xint);	

		var xs = xint;
		var xstr = [xs[0], xs[1], xs[2], xs[3], xs[4], xs[5], xs[6], xs[7], xs[8], xs[9], xs[10], xs[11]];
		dataDailySalesChart = {
			labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
			series: [xstr]
		};

		optionsDailySalesChart = {
			lineSmooth: Chartist.Interpolation.cardinal({
				tension: 0
			}),
			low: 0,
			high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
			chartPadding: {
				top: 0,
				right: 0,
				bottom: 0,
				left: 0
			}
		};

		var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);

		md.startAnimationForLineChart(dailySalesChart);
	});
	xx = localStorage.getItem("seriesprintedcheck");
	return xx;
}

function getTotalAmount() {
	var xint = [];
	var xx = [];
	$.ajax({
		url: '/Reports/GetAmount',
		type: 'POST',
		dataType: 'json',
		beforeSend: function () {
			$('#divSpinner').removeClass("hidden");
			$('#lblLoading').html("Loading Reports...");
		},
		complete: function () {
			$('#divSpinner').addClass("hidden");
		},
		success: function (a) {
			for (i = 0; i < a.length; i++) {
				xint.push(a[i]);
			}
		}
	}).done(function (a) {
		localStorage.removeItem("seriesamount");
		localStorage.setItem("seriesamount", xint);

		var xs2 = xint;
		var xstr2 = [xs2[0], xs2[1], xs2[2], xs2[3], xs2[4], xs2[5], xs2[6], xs2[7], xs2[8], xs2[9], xs2[10], xs2[11]];
		var dataEmailsSubscriptionChart = {
			labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
			series: [
				xstr2

			]
		};
		var optionsEmailsSubscriptionChart = {
			axisX: {
				showGrid: false
			},
			low: 0,
			high: 10000,
			chartPadding: {
				top: 0,
				right: 5,
				bottom: 0,
				left: 0
			}
		};
		var responsiveOptions = [
			['screen and (max-width: 640px)', {
				seriesBarDistance: 5,
				axisX: {
					labelInterpolationFnc: function (value) {
						return value[0];
					}
				}
			}]
		];
		var emailsSubscriptionChart = Chartist.Bar('#emailsSubscriptionChart', dataEmailsSubscriptionChart, optionsEmailsSubscriptionChart, responsiveOptions);

		//start animation for the Emails Subscription Chart
		md.startAnimationForBarChart(emailsSubscriptionChart);
	});
	xx = localStorage.getItem("seriesamount");
	return xx;
}


function getDateTo(month) {
	var today = new Date();
	var lastDayOfMonth = new Date(today.getFullYear(), month, 0);
	return today.getFullYear() + "-" + zeroPad(month, 2) + "-" + lastDayOfMonth.getDate();
}

function getDateFrom(month) {
	var today = new Date();
	var lastDayOfMonth = new Date(today.getFullYear(), month, 0);
	return today.getFullYear() + "-" + zeroPad(month, 2) + "-" + "01";
}

function zeroPad(num, places) {
	var zero = places - num.toString().length + 1;
	return Array(+(zero > 0 && zero)).join("0") + num;
}

function loadChart() {
	
}

demo = {
	initPickColor: function () {
		$('.pick-class-label').click(function () {
			var new_class = $(this).attr('new-class');
			var old_class = $('#display-buttons').attr('data-class');
			var display_div = $('#display-buttons');
			if (display_div.length) {
				var display_buttons = display_div.find('.btn');
				display_buttons.removeClass(old_class);
				display_buttons.addClass(new_class);
				display_div.attr('data-class', new_class);
			}
		});
	},

	initDocumentationCharts: function () {
		/* ----------==========     Daily Sales Chart initialization For Documentation    ==========---------- */

		dataDailySalesChart = {
			//labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
			//series: [
			//	[12, 17, 7, 17, 23, 18, 38]
			//]
			labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
			series: [
				[3, 6, 5, 4, 4, 6, 5, 7, 7, 4, 3, 9]
			]
		};

		optionsDailySalesChart = {
			lineSmooth: Chartist.Interpolation.cardinal({
				tension: 0
			}),
			low: 0,
			high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
			chartPadding: {
				top: 0,
				right: 0,
				bottom: 0,
				left: 0
			}
		};

		var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);

		md.startAnimationForLineChart(dailySalesChart);
	},

	initDashboardPageCharts: function () {

		/* ----------==========     Daily Sales Chart initialization    ==========---------- */		
		var xs = getTotalPrinted();
		var xss = xs.split(",");
		var xstr = [xss[0], xss[1], xss[2], xss[3], xss[4], xss[5], xss[6], xss[7], xss[8], xss[9], xss[10], xss[11]];
		dataDailySalesChart = {
			labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
			series: [xstr]
		};

		optionsDailySalesChart = {
			lineSmooth: Chartist.Interpolation.cardinal({
				tension: 0
			}),
			low: 0,
			high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
			chartPadding: {
				top: 0,
				right: 0,
				bottom: 0,
				left: 0
			}
		};

		var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);

		md.startAnimationForLineChart(dailySalesChart);



		/* ----------==========     Completed Tasks Chart initialization    ==========---------- */

		//dataCompletedTasksChart = {
		//	labels: ['12am', '3pm', '6pm', '9pm', '12pm', '3am', '6am', '9am'],
		//	series: [
		//		[230, 750, 450, 300, 280, 240, 200, 190]
		//	]
		//};

		//optionsCompletedTasksChart = {
		//	lineSmooth: Chartist.Interpolation.cardinal({
		//		tension: 0
		//	}),
		//	low: 0,
		//	high: 1000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
		//	chartPadding: {
		//		top: 0,
		//		right: 0,
		//		bottom: 0,
		//		left: 0
		//	}
		//};

		//var completedTasksChart = new Chartist.Line('#completedTasksChart', dataCompletedTasksChart, optionsCompletedTasksChart);

		//// start animation for the Completed Tasks Chart - Line Chart
		//md.startAnimationForLineChart(completedTasksChart);


		/* ----------==========     Emails Subscription Chart initialization    ==========---------- */
		var xs2 = getTotalAmount();
		var xs3 = xs2.split(",");
		var xstr2 = [xs3[0], xs3[1], xs3[2], xs3[3], xs3[4], xs3[5], xs3[6], xs3[7], xs3[8], xs3[9], xs3[10], xs3[11]];
		var dataEmailsSubscriptionChart = {
			labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
			series: [
				xstr2

			]
		};
		var optionsEmailsSubscriptionChart = {
			axisX: {
				showGrid: false
			},
			low: 0,
			high: 10000,
			chartPadding: {
				top: 0,
				right: 5,
				bottom: 0,
				left: 0
			}
		};
		var responsiveOptions = [
			['screen and (max-width: 640px)', {
				seriesBarDistance: 5,
				axisX: {
					labelInterpolationFnc: function (value) {
						return value[0];
					}
				}
			}]
		];
		var emailsSubscriptionChart = Chartist.Bar('#emailsSubscriptionChart', dataEmailsSubscriptionChart, optionsEmailsSubscriptionChart, responsiveOptions);

		//start animation for the Emails Subscription Chart
		md.startAnimationForBarChart(emailsSubscriptionChart);

	},

	initGoogleMaps: function () {
		var myLatlng = new google.maps.LatLng(40.748817, -73.985428);
		var mapOptions = {
			zoom: 13,
			center: myLatlng,
			scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
			styles: [{
				"featureType": "water",
				"stylers": [{
					"saturation": 43
				}, {
					"lightness": -11
				}, {
					"hue": "#0088ff"
				}]
			}, {
				"featureType": "road",
				"elementType": "geometry.fill",
				"stylers": [{
					"hue": "#ff0000"
				}, {
					"saturation": -100
				}, {
					"lightness": 99
				}]
			}, {
				"featureType": "road",
				"elementType": "geometry.stroke",
				"stylers": [{
					"color": "#808080"
				}, {
					"lightness": 54
				}]
			}, {
				"featureType": "landscape.man_made",
				"elementType": "geometry.fill",
				"stylers": [{
					"color": "#ece2d9"
				}]
			}, {
				"featureType": "poi.park",
				"elementType": "geometry.fill",
				"stylers": [{
					"color": "#ccdca1"
				}]
			}, {
				"featureType": "road",
				"elementType": "labels.text.fill",
				"stylers": [{
					"color": "#767676"
				}]
			}, {
				"featureType": "road",
				"elementType": "labels.text.stroke",
				"stylers": [{
					"color": "#ffffff"
				}]
			}, {
				"featureType": "poi",
				"stylers": [{
					"visibility": "off"
				}]
			}, {
				"featureType": "landscape.natural",
				"elementType": "geometry.fill",
				"stylers": [{
					"visibility": "on"
				}, {
					"color": "#b8cb93"
				}]
			}, {
				"featureType": "poi.park",
				"stylers": [{
					"visibility": "on"
				}]
			}, {
				"featureType": "poi.sports_complex",
				"stylers": [{
					"visibility": "on"
				}]
			}, {
				"featureType": "poi.medical",
				"stylers": [{
					"visibility": "on"
				}]
			}, {
				"featureType": "poi.business",
				"stylers": [{
					"visibility": "simplified"
				}]
			}]

		};
		var map = new google.maps.Map(document.getElementById("map"), mapOptions);

		var marker = new google.maps.Marker({
			position: myLatlng,
			title: "Hello World!"
		});

		// To add the marker to the map, call setMap();
		marker.setMap(map);
	},

	showNotification: function (from, align, message, clr) {
		var $jq = jQuery.noConflict();
		//color = Math.floor((Math.random() * 4) + 1);
		color = clr;
		$jq.notify({
			icon: "notifications",
			message: message

		}, {
				type: type[color],
				z_index: 3000,
				delay: 100,
				timer: 1000,
				placement: {
					from: from,
					align: align
				}
			});
	},

	showNotificationDashboard: function (from, align, message, clr) {
		var $jq = jQuery.noConflict();
		//color = Math.floor((Math.random() * 4) + 1);
		color = clr;
		$jq.notify({
			icon: "notifications",
			message: message

		}, {
				type: type[color],
				z_index: 3000,
				delay: 100,
				timer: 2000,
				placement: {
					from: from,
					align: align
				}
			});
	},

	closeNotificationDashboard: function () {
		var $jq = jQuery.noConflict();

		$jq.notifyClose('top-right');
	}

};