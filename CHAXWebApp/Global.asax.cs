﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CHAXWebApp.Models.DBContexts;
using CHAXWebApp.Models.DBInitializers;


namespace CHAXWebApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //Database.SetInitializer<CHAX_Context>(null); //Uncomment this line if you dont want the database to be dropped if there are changes in model
            Database.SetInitializer<CHAX_Context>(new CHAX_Context_Initializer());
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
