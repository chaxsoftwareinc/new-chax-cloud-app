﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CHAXWebApp.Controllers
{
    public class ArchiveController : Controller
    {
        // GET: Archive
        [Authorize]
        [RequireHttps]
        public ActionResult Archive()
        {
            return View();
        }
    }
}