﻿using CHAXWebApp.Models;
using CHAXWebApp.Models.DBContexts;
using CHAXWebApp.Models.DBTables;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CHAXWebApp.Controllers
{
    public class FormMessagesController : Controller
    {
        // GET: FormMessages
        public ActionResult FormMessages()
        {
            return View();
        }

        public JsonResult AddMessage(string message)
        {
            var p1 = new FormMessage();
            var email = User.Identity.GetUserName();
            string r = "";
            using (var dd = new CHAX_Context())
            {
                p1 = dd.FormMessage.Where(x => x.UserName == email).FirstOrDefault();
                if (p1 != null)
                {
                    r = UpdateMessage(email, message);
                }
                else
                {
                    r = "1";
                    var p = new FormMessage
                    {
                        UserName = email,
                        Message = message
                    };

                    using (var db = new CHAX_Context())
                    {
                        try
                        {
                            db.FormMessage.Add(p);
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            r = ex.Message.ToString();
                        }
                    }
                }
            }

            return Json(r);
        }

        public string UpdateMessage(string username, string message)
        {
            string r = "0";
            string email = User.Identity.GetUserName();
            FormMessage p;
            using (var db = new CHAX_Context())
            {
                p = db.FormMessage.Where(x => x.UserName == email).FirstOrDefault();
            }
            if (p != null)
            {
                p.UserName = username;
                p.Message = message;

                using (var db = new CHAX_Context())
                {
                    try
                    {
                        db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        r = "1";
                    }
                    catch (Exception ex)
                    {
                        r = ex.Message.ToString();
                    }
                }
            }

            return r;
        }


        public JsonResult GetMessage()
        {
            var p1 = new FormMessage();
            var email = User.Identity.GetUserName();
            string r = "";
            using (var dd = new CHAX_Context())
            {

                p1 = dd.FormMessage.Where(x => x.UserName == email).FirstOrDefault();


                if (p1 != null)
                {
                    r = p1.Message;
                }
                else
                {
                    r = "No Message!";
                }
            }

            return Json(r);
        }
    }
}