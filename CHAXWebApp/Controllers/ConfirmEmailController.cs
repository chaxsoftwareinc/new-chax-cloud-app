﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CHAXWebApp.Controllers
{
    public class ConfirmEmailController : Controller
    {
        // GET: ConfirmEmail
        public ActionResult ConfirmEmail()
        {
            ViewBag.Email = User.Identity.GetUserName();
            return View();
        }
    }
}