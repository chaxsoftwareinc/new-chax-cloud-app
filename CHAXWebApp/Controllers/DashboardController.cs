﻿using CHAXWebApp.Models;
using CHAXWebApp.Models.DBContexts;
using CHAXWebApp.Models.DBTables;
using CHAXWebApp.Models.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CHAXWebApp.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Dashboard
        [Authorize]
        [RequireHttps]
        public ActionResult Dashboard()
        {
            if ((IsEmailConfirmed() && IsParentUser() == 0) || IsParentUser() == 1)
            {
                return View();
            }
            return RedirectToAction("ConfirmEmail", "ConfirmEmail");
        }

        private bool IsEmailConfirmed()
        {
            var userid = User.Identity.GetUserId();
            bool isconfirmed = false;
            using (var db = new ApplicationDbContext())
            {
                isconfirmed = db.Users.Where(x => x.Id == userid).FirstOrDefault().EmailConfirmed;
            }
            return isconfirmed;
        }

        private int IsParentUser()
        {
            int isparent = 0;
            var userid = User.Identity.GetUserId();
            using (var db = new ApplicationDbContext())
            {
                isparent = db.Users.Where(x => x.Id == userid).FirstOrDefault().UserLevel;
            }
            return isparent;
        }

        public JsonResult GetTopPrintedChecks()
        {
            var email = User.Identity.GetUserName();
            List<PrintedCheck> p = new List<PrintedCheck>();
            using (var db = new ApplicationDbContext())
            {
                var pp = db.Users.Where(x => x.UserName == email).FirstOrDefault();
                if (pp.UserLevel == 1)
                {
                    using (var db2 = new CHAX_Context())
                    {
                        p = db2.PrintedCheck.Where(x => x.IsDeleted == false && (x.UserEmail == email || x.UserEmail == pp.ParentUserName) && x.IsPrinted == false).OrderByDescending(x => x.DateCreated).Take(50).ToList();
                    }
                }
                else {
                    using (var db3 = new CHAX_Context())
                    {
                        p = db3.PrintedCheck.Where(x => x.IsDeleted == false && x.UserEmail == email && x.IsPrinted == false).OrderByDescending(x => x.DateCreated).Take(50).ToList();
                    }
                    
                }
            }
            return Json(p);
        }

        public JsonResult GetAllPrintedChecks()
        {
            var email = User.Identity.GetUserName();
            List<PrintedCheck> p = new List<PrintedCheck>();
            using (var db = new CHAX_Context())
            {
                p = db.PrintedCheck.Where(x => x.IsDeleted == false && x.UserEmail == email).OrderByDescending(x => x.DateCreated).ToList();
            }
            return Json(p, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllMemorizedChecks()
        {
            var email = User.Identity.GetUserName();
            List<PrintedCheck> p = new List<PrintedCheck>();
            using (var db = new CHAX_Context())
            {
                p = db.PrintedCheck.Where(x => x.IsDeleted == false && x.IsMemorized == true && x.UserEmail == email).OrderByDescending(x => x.DateCreated).ToList();
            }
            return Json(p, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTopMemorizedChecks()
        {
            var email = User.Identity.GetUserName();
            List<PrintedCheck> p = new List<PrintedCheck>();
            using (var db = new CHAX_Context())
            {
                p = db.PrintedCheck.Where(x => x.IsMemorized == true && x.IsDeleted == false && x.UserEmail == email).OrderByDescending(x => x.DateCreated).Take(20).ToList();
            }
            return Json(p);
        }

        public JsonResult MemorizeCheck(int id)
        {
            var email = User.Identity.GetUserName();
            string r = "0";
            PrintedCheck p;
            using (var db = new CHAX_Context())
            {
                p = db.PrintedCheck.Where(x => x.ID == id && x.UserEmail == email).FirstOrDefault<PrintedCheck>();
            }
            if (p != null)
            {
                if (p.IsMemorized == true)
                {
                    r = "This check number is already memorized.";
                }
                else
                {
                    p.IsMemorized = true;
                    using (var db = new CHAX_Context())
                    {
                        try
                        {
                            db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            r = ex.Message.ToString();
                        }
                    }
                }

            }
            else
            {
                r = "ID does not exist";
            }

            return Json(r);
        }

        public JsonResult RemoveCheck(int id)
        {
            var email = User.Identity.GetUserName();
            string r = "0";
            PrintedCheck p;
            using (var db = new CHAX_Context())
            {
                p = db.PrintedCheck.Where(x => x.ID == id && x.UserEmail == email).FirstOrDefault<PrintedCheck>();
            }

            if (p != null)
            {
                if (p.IsDeleted == true)
                {
                    r = "This check number is already deleted.";
                }
                else
                {
                    p.IsDeleted = true;
                    using (var db = new CHAX_Context())
                    {
                        try
                        {
                            db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            r = ex.Message.ToString();
                        }
                    }
                }

            }
            else
            {
                r = "ID does not exist";
            }

            return Json(r);
        }

        public JsonResult UnmemorizeCheck(int id)
        {
            var email = User.Identity.GetUserName();
            string r = "0";
            PrintedCheck p;
            using (var db = new CHAX_Context())
            {
                p = db.PrintedCheck.Where(x => x.ID == id && x.UserEmail == email).FirstOrDefault<PrintedCheck>();
            }
            if (p != null)
            {
                p.IsMemorized = false;
                using (var db = new CHAX_Context())
                {
                    try
                    {
                        db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        r = ex.Message.ToString();
                    }
                }
            }
            else
            {
                r = "ID does not exist";
            }

            return Json(r);
        }
        public JsonResult PreviewCheck(int id)
        {
            var email = User.Identity.GetUserName();
            List<PrintedCheck> p = new List<PrintedCheck>();
            using (var db = new CHAX_Context())
            {
                //DateTime date = Convert.ToDateTime(DateTime.Now.Date.ToShortDateString()).Date;
                p = db.PrintedCheck.Where(x => x.ID == id && x.UserEmail == email).ToList();
            }
            return Json(p);
        }

        public JsonResult PreviewCheckForEdit(int id)
        {
            var email = User.Identity.GetUserName();
            List<PrintedCheck> p = new List<PrintedCheck>();
            CheckInfoViewModel ci = new CheckInfoViewModel();
            using (var db = new CHAX_Context())
            {
                //DateTime date = Convert.ToDateTime(DateTime.Now.Date.ToShortDateString()).Date;
                p = db.PrintedCheck.Where(x => x.ID == id && x.UserEmail == email).ToList();
                foreach (var item in p)
                {
                    ci.ID = item.ID;
                    ci.CheckNumber = item.CheckNumber;
                    ci.PayerName = item.PayerName;
                    ci.PayerAddress = item.PayerAddress;
                    ci.BankName = item.BankName;
                    ci.RoutingNumber = item.RoutingNumber;
                    ci.AccountNumber = item.AccountNumber;
                    ci.Amount = item.Amount;
                    ci.AmountInWords = item.AmountInWords;
                    ci.Date = item.Date.ToString();
                    ci.PayeeName = item.PayeeName;
                    ci.Payee = item.Payee;
                    ci.Memo = item.Memo;

                }

            }
            return Json(p);
        }

        public JsonResult GetAmountThisMonth()
        {
            var email = User.Identity.GetUserName();
            decimal str = 0;
            int year = DateTime.Now.Date.Year;
            int month = DateTime.Now.Date.Month;
            int lastday = DateTime.DaysInMonth(year, month);
            string que = @"select isnull(sum(convert(numeric(18,2), Amount)), 0) as TotalAmountThisMonth from PrintedCheck where Date between" +
                         @"'" + year + "-" + month + "-01' and '" + year + "-" + month + "-" + lastday + "' and IsDeleted = 0 and UserEmail = '" + email + "'";
            using (var db = new CHAX_Context())
            {
                str = db.Database.SqlQuery<decimal>(que).FirstOrDefault();
            }
            return Json(str.ToString());
        }

        public JsonResult GetPrintedThisMonth()
        {
            var email = User.Identity.GetUserName();
            int str = 0;
            int year = DateTime.Now.Date.Year;
            int month = DateTime.Now.Date.Month;
            int lastday = DateTime.DaysInMonth(year, month);
            string que = @"select count(ID) as TotalAmountThisMonth from PrintedCheck where Date between" +
                        "'" + year + "-" + month + "-01' and '" + year + "-" + month + "-" + lastday + "' and IsDeleted = 0 and UserEmail = '" + email + "'";
            using (var db = new CHAX_Context())
            {
                str = db.Database.SqlQuery<int>(que).FirstOrDefault();
            }
            return Json(str.ToString());
        }

        public JsonResult GetPrinted()
        {
            var email = User.Identity.GetUserName();
            int str = 0;
            string que = @"select isnull(count(ID), 0) as TotalAmountThisMonth from PrintedCheck where
                            IsDeleted = 0 and UserEmail = '" + email + "'";
            using (var db = new CHAX_Context())
            {
                str = db.Database.SqlQuery<int>(que).FirstOrDefault();
            }
            return Json(str.ToString());
        }

        public JsonResult GetTotalAmount()
        {
            var email = User.Identity.GetUserName();
            decimal str = 0;
            string que = @"select isnull(sum(convert(numeric(18,2), Amount)), 0) as TotalAmountThisMonth from PrintedCheck where IsDeleted = 0 and UserEmail = '" + email + "'";
            using (var db = new CHAX_Context())
            {
                str = db.Database.SqlQuery<decimal>(que).FirstOrDefault();
            }
            return Json(str.ToString());
        }

        public JsonResult GetAllBank()
        {
            List<Bank> p = new List<Bank>();
            using (var db = new CHAX_Context())
            {
                p = db.Bank.Where(x => x.ID > 1).ToList();
            }
            var jsonResult = Json(p, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public JsonResult GenerateCheck(int id, string layout)
        {
            string r = "0";
            string email = User.Identity.GetUserName();
            PrintedCheck p;
            using (var db = new CHAX_Context())
            {
                p = db.PrintedCheck.Where(x => x.UserEmail == email && x.ID == id).FirstOrDefault();
            }
            if (p != null)
            {
                p.IsGenerated = true;
                p.CheckLayout = GetLayout(layout).ToString();
                using (var db = new CHAX_Context())
                {
                    try
                    {
                        db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        r = ex.Message.ToString();
                    }
                }

                r = "1";
            }

            return Json(r);
        }


        private int GetLayout(string desc)
        {
            int p;
            using (var db = new CHAX_Context())
            {
                p = db.CheckLayout.Where(x => x.LayoutDescription == desc).FirstOrDefault().LayoutCode;
            }
            return p;
        }

        public JsonResult PrintCheck(int id)
        {
            string r = "0";
            string email = User.Identity.GetUserName();
            PrintedCheck p;
            using (var db = new CHAX_Context())
            {
                p = db.PrintedCheck.Where(x => x.UserEmail == email && x.ID == id).FirstOrDefault();
            }
            if (p != null)
            {
                p.IsPrinted = true;
                using (var db = new CHAX_Context())
                {
                    try
                    {
                        db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        r = ex.Message.ToString();
                    }
                }

                r = "1";
            }

            return Json(r);
        }
    }
}