﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using CHAXWebApp.Models.ViewModels;
using System.Globalization;
using CHAXWebApp.Models.DBTables;
using CHAXWebApp.Models.DBContexts;
using static CHAXWebApp.Classes.CHAX;
using System.Drawing.Imaging;
using Ghostscript.NET;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using CHAXWebApp.Models;

namespace CHAXWebApp.Controllers
{
    public class CheckController : Controller
    {
        // GET: Check
        public ActionResult Check()
        {
            return View();
        }

        [HttpGet]
        [DeleteFileAttribute] //Action Filter, it will auto delete the file after download, 
        public ActionResult Download(string file)
        {
            //get the temp folder and file path in server
            string fullPath = Path.Combine(Server.MapPath("~/Files"), file);

            //return the file for download, this is an Excel 
            //so I set the file content type to "application/vnd.ms-excel"
            return File(fullPath, "application/pdf", file);
        }

        [HttpGet]
        [DeleteFileAttribute] //Action Filter, it will auto delete the file after preview, 
        public ActionResult Preview(string file)
        {

            //get the temp folder and file path in server
            string fullPath = Path.Combine(Server.MapPath("~/Files"), file);
            //return the file for download, this is an Excel 
            //so I set the file content type to "application/vnd.ms-excel"

            //return base.File(fullPath, "application/pdf");
            return base.File(fullPath, "image/jpeg");
        }

        public void ConvertToImage(string flupload)
        {
            String PdfFolderPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Files/" + flupload);
            Pdf2ImageConversion(flupload, PdfFolderPath);
        }

        private void Pdf2ImageConversion(string FileName, string PdfFolderPath)
        {
            String FileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(FileName);
            String ImgFolderPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Files/"
                + FileNameWithoutExtension + ".jpeg");
            var info = new System.IO.FileInfo(ImgFolderPath);
            if (System.IO.File.Exists(ImgFolderPath))
            {
                System.IO.File.Delete(ImgFolderPath);
            }
            if (info.Exists.Equals(false))
            {
                GhostscriptPngDevice img = new GhostscriptPngDevice(GhostscriptPngDeviceType.Png16m)
                {
                    GraphicsAlphaBits = GhostscriptImageDeviceAlphaBits.V_4,
                    TextAlphaBits = GhostscriptImageDeviceAlphaBits.V_4,
                    ResolutionXY = new GhostscriptImageDeviceResolution(200, 200)
                };
                img.InputFiles.Add(PdfFolderPath);
                img.Pdf.FirstPage = 1;
                img.Pdf.LastPage = 1;
                img.PostScript = string.Empty;
                img.OutputPath = ImgFolderPath;
                img.Process();
            }
            if (System.IO.File.Exists(ImgFolderPath))
            {
                System.IO.File.Delete(PdfFolderPath);
            }
        }

        [HttpPost]
        public ActionResult CreateCheck(CheckInfoViewModel ci)
        {
            dynamic xstr = null;
            if (CheckIfExisting(ci.CheckNumber, ci.AccountNumber) == true)
            {
                xstr = Json(new { result = "true" });
            }
            else
            {
                xstr = Pdf(ci);
            }
            return xstr;
        }

        [HttpPost]
        public ActionResult Pdf(CheckInfoViewModel ci)
        {
            string xproc = ci.ProcessType;
            string cnumber = ci.CheckNumber;
            MemoryStream workStream = new MemoryStream();
            dynamic x = null;
            if (ci.CheckLayout == "Top")
            {
                workStream = PdfTop(ci);
            }
            else if (ci.CheckLayout == "Middle")
            {
                workStream = PdfMiddle(ci);
            }
            else if (ci.CheckLayout == "Bottom")
            {
                workStream = PdfBottom(ci);
            }
            else if (ci.CheckLayout == "Batch")
            {
                workStream = PdfBatch(ci);
            }
            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;

            if (ci.CheckLayout == "Top")
            {
                if (xproc == "Download")
                {
                    x = File(workStream, "application/pdf", "CHAXWeb-Top-" + ci.CheckNumber + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf");
                    UpdatePrintedChecks(ci);
                }
                else if (xproc == "Preview")
                {
                    System.IO.File.WriteAllBytes(Server.MapPath("~/Files/CHAXWebTop") + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf", byteInfo);
                    x = Content("<script>window.location = '/Files/CHAXWebTop" + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf" + "?page=hsn#toolbar=0';</script>");
                }
                else if (xproc == "Save")
                {
                    x = Json(new { result = UpdatePrintedChecks(ci) });
                }
                else if (xproc == "Preview Dashboard")
                {
                    System.IO.File.WriteAllBytes(Server.MapPath("~/Files/CHAXWebTop") + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf", byteInfo);                    
                    ConvertToImage("CHAXWebTop" + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf");
                    x = Json("CHAXWebTop" + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".jpeg", JsonRequestBehavior.AllowGet);
                }
                else if (xproc == "Save Dashboard")
                {
                    System.IO.File.WriteAllBytes(Server.MapPath("~/Files/CHAXWebTop") + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf", byteInfo);
                    x = Json("CHAXWebTop" + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf");
                }
            }
            else if (ci.CheckLayout == "Middle")
            {
                if (xproc == "Download")
                {
                    x = File(workStream, "application/pdf", "CHAXWeb-Middle-" + ci.CheckNumber + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf");
                    UpdatePrintedChecks(ci);
                }
                else if (xproc == "Preview")
                {
                    System.IO.File.WriteAllBytes(Server.MapPath("~/Files/CHAXWebMiddle") + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf", byteInfo);
                    x = Content("<script>window.location = '/Files/CHAXWebMiddle" + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf" + "?page=hsn#toolbar=0';</script>");
                }
                else if (xproc == "Save")
                {
                    x = Json(new { result = UpdatePrintedChecks(ci) });
                }
                else if (xproc == "Preview Dashboard")
                {
                    System.IO.File.WriteAllBytes(Server.MapPath("~/Files/CHAXWebMiddle") + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf", byteInfo);
                    ConvertToImage("CHAXWebMiddle" + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf");
                    x = Json("CHAXWebMiddle" + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".jpeg", JsonRequestBehavior.AllowGet);
                }
                else if (xproc == "Save Dashboard")
                {
                    System.IO.File.WriteAllBytes(Server.MapPath("~/Files/CHAXWebMiddle") + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf", byteInfo);
                    x = Json("CHAXWebMiddle" + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf");
                }
            }
            else if (ci.CheckLayout == "Bottom")
            {
                if (xproc == "Download")
                {
                    x = File(workStream, "application/pdf", "CHAXWeb-Bottom-" + ci.CheckNumber + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf");
                    UpdatePrintedChecks(ci);
                }
                else if (xproc == "Preview")
                {
                    System.IO.File.WriteAllBytes(Server.MapPath("~/Files/CHAXWebBottom") + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf", byteInfo);
                    x = Content("<script>window.location = '/Files/CHAXWebBottom" + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf" + "?page=hsn#toolbar=0';</script>");
                }
                else if (xproc == "Save")
                {
                    x = Json(new { result = UpdatePrintedChecks(ci) });
                }
                else if (xproc == "Preview Dashboard")
                {
                    System.IO.File.WriteAllBytes(Server.MapPath("~/Files/CHAXWebBottom") + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf", byteInfo);
                    ConvertToImage("CHAXWebBottom" + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf");
                    x = Json("CHAXWebBottom" + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".jpeg", JsonRequestBehavior.AllowGet);
                }
                else if (xproc == "Save Dashboard")
                {
                    System.IO.File.WriteAllBytes(Server.MapPath("~/Files/CHAXWebBottom") + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf", byteInfo);
                    x = Json("CHAXWebBottom" + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf");
                }
            }
            else if (ci.CheckLayout == "Batch")
            {
                if (xproc == "Download")
                {
                    x = File(workStream, "application/pdf", "CHAXWeb-Middle-" + ci.CheckNumber + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf");
                    UpdatePrintedChecks(ci);
                }
                else if (xproc == "Preview")
                {
                    System.IO.File.WriteAllBytes(Server.MapPath("~/Files/CHAXWebBatch") + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf", byteInfo);
                    x = Content("<script>window.location = '/Files/CHAXWebBatch" + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf" + "?page=hsn#toolbar=0';</script>");
                }
                else if (xproc == "Save")
                {
                    x = Json(new { result = UpdatePrintedChecks(ci) });
                }
                else if (xproc == "Preview Dashboard")
                {
                    System.IO.File.WriteAllBytes(Server.MapPath("~/Files/CHAXWebBatch") + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf", byteInfo);
                    ConvertToImage("CHAXWebBatch" + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf");
                    x = Json("CHAXWebBatch" + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".jpeg", JsonRequestBehavior.AllowGet);
                }
                else if (xproc == "Save Dashboard")
                {
                    System.IO.File.WriteAllBytes(Server.MapPath("~/Files/CHAXWebBatch") + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf", byteInfo);
                    x = Json("CHAXWebBatch" + DateTime.Now.ToString("ddMMyyyy") + cnumber + ".pdf");
                }
            }

            return x;
        }

        private string TruncateAccountNumber(string routing, string account)
        {
            int xint = (routing + account).Length - 4;
            string xstr = "";
            for (int i = 0; i < xint; i++)
            {
                xstr = xstr + "X";
            }
            return xstr;
        }

        private string FormatAddressPhone(string address, string phone)
        {
            var xstr = address + "\r\n" + phone;
            for (int i = CountLines(xstr); i < 5; i++)
            {
                xstr = xstr + "\r\n ";
            }
            return xstr;
        }

        private int CountLines(string xstr)
        {
            return xstr.Split('\n').Length;
        }

        private float GetCheckHor()
        {
            var email = User.Identity.GetUserName();
            float p = 0;
            using (var db = new ApplicationDbContext())
            {
                //DateTime date = Convert.ToDateTime(DateTime.Now.Date.ToShortDateString()).Date;
                p = db.Users.Where(x => x.UserName == email).FirstOrDefault().CheckHor;
            }
            return p;
        }

        private float GetMICRHor()
        {
            var email = User.Identity.GetUserName();
            float p = 0;
            using (var db = new ApplicationDbContext())
            {
                //DateTime date = Convert.ToDateTime(DateTime.Now.Date.ToShortDateString()).Date;
                p = db.Users.Where(x => x.UserName == email).FirstOrDefault().MICRHor;
            }
            return p;
        }

        private float GetMICRVer()
        {
            var email = User.Identity.GetUserName();
            float p = 0;
            using (var db = new ApplicationDbContext())
            {
                //DateTime date = Convert.ToDateTime(DateTime.Now.Date.ToShortDateString()).Date;
                p = db.Users.Where(x => x.UserName == email).FirstOrDefault().MICRVer;
            }
            return p;
        }

        private MemoryStream PdfMiddle(CheckInfoViewModel ci)
        {
            string cnumber = ci.CheckNumber;
            string pname = ci.PayerName;
            string pyname = ci.PayerName;
            //string paddress = ci.PayerAddress + ci.PayerPhone;
            string paddress = FormatAddressPhone(ci.PayerAddress, ci.PayerPhone);
            string pphone = ci.PayerPhone;
            string pyphone = ci.PayeePhone;
            string bname = ci.BankName;
            string rnumber = ci.RoutingNumber;
            string anumber = ci.AccountNumber;
            //string ppayee = ci.Payee;
            string ppayee = FormatAddressPhone(ci.Payee, ci.PayeePhone);
            string pdate = ci.Date;
            string pamount = ci.Amount;
            string cmemo = ci.Memo;
            string payeename = ci.PayeeName;
            //string payeeaddress = ci.PayeeAddress;
            string payeeaddress = FormatAddressPhone(ci.PayeeAddress, ci.PayeePhone);
            string[] bankadd = ci.BankAddress?.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            string bankaddress = bankadd != null ? bankadd[0].ToString() : "";
            string bankcitystate = bankadd != null ? bankadd[1].ToString() : "";
            pamount = float.Parse(pamount).ToString("N", new CultureInfo("en-US"));
            string pamountwords = ci.AmountInWords;
            string path = Server.MapPath("~");

            MemoryStream workStream = new MemoryStream();
            float ytoadd = 0;
            float ytoadda = 0.35f;
            float ytoaddf = 0.25f;
            float checkhor = ci.Test == "test" ? ci.CheckHor : GetCheckHor();
            float micrhor = ci.Test == "test" ? ci.MICRHor : GetMICRHor();
            float micrver = ci.Test == "test" ? ci.MICRVer : GetMICRVer();

            Document document = new Document(PageSize.LETTER, 0f, 0f, 20f, 0f);
            PdfWriter writer = PdfWriter.GetInstance(document, workStream);
            writer.CloseStream = false;

            document.Open();
            //CONFIRMATION OF CHECK VARIABLES START
            BaseFont f_cn = BaseFont.CreateFont("c:\\windows\\fonts\\arial.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BaseFont f_cn_bold = BaseFont.CreateFont(path + "\\fonts\\ARIALBD.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font fontnormalbold10 = FontFactory.GetFont("Arial", 10, Font.BOLD);
            Font f_cn_bold_10 = new Font(f_cn_bold, 10);
            Font f_cn_bold_10_times = new Font(bfTimes, 10, Font.BOLD);
            Font f_cn_bold_8 = new Font(f_cn_bold, 8);
            Font f_cn_8 = new Font(f_cn, 8, Font.NORMAL, BaseColor.WHITE);
            Font f_cn_bold_12_times = new Font(bfTimes, 12, Font.BOLDITALIC);

            BaseFont f_micr = BaseFont.CreateFont(path + "\\fonts\\MCCH____.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BaseFont f_micr2 = BaseFont.CreateFont(path + "\\fonts\\MCCH____.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            Font f_cn_arial_7 = new Font(f_cn, 7);
            Font smallfontnormal = FontFactory.GetFont("Arial", 8, Font.NORMAL);
            Font micrf = new Font(f_micr, 12, Font.BOLD);
            Font smallfont = FontFactory.GetFont("Arial", 8);
            Font bigfont10 = FontFactory.GetFont("Arial", 10);
            Font bigfont14 = FontFactory.GetFont("Arial", 14);
            Font bigfont = FontFactory.GetFont("Arial", 12);
            Font bigfontmcch = FontFactory.GetFont("MICR", 12);
            Chunk glue = new Chunk(new VerticalPositionMark());

            string blankline = "\n";
            string drawn = "Drawn to Pay To";
            string payee = pyname + Environment.NewLine + ppayee;
            string confirmation = "CONFIRMATION OF CHECK";
            string confirmationdate = "Date: " + pdate + " #: " + cnumber;
            string confirmationamount = "Amount: $" + pamount;
            string phone = @"Phone #: " + pphone;
            string memo = cmemo + Environment.NewLine +
@"As you instructed a check has been prepared for " + pamount;
            string payfrom = @"Pay from the account of:";

            string payername = pname;
            string payeraddress = paddress;
            string drawnonbank = "Drawn on bank:";
            string bankname = bname;
            string accountnumber = anumber;
            string micr = "A" + rnumber + "A";
            string printed = "Printed using CHAX®";
            string checksoft = "check software - 2687708";
            string printed2 = "Printed using CHAX® check software - 2687708";
            string notnego = "CONFIRMATION - NOT NEGOTIABLE";
            //CONFIRMATION OF CHECK VARIABLES END

            //ACTUAL VARIABLES START
            string amountwords = pamountwords.Replace(" Dollars", "").Replace(" Cents", "");
            string memoactual = "MEMO         " + cmemo;
            Chunk routing = new Chunk(TruncateAccountNumber(rnumber, anumber), bigfont);
            Chunk routing2 = new Chunk(anumber.Substring(anumber.Length - 4), micrf);
            string digits = cnumber;
            string date = pdate;
            string amount2 = "$" + pamount;
            string sig1 = "SIGNATURE NOT REQUIRED";
            string sig2 = "Your depositor has authorized this payment to payee.";
            string sig3 = "Payee to hold you harmless for payment of this document.";
            string sig4 = "This document shall be deposited only to credit of payee.";
            string sig5 = "Absence of endorsement is guaranteed by payee.";
            string tracking = "Tracking Code:  MKGT-MKG5-3456";
            string notvalid = "*** Not valid for use as a money order or cashier's check ***";
            string tothe = "TO THE";
            string order = "ORDER";
            string of = "OF";
            string asterisks = "******************************";
            string dollars = " Dollars";
            string authorized = "AUTHORIZED SIGNATURE";
            string routingP = "A" + rnumber + "A " + anumber + "C";
            string cnumberP = "C" + cnumber + " ";
            //ACTUAL VARIABLES END

            //FILE COPY VARIABLES START
            string routingfilecopy = micr + " " + anumber + "C";
            string filecopy = @"FILE COPY OF CHECK";
            //FILE COPY VARIABLES END

            //CONFIRMATION OF CHECK PARAGRAPHS START
            Paragraph blanklineP = new Paragraph(12, blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph blanklinePbig = new Paragraph(ConvertToInch(.45f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph drawnP = new Paragraph(9, drawn, smallfont)
            {
                IndentationLeft = ConvertToInch(.28f)
            };

            Paragraph payeeP = new Paragraph(9, payee, smallfont)
            {
                IndentationLeft = ConvertToInch(.28f)                               
            };

            Paragraph phoneP = new Paragraph(9, phone, smallfont)
            {
                IndentationLeft = ConvertToInch(.32f)
            };

            Paragraph memoP = new Paragraph(9, memo, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph payfromP = new Paragraph(9, payfrom, f_cn_bold_8)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph payernameP = new Paragraph(9, payername, f_cn_bold_8)
            {
                IndentationLeft = ConvertToInch(1f)
            };

            Paragraph payeraddressP = new Paragraph(9, payeraddress, smallfont)
            {
                IndentationLeft = ConvertToInch(1f)
            };

            Paragraph mcchP = new Paragraph(9, micr, micrf)
            {
                IndentationLeft = ConvertToInch(.12f),
            };

            Paragraph linep = new Paragraph(5,
                new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 5.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1))
                )
            {
                IndentationLeft = ConvertToInch(.12f)
            };
            //CONFIRMATION OF CHECK PARAGRAPHS END

            //ACTUAL CHECK PARAGRAPHS START
            Paragraph printed2P = new Paragraph(0, printed2, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph payernamePactual = new Paragraph(11, payername, f_cn_bold_10_times)
            {
                IndentationLeft = ConvertToInch(1f + checkhor)
            };

            Paragraph payeraddressPactual = new Paragraph(8, payeraddress, f_cn_arial_7)
            {
                IndentationLeft = ConvertToInch(1f + checkhor)
            };

            Paragraph trackingCodePactual = new Paragraph(12, tracking, f_cn_arial_7)
            {
                IndentationLeft = ConvertToInch(.5f + checkhor)
            };

            Paragraph notValidPactual = new Paragraph(8, notvalid, f_cn_arial_7)
            {
                IndentationLeft = ConvertToInch(.5f + checkhor)
            };

            Paragraph amountwordsP = new Paragraph(9, "PAY          ", bigfont10)
            {
                IndentationLeft = ConvertToInch(.25f + checkhor)
            };

            amountwordsP.Add(new Chunk(amountwords, f_cn_bold_12_times));

            Paragraph payeePactual = new Paragraph(9, payeename + "\n", fontnormalbold10)
            {
                IndentationLeft = ConvertToInch(.89f + checkhor)
            };

            Paragraph payeeaddressPactual = new Paragraph(9, payeeaddress, smallfontnormal)
            {
                IndentationLeft = ConvertToInch(.89f + checkhor)
            };


            Paragraph phonePactual = new Paragraph(9, phone, smallfont)
            {
                IndentationLeft = ConvertToInch(.78f)
            };

            Paragraph memoPactual = new Paragraph(9, memoactual, smallfont)
            {
                IndentationLeft = ConvertToInch(.25f + checkhor)
            };

            //Paragraph routingP = new Paragraph(9, ("A" + rnumber + "A" + anumber + "C" + cnumber), micrf)
            //{
            //    Alignment = Element.ALIGN_CENTER
            //};

            Paragraph routingP1 = new Paragraph(9, (""), micrf)
            {
                Alignment = Element.ALIGN_CENTER
            };

            Paragraph blanklinePbig2 = new Paragraph(ConvertToInch(.38f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph blanklinePbig3 = new Paragraph(ConvertToInch(.28f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Chunk signatureLinep = new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 5.0F, BaseColor.BLACK, Element.ALIGN_RIGHT, 1));
            //ACTUAL CHECK PARAGRAPHS END
            //FILE COPY PARAGRAPHS START
            Paragraph phonePfileCopy = new Paragraph(15, phone, bigfont10)
            {
                IndentationLeft = ConvertToInch(.78f)
            };

            Paragraph routingPfilecopy = new Paragraph(12)
            {
                IndentationLeft = ConvertToInch(.12f),
            };

            routingPfilecopy.Add(routing);
            routingPfilecopy.Add(routing2);
            //FILE COPY PARAGRAPHS END

            PdfContentByte cb = writer.DirectContent;
            cb.BeginText();
            //CONFIRMATION OF CHECK CONTENTBYTE START           
            cb.SetFontAndSize(f_cn_bold, 12);
            cb.SetTextMatrix(ConvertToInchX(4.70f), ConvertToInchY(.6f + ytoadd));
            cb.ShowText(confirmation);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(.6f + ytoadd) - 12);
            cb.ShowText(confirmationdate);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(.6f + ytoadd) - 24);
            cb.ShowText(confirmationamount);
            cb.SetFontAndSize(f_cn, 8);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(1.75f + ytoadd));
            cb.ShowText(drawnonbank);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(1.75f + ytoadd) - 8);
            cb.ShowText(bankname);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(1.75f + ytoadd) - 16);
            cb.ShowText(accountnumber);
            cb.SetFontAndSize(f_cn_bold, 8);
            cb.SetTextMatrix(ConvertToInchX(5.80f), ConvertToInchY(2.42f + ytoadd));
            cb.ShowText(printed);
            cb.SetFontAndSize(f_cn, 8);
            cb.SetTextMatrix(ConvertToInchX(5.80f), ConvertToInchY(2.42f + ytoadd) - 10);
            cb.ShowText(checksoft);
            cb.SetFontAndSize(f_cn_bold, 10);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(2.42f + ytoadd) - 20);
            cb.ShowText(notnego);
            //CONFIRMATION OF CHECK CONTENTBYTE END  
            //ACTUAL CHECK CONTENTBYTE START  
            cb.SetFontAndSize(f_cn_bold, 7);
            cb.SetTextMatrix(ConvertToInchX(4f + checkhor), ConvertToInchY(3.80f + ytoadd));
            cb.ShowText(bankname);
            cb.SetFontAndSize(f_cn, 6);
            cb.SetTextMatrix(ConvertToInchX(4f + checkhor), ConvertToInchY(3.80f + ytoadd) - 7);
            cb.ShowText(bankaddress);
            cb.SetTextMatrix(ConvertToInchX(4f + checkhor), ConvertToInchY(3.80f + ytoadd) - 13);
            cb.ShowText(bankcitystate);
            cb.SetFontAndSize(f_cn_bold, 18);
            cb.ShowTextAligned(Element.ALIGN_RIGHT, digits, ConvertToInchX(8f + checkhor), ConvertToInchY(4f + ytoadd), 0);
            cb.SetFontAndSize(f_cn_bold, 14);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5f + ytoadd));
            cb.ShowText(asterisks);
            cb.SetFontAndSize(f_cn_bold, 11);
            cb.SetTextMatrix(ConvertToInchX(7.4f + checkhor), ConvertToInchY(4.94f + ytoadd));
            cb.ShowText(dollars);

            cb.SetFontAndSize(f_cn, 7);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.1f + ytoadda));
            cb.ShowText(sig1);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.1f + ytoadda) - 8);
            cb.ShowText(sig2);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.1f + ytoadda) - 16);
            cb.ShowText(sig3);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.1f + ytoadda) - 24);
            cb.ShowText(sig4);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.1f + ytoadda) - 32);
            cb.ShowText(sig5);

            cb.SetFontAndSize(f_cn, 10);
            cb.SetTextMatrix(ConvertToInchX(.25f + checkhor), ConvertToInchY(5.37f + ytoadd));
            cb.ShowText(tothe);
            cb.SetTextMatrix(ConvertToInchX(.25f + checkhor), ConvertToInchY(5.37f + ytoadd) - 10);
            cb.ShowText(order);
            cb.SetTextMatrix(ConvertToInchX(.25f + checkhor), ConvertToInchY(5.37f + ytoadd) - 20);
            cb.ShowText(of);

            cb.SetFontAndSize(f_cn_bold, 10);
            cb.SetTextMatrix(ConvertToInchX(5.75f + checkhor), ConvertToInchY(6.12f + ytoadd));
            cb.ShowText(pyname);
            cb.SetFontAndSize(f_cn, 7);
            cb.SetTextMatrix(ConvertToInchX(5.75f + checkhor), ConvertToInchY(6.28f + ytoadd));
            cb.ShowText(authorized);

            //ACTUAL CHECK CONTENTBYTE END
            //FILE COPY CONTENTBYTE START       
            cb.SetFontAndSize(f_cn_bold, 12);
            cb.SetTextMatrix(ConvertToInchX(4.70f), ConvertToInchY(7.47f + ytoaddf));
            cb.ShowText(filecopy);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(7.47f + ytoaddf) - 12);
            cb.ShowText(confirmationdate);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(7.47f + ytoaddf) - 24);
            cb.ShowText(confirmationamount);
            cb.SetFontAndSize(f_cn, 8);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(8.60f + ytoaddf));
            cb.ShowText(drawnonbank);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(8.60f + ytoaddf) - 8);
            cb.ShowText(bankname);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(8.60f + ytoaddf) - 16);
            cb.ShowText(accountnumber);
            cb.SetFontAndSize(f_cn_bold, 8);
            cb.SetTextMatrix(ConvertToInchX(5.80f), ConvertToInchY(9.30f + ytoaddf));
            cb.ShowText(printed);
            cb.SetFontAndSize(f_cn, 8);
            cb.SetTextMatrix(ConvertToInchX(5.80f), ConvertToInchY(9.30f + ytoaddf) - 10);
            cb.ShowText(checksoft);
            cb.SetFontAndSize(f_cn_bold, 10);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(9.30f + ytoaddf) - 20);
            cb.ShowText(notnego);
            cb.EndText();

            //FILE COPY CONTENTBYTE END

            cb.MoveTo(ConvertToInchX(7.8f + checkhor), ConvertToInchY(6.15f));
            cb.LineTo(ConvertToInchX(5.25f + checkhor), ConvertToInchY(6.15f));
            cb.Stroke();

            //Table
            string lDate = "Date";
            string lCheckNumber = "Check No.";
            string lAmount = "Amount";
            PdfPTable table = new PdfPTable(3)
            {
                TotalWidth = 290
            };

            PdfPCell cellDateLabel = new PdfPCell(new Phrase(lDate, new Font(bfTimes, 8)))
            {
                BackgroundColor = new BaseColor(192, 192, 192),
                HorizontalAlignment = 1
            };
            PdfPCell cellCheckNoLabel = new PdfPCell(new Phrase(lCheckNumber, new Font(bfTimes, 8)))
            {
                BackgroundColor = new BaseColor(192, 192, 192),
                HorizontalAlignment = 1
            };
            PdfPCell cellAmountLabel = new PdfPCell(new Phrase(lAmount, new Font(bfTimes, 8)))
            {
                BackgroundColor = new BaseColor(192, 192, 192),
                HorizontalAlignment = 1
            };
            PdfPCell cellDate = new PdfPCell(new Phrase(date, new Font(f_cn, 10)))
            {
                HorizontalAlignment = 2
            };
            PdfPCell cellCheckNo = new PdfPCell(new Phrase(cnumber, new Font(f_cn, 10)))
            {
                HorizontalAlignment = 2
            };
            PdfPCell cellAmount = new PdfPCell(new Phrase(amount2, new Font(f_cn, 10)))
            {
                HorizontalAlignment = 2
            };

            table.AddCell(cellDateLabel);
            table.AddCell(cellCheckNoLabel);
            table.AddCell(cellAmountLabel);
            table.AddCell(cellDate);
            table.AddCell(cellCheckNo);
            table.AddCell(cellAmount);
            table.WriteSelectedRows(0, -1, ConvertToInch(4.0f + checkhor), (ConvertToInchY(4.55f) + table.TotalHeight), cb);
            //Table

            //Clear Band
            float clearBandXPos = ConvertToInchX(0f);
            float clearBandYPos = ConvertToInchY(7f);
            float clearBandWidth = ConvertToInch(8.5f);
            float clearBandHeight = ConvertToInch(0.625f);
            cb.SetLineWidth(1);
            cb.Rectangle(clearBandXPos, clearBandYPos, clearBandWidth, clearBandHeight);
            //cb.Stroke();
            //Clear Band
            //MICR Distance From Bottom
            float distanceXPos = ConvertToInchX(0f);
            float distanceYPos = ConvertToInchY(7f);
            float distanceWidth = ConvertToInch(8.5f);
            float distanceHeight = ConvertToInch(0.150f);
            cb.SetLineWidth(1);
            cb.Rectangle(distanceXPos, distanceYPos, distanceWidth, distanceHeight);
            //cb.Stroke();
            //MICR Distance From Bottom
            //MICR Box
            float MICRXPos = ConvertToInchX(8.5f - 5.6875f);
            float MICRYPos = ConvertToInchY(7f) + distanceHeight;
            float MICRWidth = ConvertToInch(5.6875f - 0.3125f);
            float MICRHeight = ConvertToInch(0.4375f);
            cb.SetLineWidth(1);
            cb.Rectangle(MICRXPos, MICRYPos, MICRWidth, MICRHeight);
            //cb.Stroke();
            //MICR Box
            //MICR 
            cb.BeginText();
            cb.SetFontAndSize(f_micr, 15);

            cb.ShowTextAligned(Element.ALIGN_LEFT, routingP, MICRXPos + ConvertToInch(micrhor), MICRYPos - ConvertToInch(micrver), 0);
            cb.ShowTextAligned(Element.ALIGN_RIGHT, cnumberP, MICRXPos + ConvertToInch(micrhor), MICRYPos - ConvertToInch(micrver), 0);
            //cb.SetTextMatrix(MICRXPos, MICRYPos);
            //cb.ShowText(routingP);
            cb.EndText();
            //MICR 


            //CONFIRMATION OF CHECK ADD TO DOC START  
            document.Add(drawnP);
            document.Add(payeeP);
            document.Add(blanklineP);
            document.Add(memoP);
            document.Add(payfromP);
            document.Add(blanklineP);
            document.Add(payernameP);
            document.Add(payeraddressP);
            document.Add(blanklineP);
            document.Add(mcchP);
            document.Add(blanklinePbig);
            document.Add(linep);
            document.Add(linep); 
            //CONFIRMATION OF CHECK ADD TO DOC END  

            //ACTUAL CHECK START  
            document.Add(blanklineP);
            document.Add(printed2P);
            document.Add(payernamePactual);
            document.Add(payeraddressPactual);
            document.Add(trackingCodePactual);
            document.Add(notValidPactual);
            document.Add(blanklineP);
            document.Add(amountwordsP);
            document.Add(blanklinePbig3);
            document.Add(payeePactual);
            document.Add(payeeaddressPactual);
            document.Add(blanklineP);
            document.Add(memoPactual);
            document.Add(blanklinePbig2);
            document.Add(routingP1); 
            //ACTUAL CHECK END 

            //FILE COPY OF CHECK START  
            document.Add(blanklinePbig);
            document.Add(drawnP);
            document.Add(payeeP);
            document.Add(blanklineP);
            document.Add(phoneP);
            document.Add(memoP);
            document.Add(payfromP);
            document.Add(blanklineP);
            document.Add(payernameP);
            document.Add(payeraddressP);
            document.Add(blanklinePbig);
            document.Add(routingPfilecopy);
            //FILE COPY OF CHECK END 
            document.Close();
            return workStream;
        }
        private MemoryStream PdfTop(CheckInfoViewModel ci)
        {
            string cnumber = ci.CheckNumber;
            string pname = ci.PayerName;
            string pyname = ci.PayerName;
            string paddress = FormatAddressPhone(ci.PayerAddress, ci.PayerPhone);
            string pphone = ci.PayerPhone;
            string bname = ci.BankName;
            string rnumber = ci.RoutingNumber;
            string anumber = ci.AccountNumber;
            //string ppayee = ci.Payee;
            string ppayee = FormatAddressPhone(ci.Payee, ci.PayeePhone);
            string pdate = ci.Date;
            string pamount = ci.Amount;
            string cmemo = ci.Memo;
            string[] bankadd = ci.BankAddress?.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            string bankaddress = bankadd != null ? bankadd[0].ToString() : "";
            string bankcitystate = bankadd != null ? bankadd[1].ToString() : "";
            string payeename = ci.PayeeName;
            //string payeeaddress = ci.PayeeAddress;
            string payeeaddress = FormatAddressPhone(ci.PayeeAddress, ci.PayeePhone);
            pamount = float.Parse(pamount).ToString("N", new CultureInfo("en-US"));
            string pamountwords = ci.AmountInWords;

            MemoryStream workStream = new MemoryStream();
            float ytoadd = -3.3f;
            float ytoaddc = 3.3f;
            float ytoaddf = 0.2f;
            float checkhor = ci.Test == "test" ? ci.CheckHor : GetCheckHor();
            float micrhor = ci.Test == "test" ? ci.MICRHor : GetMICRHor();
            float micrver = ci.Test == "test" ? ci.MICRVer : GetMICRVer();

            string path = Server.MapPath("~");
            Document document = new Document(PageSize.LETTER, 0f, 0f, 0f, 0f);
            PdfWriter writer = PdfWriter.GetInstance(document, workStream);
            writer.CloseStream = false;

            document.Open();
            //CONFIRMATION OF CHECK VARIABLES START
            BaseFont f_cn = BaseFont.CreateFont("c:\\windows\\fonts\\arial.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BaseFont f_cn_bold = BaseFont.CreateFont(path + "\\fonts\\ARIALBD.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font f_cn_bold_10 = new Font(f_cn_bold, 10);
            Font f_cn_10 = new Font(f_cn, 10);
            Font f_cn_bold_10_times = new Font(bfTimes, 10, Font.BOLD);
            Font f_cn_bold_12_times = new Font(bfTimes, 12, Font.BOLDITALIC);
            Font smallfontnormal = FontFactory.GetFont("Arial", 8, Font.NORMAL);
            Font fontnormalbold10 = FontFactory.GetFont("Arial", 10, Font.BOLD);
            Font f_cn_arial_7 = new Font(f_cn, 7);
            Font f_cn_arial_6 = new Font(f_cn, 6);
            Font f_cn_arial_6_bold = new Font(f_cn, 6, Font.BOLD);
            Font f_cn_bold_8 = new Font(f_cn_bold, 8);
            Font f_cn_8 = new Font(f_cn, 8, Font.NORMAL, BaseColor.WHITE);
            Font f_cn_9_bolditalic = new Font(f_cn, 9, Font.BOLDITALIC, BaseColor.WHITE);


            BaseFont f_micr = BaseFont.CreateFont(path + "\\fonts\\MCCH____.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            Font micrf = new Font(f_micr, 12, Font.BOLD);
            Font smallfont = FontFactory.GetFont("Arial", 8);
            Font bigfont10 = FontFactory.GetFont("Arial", 10);
            Font bigfont14 = FontFactory.GetFont("Arial", 14);
            Font bigfont = FontFactory.GetFont("Arial", 12);
            Font bigfontmcch = FontFactory.GetFont("MICR", 12);
            Chunk glue = new Chunk(new VerticalPositionMark());

            string blankline = "\n";
            string drawn = "Drawn to Pay To";
            string payee = pyname + Environment.NewLine + ppayee;
            string confirmation = "CONFIRMATION OF CHECK";
            string confirmationdate = "Date: " + pdate + " #: " + cnumber;
            string confirmationamount = "Amount: $" + pamount;
            string phone = @"Phone #: " + pphone;
            string memo = cmemo + Environment.NewLine +
@"As you instructed a check has been prepared for " + pamount;
            string payfrom = "Pay from the account of:";
            string payername = pname;
            string payeraddress = paddress;
            string drawnonbank = "Drawn on bank:";
            string bankname = bname;
            string accountnumber = anumber;
            string micr = "A" + rnumber + "A";
            string printed = "Printed using CHAX®";
            string checksoft = "check software - 2687708";
            string printed2 = "Printed using CHAX® check software - 2687708";
            string notnego = "CONFIRMATION - NOT NEGOTIABLE";
            //CONFIRMATION OF CHECK VARIABLES END

            //ACTUAL VARIABLES START
            string amountwords = pamountwords.Replace(" Dollars", "").Replace(" Cents", "");
            string tracking = "Tracking Code:  MKGT-MKG5-3456";
            string notvalid = "*** Not valid for use as a money order or cashier's check ***";
            string memoactual = "MEMO         " + cmemo;
            Chunk routing = new Chunk(TruncateAccountNumber(rnumber, anumber), bigfont);
            Chunk routing2 = new Chunk(anumber.Substring(anumber.Length - 4), micrf);
            string digits = cnumber;
            string date = pdate;
            string amount2 = "$" + pamount;
            string sig1 = "SIGNATURE NOT REQUIRED";
            string sig2 = "Your depositor has authorized this payment to payee.";
            string sig3 = "Payee to hold you harmless for payment of this document.";
            string sig4 = "This document shall be deposited only to credit of payee.";
            string sig5 = "Absence of endorsement is guaranteed by payee.";
            string asterisks = "******************************";
            string dollars = " Dollars";
            string tothe = "TO THE";
            string order = "ORDER";
            string of = "OF";
            string authorized = "AUTHORIZED SIGNATURE";
            string routingP = "A" + rnumber + "A " + anumber + "C";
            string cnumberP = "C" + cnumber + " ";
            //ACTUAL VARIABLES END

            //FILE COPY VARIABLES START
            string routingfilecopy = micr + " " + anumber + "C";
            string filecopy = @"FILE COPY OF CHECK";
            //FILE COPY VARIABLES END

            //CONFIRMATION OF CHECK PARAGRAPHS START
            Paragraph blanklineP = new Paragraph(12, blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph blanklinePbig = new Paragraph(ConvertToInch(.45f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph blanklinePbig2 = new Paragraph(ConvertToInch(.28f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph blanklinePbig3 = new Paragraph(ConvertToInch(.58f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph drawnP = new Paragraph(9, drawn, smallfont)
            {
                IndentationLeft = ConvertToInch(.28f)
            };

            Paragraph payeeP = new Paragraph(9, payee, smallfont)
            {
                IndentationLeft = ConvertToInch(.28f)
            };

            Paragraph phoneP = new Paragraph(9, phone, smallfont)
            {
                IndentationLeft = ConvertToInch(.32f)
            };

            Paragraph memoP = new Paragraph(9, memo, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph payfromP = new Paragraph(9, payfrom, f_cn_bold_8)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph payernameP = new Paragraph(9, payername, f_cn_bold_8)
            {
                IndentationLeft = ConvertToInch(1f)
            };

            Paragraph payeraddressP = new Paragraph(9, payeraddress, smallfont)
            {
                IndentationLeft = ConvertToInch(1f)
            };

            Paragraph mcchP = new Paragraph(9, micr, micrf)
            {
                IndentationLeft = ConvertToInch(.12f),
            };

            Paragraph linep = new Paragraph(5,
                new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 5.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1))
                )
            {
                IndentationLeft = ConvertToInch(.12f)
            };
            //CONFIRMATION OF CHECK PARAGRAPHS END

            //ACTUAL CHECK PARAGRAPHS START
            Paragraph printed2P = new Paragraph(0, printed2, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph payernamePactual = new Paragraph(11, payername, f_cn_bold_10_times)
            {
                IndentationLeft = ConvertToInch(1f + checkhor)
            };

            Paragraph payeraddressPactual = new Paragraph(8, payeraddress, f_cn_arial_7)
            {
                IndentationLeft = ConvertToInch(1f + checkhor)
            };

            Paragraph trackingCodePactual = new Paragraph(12, tracking, f_cn_arial_7)
            {
                IndentationLeft = ConvertToInch(.5f + checkhor)
            };

            Paragraph notValidPactual = new Paragraph(8, notvalid, f_cn_arial_7)
            {
                IndentationLeft = ConvertToInch(.5f + checkhor)
            };

            Paragraph amountwordsP = new Paragraph(9, "PAY          ", bigfont10)
            {
                IndentationLeft = ConvertToInch(.25f + checkhor)
            };
            amountwordsP.Add(new Chunk(amountwords, f_cn_bold_12_times));

            Paragraph payeePactual = new Paragraph(9, payeename + "\n", fontnormalbold10)
            {
                IndentationLeft = ConvertToInch(.89f + checkhor)
            };

            Paragraph payeeaddressPactual = new Paragraph(9, payeeaddress, smallfontnormal)
            {
                IndentationLeft = ConvertToInch(.89f + checkhor)
            };


            Paragraph phonePactual = new Paragraph(9, phone, smallfont)
            {
                IndentationLeft = ConvertToInch(.78f)
            };

            Paragraph memoPactual = new Paragraph(9, memoactual, smallfont)
            {
                IndentationLeft = ConvertToInch(.25f + checkhor)
            };

            //Paragraph routingP = new Paragraph(9, ("A" + rnumber + "A" + anumber + "C" + cnumber), micrf)
            //{
            //    Alignment = Element.ALIGN_CENTER
            //};


            Paragraph routingP2 = new Paragraph(9, "", micrf)
            {
                Alignment = Element.ALIGN_CENTER
            };

            Chunk signatureLinep = new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 5.0F, BaseColor.BLACK, Element.ALIGN_RIGHT, 1));



            //ACTUAL CHECK PARAGRAPHS END
            //FILE COPY PARAGRAPHS START
            Paragraph phonePfileCopy = new Paragraph(15, phone, bigfont10)
            {
                IndentationLeft = ConvertToInch(.78f)
            };

            Paragraph routingPfilecopy = new Paragraph(12)
            {
                IndentationLeft = ConvertToInch(.12f),
            };

            routingPfilecopy.Add(routing);
            routingPfilecopy.Add(routing2);
            //FILE COPY PARAGRAPHS END

            PdfContentByte cb = writer.DirectContent;
            cb.BeginText();
            //ACTUAL CHECK CONTENTBYTE START  
            cb.SetFontAndSize(f_cn_bold, 7);
            cb.SetTextMatrix(ConvertToInchX(4f + checkhor), ConvertToInchY(3.60f + ytoadd));
            cb.ShowText(bankname);
            cb.SetFontAndSize(f_cn, 6);
            cb.SetTextMatrix(ConvertToInchX(4f + checkhor), ConvertToInchY(3.60f + ytoadd) - 7);
            cb.ShowText(bankaddress);
            cb.SetTextMatrix(ConvertToInchX(4f + checkhor), ConvertToInchY(3.60f + ytoadd) - 13);
            cb.ShowText(bankcitystate);
            cb.SetFontAndSize(f_cn_bold, 18);
            cb.ShowTextAligned(Element.ALIGN_RIGHT, digits, ConvertToInchX(8f + checkhor), ConvertToInchY(3.80f + ytoadd), 0);
            cb.SetFontAndSize(f_cn_bold, 14);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(4.76f + ytoadd));
            cb.ShowText(asterisks);
            cb.SetFontAndSize(f_cn_bold, 11);
            cb.SetTextMatrix(ConvertToInchX(7.4f + checkhor), ConvertToInchY(4.7f + ytoadd));
            cb.ShowText(dollars);
            cb.SetFontAndSize(f_cn, 7);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.25f + ytoadd));
            cb.ShowText(sig1);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.25f + ytoadd) - 8);
            cb.ShowText(sig2);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.25f + ytoadd) - 16);
            cb.ShowText(sig3);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.25f + ytoadd) - 24);
            cb.ShowText(sig4);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.25f + ytoadd) - 32);
            cb.ShowText(sig5);
            cb.SetFontAndSize(f_cn, 10);
            cb.SetTextMatrix(ConvertToInchX(.25f + checkhor), ConvertToInchY(5.15f + ytoadd));
            cb.ShowText(tothe);
            cb.SetTextMatrix(ConvertToInchX(.25f + checkhor), ConvertToInchY(5.15f + ytoadd) - 10);
            cb.ShowText(order);
            cb.SetTextMatrix(ConvertToInchX(.25f + checkhor), ConvertToInchY(5.15f + ytoadd) - 20);
            cb.ShowText(of);
            cb.SetFontAndSize(f_cn_bold, 10);
            cb.SetTextMatrix(ConvertToInchX(5.75f + checkhor), ConvertToInchY(5.91f + ytoadd));
            cb.ShowText(pyname);
            cb.SetFontAndSize(f_cn, 7);
            cb.SetTextMatrix(ConvertToInchX(5.75f + checkhor), ConvertToInchY(6.1f + ytoadd));
            cb.ShowText(authorized);

            //ACTUAL CHECK CONTENTBYTE END
            //CONFIRMATION OF CHECK CONTENTBYTE START           
            cb.SetFontAndSize(f_cn_bold, 12);
            cb.SetTextMatrix(ConvertToInchX(4.70f), ConvertToInchY(.6f + ytoaddc));
            cb.ShowText(confirmation);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(.6f + ytoaddc) - 12);
            cb.ShowText(confirmationdate);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(.6f + ytoaddc) - 24);
            cb.ShowText(confirmationamount);
            cb.SetFontAndSize(f_cn, 8);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(1.75f + ytoaddc));
            cb.ShowText(drawnonbank);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(1.75f + ytoaddc) - 8);
            cb.ShowText(bankname);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(1.75f + ytoaddc) - 16);
            cb.ShowText(accountnumber);
            cb.SetFontAndSize(f_cn_bold, 12);
            cb.SetTextMatrix(ConvertToInchX(5.80f), ConvertToInchY(2.42f + ytoaddc));
            cb.ShowText(printed);
            cb.SetFontAndSize(f_cn, 12);
            cb.SetTextMatrix(ConvertToInchX(5.80f), ConvertToInchY(2.42f + ytoaddc) - 10);
            cb.ShowText(checksoft);
            cb.SetFontAndSize(f_cn_bold, 10);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(2.42f + ytoaddc) - 20);
            cb.ShowText(notnego);
            //CONFIRMATION OF CHECK CONTENTBYTE END  

            //FILE COPY CONTENTBYTE START       
            cb.SetFontAndSize(f_cn_bold, 12);
            cb.SetTextMatrix(ConvertToInchX(4.70f), ConvertToInchY(7.47f + ytoaddf));
            cb.ShowText(filecopy);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(7.47f + ytoaddf) - 12);
            cb.ShowText(confirmationdate);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(7.47f + ytoaddf) - 24);
            cb.ShowText(confirmationamount);
            cb.SetFontAndSize(f_cn, 8);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(8.60f + ytoaddf));
            cb.ShowText(drawnonbank);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(8.60f + ytoaddf) - 8);
            cb.ShowText(bankname);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(8.60f + ytoaddf) - 16);
            cb.ShowText(accountnumber);
            cb.SetFontAndSize(f_cn_bold, 12);
            cb.SetTextMatrix(ConvertToInchX(5.80f), ConvertToInchY(9.30f + ytoaddf));
            cb.ShowText(printed);
            cb.SetFontAndSize(f_cn, 12);
            cb.SetTextMatrix(ConvertToInchX(5.80f), ConvertToInchY(9.30f + ytoaddf) - 10);
            cb.ShowText(checksoft);
            cb.SetFontAndSize(f_cn_bold, 10);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(9.30f + ytoaddf) - 20);
            cb.ShowText(notnego);
            //FILE COPY CONTENTBYTE END
            cb.EndText();

            cb.MoveTo(ConvertToInchX(7.8f + checkhor), ConvertToInchY(2.65f));
            cb.LineTo(ConvertToInchX(5.25f + checkhor), ConvertToInchY(2.65f));
            cb.Stroke();

            //Table
            string lDate = "Date";
            string lCheckNumber = "Check No.";
            string lAmount = "Amount";
            PdfPTable table = new PdfPTable(3)
            {
                TotalWidth = 290
            };

            PdfPCell cellDateLabel = new PdfPCell(new Phrase(lDate, new Font(bfTimes, 8)))
            {
                BackgroundColor = new BaseColor(192, 192, 192),
                HorizontalAlignment = 1
            };
            PdfPCell cellCheckNoLabel = new PdfPCell(new Phrase(lCheckNumber, new Font(bfTimes, 8)))
            {
                BackgroundColor = new BaseColor(192, 192, 192),
                HorizontalAlignment = 1
            };
            PdfPCell cellAmountLabel = new PdfPCell(new Phrase(lAmount, new Font(bfTimes, 8)))
            {
                BackgroundColor = new BaseColor(192, 192, 192),
                HorizontalAlignment = 1
            };
            PdfPCell cellDate = new PdfPCell(new Phrase(date, new Font(f_cn, 10)))
            {
                HorizontalAlignment = 2
            };
            PdfPCell cellCheckNo = new PdfPCell(new Phrase(cnumber, new Font(f_cn, 10)))
            {
                HorizontalAlignment = 2
            };
            PdfPCell cellAmount = new PdfPCell(new Phrase(amount2, new Font(f_cn, 10)))
            {
                HorizontalAlignment = 2
            };

            table.AddCell(cellDateLabel);
            table.AddCell(cellCheckNoLabel);
            table.AddCell(cellAmountLabel);
            table.AddCell(cellDate);
            table.AddCell(cellCheckNo);
            table.AddCell(cellAmount);
            table.WriteSelectedRows(0, -1, ConvertToInch(4.0f + checkhor), (ConvertToInchY(1.1f) + table.TotalHeight), cb);
            //Table

            //Clear Band
            float clearBandXPos = ConvertToInchX(0f);
            float clearBandYPos = ConvertToInchY(3.5f);
            float clearBandWidth = ConvertToInch(8.5f);
            float clearBandHeight = ConvertToInch(0.625f);
            cb.SetLineWidth(1);
            cb.Rectangle(clearBandXPos, clearBandYPos, clearBandWidth, clearBandHeight);
            //cb.Stroke();
            //Clear Band
            //MICR Distance From Bottom
            float distanceXPos = ConvertToInchX(0f);
            float distanceYPos = ConvertToInchY(3.5f);
            float distanceWidth = ConvertToInch(8.5f);
            float distanceHeight = ConvertToInch(0.150f);
            cb.SetLineWidth(1);
            cb.Rectangle(distanceXPos, distanceYPos, distanceWidth, distanceHeight);
            //cb.Stroke();
            //MICR Distance From Bottom
            //MICR Box
            float MICRXPos = ConvertToInchX(8.5f - 5.6875f);
            float MICRYPos = ConvertToInchY(3.5f) + distanceHeight;
            float MICRWidth = ConvertToInch(5.6875f - 0.3125f);
            float MICRHeight = ConvertToInch(0.4375f);
            cb.SetLineWidth(1);
            cb.Rectangle(MICRXPos, MICRYPos, MICRWidth, MICRHeight);
            //cb.Stroke();
            //MICR Box
            //MICR 
            cb.BeginText();
            cb.SetFontAndSize(f_micr, 15);
            cb.ShowTextAligned(Element.ALIGN_LEFT, routingP, MICRXPos + ConvertToInch(micrhor), MICRYPos - ConvertToInch(micrver), 0);
            cb.ShowTextAligned(Element.ALIGN_RIGHT, cnumberP, MICRXPos + ConvertToInch(micrhor), MICRYPos - ConvertToInch(micrver), 0);
            cb.EndText();
            //MICR 

            //ACTUAL CHECK START  
            document.Add(blanklineP); 
            document.Add(printed2P);
            document.Add(payernamePactual);
            document.Add(payeraddressPactual);
            document.Add(trackingCodePactual);
            document.Add(notValidPactual);
            document.Add(blanklineP);
            document.Add(amountwordsP);
            document.Add(blanklinePbig2);
            document.Add(payeePactual);
            document.Add(payeeaddressPactual);
            document.Add(blanklineP);
            document.Add(memoPactual);
            document.Add(blanklinePbig2);
            document.Add(routingP2);
            //ACTUAL CHECK END 
            //CONFIRMATION OF CHECK ADD TO DOC START 
            document.Add(blanklinePbig3);
            document.Add(drawnP);
            document.Add(payeeP);
            document.Add(blanklineP);
            document.Add(memoP);
            document.Add(payfromP);
            document.Add(blanklineP);
            document.Add(payernameP);
            document.Add(payeraddressP);
            document.Add(blanklinePbig);
            document.Add(mcchP);
            document.Add(blanklinePbig);
            document.Add(linep);
            document.Add(linep);
            //CONFIRMATION OF CHECK ADD TO DOC END  

            //FILE COPY OF CHECK START  
            document.Add(blanklineP);
            document.Add(drawnP);
            document.Add(payeeP);
            document.Add(blanklineP);
            document.Add(memoP);
            document.Add(payfromP);
            document.Add(blanklineP);
            document.Add(payernameP);
            document.Add(payeraddressP);
            document.Add(blanklinePbig);
            document.Add(routingPfilecopy);
            //FILE COPY OF CHECK END 
            document.Close();


            return workStream;
        }
        private MemoryStream PdfBottom(CheckInfoViewModel ci)
        {
            string cnumber = ci.CheckNumber;
            string pname = ci.PayerName;
            string pyname = ci.PayerName;
            string paddress = FormatAddressPhone(ci.PayerAddress, ci.PayerPhone);
            string pphone = ci.PayerPhone;
            string bname = ci.BankName;
            string rnumber = ci.RoutingNumber;
            string anumber = ci.AccountNumber;
            //string ppayee = ci.Payee;
            string ppayee = FormatAddressPhone(ci.Payee, ci.PayeePhone);
            string pdate = ci.Date;
            string pamount = ci.Amount;
            string cmemo = ci.Memo;
            //string payeeaddress = ci.PayeeAddress;
            string payeeaddress = FormatAddressPhone(ci.PayeeAddress, ci.PayeePhone);
            string payeename = ci.PayeeName;
            pamount = float.Parse(pamount).ToString("N", new CultureInfo("en-US"));
            string pamountwords = ci.AmountInWords;
            string[] bankadd = ci.BankAddress?.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            string bankaddress = bankadd != null ? bankadd[0].ToString() : "";
            string bankcitystate = bankadd != null ? bankadd[1].ToString() : "";

            MemoryStream workStream = new MemoryStream();
            float ytoadd = 3.75f;
            float ytoaddc = 3.3f;
            float ytoaddf = -7f;
            float checkhor = ci.Test == "test" ? ci.CheckHor : GetCheckHor();
            float micrhor = ci.Test == "test" ? ci.MICRHor : GetMICRHor();
            float micrver = ci.Test == "test" ? ci.MICRVer : GetMICRVer();

            string path = Server.MapPath("~");
            Document document = new Document(PageSize.LETTER, 0f, 0f, 10f, 0f);
            PdfWriter writer = PdfWriter.GetInstance(document, workStream);
            writer.CloseStream = false;

            document.Open();
            //CONFIRMATION OF CHECK VARIABLES START
            BaseFont f_cn = BaseFont.CreateFont("c:\\windows\\fonts\\arial.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BaseFont f_cn_bold = BaseFont.CreateFont(path + "\\fonts\\ARIALBD.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            Font f_cn_bold_10 = new Font(f_cn_bold, 10);
            Font f_cn_bold_8 = new Font(f_cn_bold, 8);
            Font f_cn_8 = new Font(f_cn, 8, Font.NORMAL, BaseColor.WHITE);
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            BaseFont f_micr = BaseFont.CreateFont(path + "\\fonts\\MCCH____.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            Font micrf = new Font(f_micr, 12, Font.BOLD);
            Font smallfont = FontFactory.GetFont("Arial", 8);
            Font bigfont10 = FontFactory.GetFont("Arial", 10);
            Font bigfont14 = FontFactory.GetFont("Arial", 14);
            Font bigfont = FontFactory.GetFont("Arial", 12);
            Font bigfontmcch = FontFactory.GetFont("MICR", 12);
            Font f_cn_arial_7 = new Font(f_cn, 7);
            Font f_cn_bold_12_times = new Font(bfTimes, 12, Font.BOLDITALIC);
            Font f_cn_bold_10_times = new Font(bfTimes, 10, Font.BOLD);
            Font smallfontnormal = FontFactory.GetFont("Arial", 8, Font.NORMAL);
            Font fontnormalbold10 = FontFactory.GetFont("Arial", 10, Font.BOLD);
            Chunk glue = new Chunk(new VerticalPositionMark());
            string blankline = "\n";
            string drawn = "Drawn to Pay To";
            string payee = pyname + Environment.NewLine + ppayee;
            string confirmation = "CONFIRMATION OF CHECK";
            string confirmationdate = "Date: " + pdate + " #: " + cnumber;
            string confirmationamount = "Amount: $" + pamount;
            string phone = @"Phone #: " + pphone;
            string memo = cmemo + Environment.NewLine +
@"As you instructed a check has been prepared for " + pamount;
            string payfrom = @"Pay from the account of:";
            string payername = pname;
            string payeraddress = paddress;
            string drawnonbank = "Drawn on bank:";
            string bankname = bname;
            string accountnumber = anumber;
            string micr = "A" + rnumber + "A";
            string printed = "Printed using CHAX®";
            string checksoft = "check software - 2687708";
            string printed2 = "Printed using CHAX® check software - 2687708";
            string notnego = "CONFIRMATION - NOT NEGOTIABLE";

            //CONFIRMATION OF CHECK VARIABLES END

            //ACTUAL VARIABLES START
            string amountwords = pamountwords.Replace(" Dollars", "").Replace(" Cents", "");
            string memoactual = "MEMO         " + cmemo;
            Chunk routing = new Chunk(TruncateAccountNumber(rnumber, anumber), bigfont);
            Chunk routing2 = new Chunk(anumber.Substring(anumber.Length - 4), micrf);
            string digits = cnumber;
            string date = pdate;
            string amount2 = "$" + pamount;
            string sig1 = "SIGNATURE NOT REQUIRED";
            string sig2 = "Your depositor has authorized this payment to payee.";
            string sig3 = "Payee to hold you harmless for payment of this document.";
            string sig4 = "This document shall be deposited only to credit of payee.";
            string sig5 = "Absence of endorsement is guaranteed by payee.";
            string tracking = "Tracking Code:  MKGT-MKG5-3456";
            string notvalid = "*** Not valid for use as a money order or cashier's check ***";
            string tothe = "TO THE";
            string order = "ORDER";
            string of = "OF";
            string asterisks = "******************************";
            string dollars = " Dollars";
            string authorized = "AUTHORIZED SIGNATURE";
            string routingP = "A" + rnumber + "A " + anumber + "C";
            string cnumberP = "C" + cnumber + " ";
            //ACTUAL VARIABLES END

            //FILE COPY VARIABLES START
            string routingfilecopy = micr + " " + anumber + "C";
            string filecopy = @"FILE COPY OF CHECK";
            //FILE COPY VARIABLES END

            //CONFIRMATION OF CHECK PARAGRAPHS START
            Paragraph blanklineP = new Paragraph(12, blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph blanklinePbig = new Paragraph(ConvertToInch(.45f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph blanklinePbig2 = new Paragraph(ConvertToInch(.38f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph blanklinePbig3 = new Paragraph(ConvertToInch(.25f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph drawnP = new Paragraph(9, drawn, smallfont)
            {
                IndentationLeft = ConvertToInch(.28f)
            };

            Paragraph payeeP = new Paragraph(9, payee, smallfont)
            {
                IndentationLeft = ConvertToInch(.28f)
            };

            Paragraph phoneP = new Paragraph(9, phone, smallfont)
            {
                IndentationLeft = ConvertToInch(.32f)
            };

            Paragraph memoP = new Paragraph(9, memo, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph payfromP = new Paragraph(9, payfrom, f_cn_bold_8)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph payernameP = new Paragraph(9, payername, f_cn_bold_8)
            {
                IndentationLeft = ConvertToInch(1f)
            };

            Paragraph payeraddressP = new Paragraph(9, payeraddress, smallfont)
            {
                IndentationLeft = ConvertToInch(1f)
            };

            Paragraph mcchP = new Paragraph(5, micr, micrf)
            {
                IndentationLeft = ConvertToInch(.12f),
            };

            Paragraph linep = new Paragraph(8,
                new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 5.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1))
                )
            {
                IndentationLeft = ConvertToInch(.12f)
            };
            //CONFIRMATION OF CHECK PARAGRAPHS END

            //ACTUAL CHECK PARAGRAPHS START
            Paragraph printed2P = new Paragraph(0, printed2, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph payernamePactual = new Paragraph(11, payername, f_cn_bold_10_times)
            {
                IndentationLeft = ConvertToInch(1f + checkhor)
            };

            Paragraph payeraddressPactual = new Paragraph(8, payeraddress, f_cn_arial_7)
            {
                IndentationLeft = ConvertToInch(1f + checkhor)
            };

            Paragraph amountwordsP = new Paragraph(9, "PAY          ", bigfont10)
            {
                IndentationLeft = ConvertToInch(.25f + checkhor)
            };

            amountwordsP.Add(new Chunk(amountwords, f_cn_bold_12_times));

            Paragraph payeePactual = new Paragraph(9, payeename + "\n", fontnormalbold10)
            {
                IndentationLeft = ConvertToInch(.89f + checkhor)
            };

            Paragraph payeeaddressPactual = new Paragraph(9, payeeaddress, smallfontnormal)
            {
                IndentationLeft = ConvertToInch(.89f + checkhor)
            };

            Paragraph phonePactual = new Paragraph(9, phone, smallfont)
            {
                IndentationLeft = ConvertToInch(.78f)
            };

            Paragraph memoPactual = new Paragraph(9, memoactual, smallfont)
            {
                IndentationLeft = ConvertToInch(.25f + checkhor)
            };

            //Paragraph routingP = new Paragraph(9, ("A" + rnumber + "A" + anumber + "C" + cnumber), micrf)
            //{
            //    Alignment = Element.ALIGN_CENTER
            //};

            Paragraph routingP2 = new Paragraph(9, "", micrf)
            {
                Alignment = Element.ALIGN_CENTER
            };

            Paragraph trackingCodePactual = new Paragraph(12, tracking, f_cn_arial_7)
            {
                IndentationLeft = ConvertToInch(.5f + checkhor)
            };

            Paragraph notValidPactual = new Paragraph(8, notvalid, f_cn_arial_7)
            {
                IndentationLeft = ConvertToInch(.5f + checkhor)
            };

            //ACTUAL CHECK PARAGRAPHS END
            //FILE COPY PARAGRAPHS START
            Paragraph phonePfileCopy = new Paragraph(15, phone, bigfont10)
            {
                IndentationLeft = ConvertToInch(.78f)
            };

            Paragraph routingPfilecopy = new Paragraph(12)
            {
                IndentationLeft = ConvertToInch(.12f),
            };

            routingPfilecopy.Add(routing);
            routingPfilecopy.Add(routing2);
            //FILE COPY PARAGRAPHS END

            PdfContentByte cb = writer.DirectContent;
            cb.BeginText();
            //FILE COPY CONTENTBYTE START       
            cb.SetFontAndSize(f_cn_bold, 12);
            cb.SetTextMatrix(ConvertToInchX(4.70f), ConvertToInchY(7.47f + ytoaddf));
            cb.ShowText(filecopy);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(7.47f + ytoaddf) - 12);
            cb.ShowText(confirmationdate);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(7.47f + ytoaddf) - 24);
            cb.ShowText(confirmationamount);
            cb.SetFontAndSize(f_cn, 8);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(8.60f + ytoaddf));
            cb.ShowText(drawnonbank);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(8.60f + ytoaddf) - 8);
            cb.ShowText(bankname);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(8.60f + ytoaddf) - 16);
            cb.ShowText(accountnumber);
            cb.SetFontAndSize(f_cn_bold, 10);
            cb.SetTextMatrix(ConvertToInchX(5.80f), ConvertToInchY(9.30f + ytoaddf));
            cb.ShowText(printed);
            cb.SetFontAndSize(f_cn, 10);
            cb.SetTextMatrix(ConvertToInchX(5.80f), ConvertToInchY(9.30f + ytoaddf) - 8);
            cb.ShowText(checksoft);
            cb.SetFontAndSize(f_cn_bold, 10);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(9.30f + ytoaddf) - 18);
            cb.ShowText(notnego);
            //FILE COPY CONTENTBYTE END
            //CONFIRMATION OF CHECK CONTENTBYTE START           
            cb.SetFontAndSize(f_cn_bold, 12);
            cb.SetTextMatrix(ConvertToInchX(4.70f), ConvertToInchY(.6f + ytoaddc));
            cb.ShowText(confirmation);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(.6f + ytoaddc) - 12);
            cb.ShowText(confirmationdate);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(.6f + ytoaddc) - 24);
            cb.ShowText(confirmationamount);
            cb.SetFontAndSize(f_cn, 8);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(1.75f + ytoaddc));
            cb.ShowText(drawnonbank);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(1.75f + ytoaddc) - 8);
            cb.ShowText(bankname);
            cb.SetTextMatrix(ConvertToInchX(5.1f), ConvertToInchY(1.75f + ytoaddc) - 16);
            cb.ShowText(accountnumber);
            cb.SetFontAndSize(f_cn_bold, 12);
            cb.SetTextMatrix(ConvertToInchX(5.80f), ConvertToInchY(2.42f + ytoaddc));
            cb.ShowText(printed);
            cb.SetFontAndSize(f_cn, 12);
            cb.SetTextMatrix(ConvertToInchX(5.80f), ConvertToInchY(2.42f + ytoaddc) - 10);
            cb.ShowText(checksoft);
            cb.SetFontAndSize(f_cn_bold, 10);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(2.42f + ytoaddc) - 20);
            cb.ShowText(notnego);
            //CONFIRMATION OF CHECK CONTENTBYTE END  
            //ACTUAL CHECK CONTENTBYTE START  
            cb.SetFontAndSize(f_cn_bold, 7);
            cb.SetTextMatrix(ConvertToInchX(4f + checkhor), ConvertToInchY(3.6f + ytoadd));
            cb.ShowText(bankname);
            cb.SetFontAndSize(f_cn, 6);
            cb.SetTextMatrix(ConvertToInchX(4f + checkhor), ConvertToInchY(3.6f + ytoadd) - 7);
            cb.ShowText(bankaddress);
            cb.SetTextMatrix(ConvertToInchX(4f + checkhor), ConvertToInchY(3.6f + ytoadd) - 13);
            cb.ShowText(bankcitystate);
            cb.SetFontAndSize(f_cn_bold, 18);
            cb.ShowTextAligned(Element.ALIGN_RIGHT, digits, ConvertToInchX(8f + checkhor), ConvertToInchY(3.8f + ytoadd), 0);
            cb.SetFontAndSize(f_cn_bold, 14);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(4.78f + ytoadd));
            cb.ShowText(asterisks);
            cb.SetFontAndSize(f_cn_bold, 11);
            cb.SetTextMatrix(ConvertToInchX(7.4f + checkhor), ConvertToInchY(4.72f + ytoadd));
            cb.ShowText(dollars);

            cb.SetFontAndSize(f_cn, 7);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.22f + ytoadd));
            cb.ShowText(sig1);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.22f + ytoadd) - 8);
            cb.ShowText(sig2);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.22f + ytoadd) - 16);
            cb.ShowText(sig3);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.22f + ytoadd) - 24);
            cb.ShowText(sig4);
            cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.22f + ytoadd) - 32);
            cb.ShowText(sig5);

            cb.SetFontAndSize(f_cn, 10);
            cb.SetTextMatrix(ConvertToInchX(.25f + checkhor), ConvertToInchY(5.12f + ytoadd));
            cb.ShowText(tothe);
            cb.SetTextMatrix(ConvertToInchX(.25f + checkhor), ConvertToInchY(5.12f + ytoadd) - 10);
            cb.ShowText(order);
            cb.SetTextMatrix(ConvertToInchX(.25f + checkhor), ConvertToInchY(5.12f + ytoadd) - 20);
            cb.ShowText(of);

            cb.SetFontAndSize(f_cn_bold, 10);
            cb.SetTextMatrix(ConvertToInchX(5.75f + checkhor), ConvertToInchY(5.87f + ytoadd));
            cb.ShowText(pyname);
            cb.SetFontAndSize(f_cn, 7);
            cb.SetTextMatrix(ConvertToInchX(5.75f + checkhor), ConvertToInchY(6.07f + ytoadd));
            cb.ShowText(authorized);

            cb.EndText();
            //ACTUAL CHECK CONTENTBYTE END

            cb.MoveTo(ConvertToInchX(7.8f + checkhor), ConvertToInchY(9.67f));
            cb.LineTo(ConvertToInchX(5.25f + checkhor), ConvertToInchY(9.67f));
            cb.Stroke();

            //Table
            string lDate = "Date";
            string lCheckNumber = "Check No.";
            string lAmount = "Amount";
            PdfPTable table = new PdfPTable(3)
            {
                TotalWidth = 290
            };

            PdfPCell cellDateLabel = new PdfPCell(new Phrase(lDate, new Font(bfTimes, 8)))
            {
                BackgroundColor = new BaseColor(192, 192, 192),
                HorizontalAlignment = 1
            };
            PdfPCell cellCheckNoLabel = new PdfPCell(new Phrase(lCheckNumber, new Font(bfTimes, 8)))
            {
                BackgroundColor = new BaseColor(192, 192, 192),
                HorizontalAlignment = 1
            };
            PdfPCell cellAmountLabel = new PdfPCell(new Phrase(lAmount, new Font(bfTimes, 8)))
            {
                BackgroundColor = new BaseColor(192, 192, 192),
                HorizontalAlignment = 1
            };
            PdfPCell cellDate = new PdfPCell(new Phrase(date, new Font(f_cn, 10)))
            {
                HorizontalAlignment = 2
            };
            PdfPCell cellCheckNo = new PdfPCell(new Phrase(cnumber, new Font(f_cn, 10)))
            {
                HorizontalAlignment = 2
            };
            PdfPCell cellAmount = new PdfPCell(new Phrase(amount2, new Font(f_cn, 10)))
            {
                HorizontalAlignment = 2
            };

            table.AddCell(cellDateLabel);
            table.AddCell(cellCheckNoLabel);
            table.AddCell(cellAmountLabel);
            table.AddCell(cellDate);
            table.AddCell(cellCheckNo);
            table.AddCell(cellAmount);
            table.WriteSelectedRows(0, -1, ConvertToInch(4.0f + checkhor), (ConvertToInchY(8.15f) + table.TotalHeight), cb);
            //Table

            //Clear Band
            float clearBandXPos = ConvertToInchX(0f);
            float clearBandYPos = ConvertToInchY(10.5f);
            float clearBandWidth = ConvertToInch(8.5f);
            float clearBandHeight = ConvertToInch(0.625f);
            cb.SetLineWidth(1);
            cb.Rectangle(clearBandXPos, clearBandYPos, clearBandWidth, clearBandHeight);
            //cb.Stroke();
            //Clear Band
            //MICR Distance From Bottom
            float distanceXPos = ConvertToInchX(0f);
            float distanceYPos = ConvertToInchY(10.5f);
            float distanceWidth = ConvertToInch(8.5f);
            float distanceHeight = ConvertToInch(0.150f);
            cb.SetLineWidth(1);
            cb.Rectangle(distanceXPos, distanceYPos, distanceWidth, distanceHeight);
            //cb.Stroke();
            //MICR Distance From Bottom
            //MICR Box
            float MICRXPos = ConvertToInchX(8.5f - 5.6875f);
            float MICRYPos = ConvertToInchY(10.5f) + distanceHeight;
            float MICRWidth = ConvertToInch(5.6875f - 0.3125f);
            float MICRHeight = ConvertToInch(0.4375f);
            cb.SetLineWidth(1);
            cb.Rectangle(MICRXPos, MICRYPos, MICRWidth, MICRHeight);
            //cb.Stroke();
            //MICR Box
            //MICR 
            cb.BeginText();
            cb.SetFontAndSize(f_micr, 15);
            cb.ShowTextAligned(Element.ALIGN_LEFT, routingP, MICRXPos + ConvertToInch(micrhor), MICRYPos - ConvertToInch(micrver), 0);
            cb.ShowTextAligned(Element.ALIGN_RIGHT, cnumberP, MICRXPos + ConvertToInch(micrhor), MICRYPos - ConvertToInch(micrver), 0);
            cb.EndText();
            //MICR 

            //FILE COPY OF CHECK START  
            document.Add(drawnP);
            document.Add(payeeP);
            document.Add(blanklineP);
            document.Add(memoP);
            document.Add(payfromP);
            document.Add(blanklineP);
            document.Add(payernameP);
            document.Add(payeraddressP);
            document.Add(blanklineP);
            document.Add(routingPfilecopy);
            //FILE COPY OF CHECK END 

            //CONFIRMATION OF CHECK ADD TO DOC START 
            document.Add(blanklinePbig);
            document.Add(blanklineP);
            document.Add(blanklineP);
            document.Add(drawnP);
            document.Add(payeeP);
            document.Add(blanklineP);
            document.Add(memoP);
            document.Add(payfromP);
            document.Add(blanklineP);
            document.Add(payernameP);
            document.Add(payeraddressP);
            document.Add(blanklinePbig);
            document.Add(mcchP);
            document.Add(blanklineP);
            document.Add(linep);
            document.Add(linep);
            document.Add(blanklinePbig2);
            //CONFIRMATION OF CHECK ADD TO DOC END  

            //ACTUAL CHECK START  
            //document.Add(blanklineP);
            document.Add(printed2P);
            document.Add(payernamePactual);
            document.Add(payeraddressPactual);
            document.Add(trackingCodePactual);
            document.Add(notValidPactual);
            document.Add(blanklineP);
            document.Add(amountwordsP);
            document.Add(blanklinePbig3);
            document.Add(payeePactual);
            document.Add(payeeaddressPactual);
            document.Add(blanklineP);
            document.Add(memoPactual);
            document.Add(blanklinePbig2);
            document.Add(routingP2);
            //ACTUAL CHECK END 

            document.Close();
            return workStream;
        }
        private MemoryStream PdfBatch2(CheckInfoViewModel ci)
        {
            string cnumber = ci.CheckNumber;
            string pname = ci.PayerName;
            string pyname = ci.PayerName;
            string paddress = FormatAddressPhone(ci.PayerAddress, ci.PayerPhone);
            string pphone = ci.PayerPhone;
            string bname = ci.BankName;
            string rnumber = ci.RoutingNumber;
            string anumber = ci.AccountNumber;
            //string ppayee = ci.Payee;
            string ppayee = FormatAddressPhone(ci.Payee, ci.PayeePhone);
            string pdate = ci.Date;
            string pamount = ci.Amount;
            string cmemo = ci.Memo;
            pamount = float.Parse(pamount).ToString("N", new CultureInfo("en-US"));
            string pamountwords = ci.AmountInWords;

            MemoryStream workStream = new MemoryStream();
            float ytoadd = -3.3f;
            float ytoadd2 = 0.23f;
            float ytoadd3 = 3.75f;
            string path = Server.MapPath("~");
            Document document = new Document(PageSize.LETTER, 0f, 0f, 0f, 0f);
            PdfWriter writer = PdfWriter.GetInstance(document, workStream);
            writer.CloseStream = false;

            document.Open();
            //CONFIRMATION OF CHECK VARIABLES START
            BaseFont f_cn = BaseFont.CreateFont("c:\\windows\\fonts\\arial.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BaseFont f_cn_bold = BaseFont.CreateFont(path + "\\fonts\\ARIALBD.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            Font f_cn_bold_10 = new Font(f_cn_bold, 10);
            Font f_cn_bold_8 = new Font(f_cn_bold, 8);
            Font f_cn_8 = new Font(f_cn, 8, Font.NORMAL, BaseColor.WHITE);


            BaseFont f_micr = BaseFont.CreateFont(path + "\\fonts\\MCCH____.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            Font micrf = new Font(f_micr, 12, Font.BOLD);
            Font smallfont = FontFactory.GetFont("Arial", 8);
            Font bigfont10 = FontFactory.GetFont("Arial", 10);
            Font bigfont14 = FontFactory.GetFont("Arial", 14);
            Font bigfont = FontFactory.GetFont("Arial", 12);
            Font bigfontmcch = FontFactory.GetFont("MICR", 12);
            Chunk glue = new Chunk(new VerticalPositionMark());

            string blankline = "\n";
            string drawn = "Drawn to Pay To";
            string payee = pyname + Environment.NewLine + ppayee;
            string confirmationdate = "Date: " + pdate + " #: " + cnumber;
            string confirmationamount = "Amount: $" + pamount;
            string phone = @"Phone #: " + pphone;
            string memo = cmemo + Environment.NewLine +
@"As you instructed a check has been prepared for " + pamount;
            string payfrom = "Pay from the account of:";
            string payername = pname;
            string payeraddress = paddress;
            string bankname = bname;
            string accountnumber = anumber;
            string micr = "A-------- - A--------0123C";
            string printed2 = "Printed using CHAX® check software - 2687708";
            //CONFIRMATION OF CHECK VARIABLES END

            //ACTUAL VARIABLES START
            string amountwords = "****" + pamountwords;
            string memoactual = cmemo;
            Chunk routing = new Chunk(TruncateAccountNumber(rnumber, anumber), bigfont);
            Chunk routing2 = new Chunk(anumber.Substring(anumber.Length - 4), micrf);
            string digits = cnumber;
            string date = pdate;
            string amount2 = "$" + pamount;
            string sig1 = "SIGNATURE NOT REQUIRED";
            string sig2 = "Your depositor has authorized this payment to payee.";
            string sig3 = "Payee to hold you harmless for payment of this document.";
            string sig4 = "This document shall be deposited only to credit of payee.";
            string sig5 = "Absence of endorsement is guaranteed by payee.";
            //ACTUAL VARIABLES END

            //FILE COPY VARIABLES START
            string routingfilecopy = micr + " " + anumber + "C";
            //FILE COPY VARIABLES END

            //CONFIRMATION OF CHECK PARAGRAPHS START
            Paragraph blanklineP = new Paragraph(12, blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph blanklinePbig = new Paragraph(ConvertToInch(.45f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };
            Paragraph blanklinePbigbatch = new Paragraph(ConvertToInch(.60f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph blanklinePbig2 = new Paragraph(ConvertToInch(.38f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph blanklinePbig3 = new Paragraph(ConvertToInch(.28f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph drawnP = new Paragraph(9, drawn, smallfont)
            {
                IndentationLeft = ConvertToInch(.28f)
            };

            Paragraph payeeP = new Paragraph(9, payee, smallfont)
            {
                IndentationLeft = ConvertToInch(.28f)
            };

            Paragraph phoneP = new Paragraph(9, phone, smallfont)
            {
                IndentationLeft = ConvertToInch(.32f)
            };

            Paragraph memoP = new Paragraph(9, memo, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph payfromP = new Paragraph(9, payfrom, f_cn_bold_8)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph payernameP = new Paragraph(9, payername, f_cn_bold_8)
            {
                IndentationLeft = ConvertToInch(1f)
            };

            Paragraph payeraddressP = new Paragraph(9, payeraddress, smallfont)
            {
                IndentationLeft = ConvertToInch(1f)
            };

            Paragraph mcchP = new Paragraph(9, micr, micrf)
            {
                IndentationLeft = ConvertToInch(.12f),
            };

            Paragraph linep = new Paragraph(5,
                new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 5.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1))
                )
            {
                IndentationLeft = ConvertToInch(.12f)
            };
            //CONFIRMATION OF CHECK PARAGRAPHS END

            //ACTUAL CHECK PARAGRAPHS START
            Paragraph printed2P = new Paragraph(0, printed2, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph payernamePactual = new Paragraph(11, payername, f_cn_bold_10)
            {
                IndentationLeft = ConvertToInch(.53f)
            };

            Paragraph payeraddressPactual = new Paragraph(11, payeraddress, bigfont10)
            {
                IndentationLeft = ConvertToInch(.53f)
            };

            Paragraph amountwordsP = new Paragraph(9, amountwords, bigfont)
            {
                IndentationLeft = ConvertToInch(.62f)
            };

            Paragraph payeePactual = new Paragraph(9, payee, smallfont)
            {
                IndentationLeft = ConvertToInch(.78f)
            };

            Paragraph phonePactual = new Paragraph(9, phone, smallfont)
            {
                IndentationLeft = ConvertToInch(.78f)
            };

            Paragraph memoPactual = new Paragraph(9, memoactual, bigfont10)
            {
                IndentationLeft = ConvertToInch(.78f)
            };

            Paragraph routingP = new Paragraph(9)
            {
                IndentationLeft = ConvertToInch(.78f),

            };

            routingP.Add(routing);
            routingP.Add(routing2);
            //ACTUAL CHECK PARAGRAPHS END
            //FILE COPY PARAGRAPHS START
            Paragraph phonePfileCopy = new Paragraph(15, phone, bigfont10)
            {
                IndentationLeft = ConvertToInch(.78f)
            };

            Paragraph routingPfilecopy = new Paragraph(12, routingfilecopy, micrf)
            {
                IndentationLeft = ConvertToInch(.12f),
            };
            //FILE COPY PARAGRAPHS END

            PdfContentByte cb = writer.DirectContent;
            cb.BeginText();
            //ACTUAL CHECK CONTENTBYTE START  
            cb.SetFontAndSize(f_cn_bold, 10);
            cb.SetTextMatrix(ConvertToInchX(4f), ConvertToInchY(3.52f + ytoadd));
            cb.ShowText(bankname);
            cb.SetFontAndSize(f_cn_bold, 8);
            cb.SetTextMatrix(ConvertToInchX(4.1f), ConvertToInchY(3.52f + ytoadd) - 8);
            cb.ShowText(accountnumber);
            cb.SetTextMatrix(ConvertToInchX(6.8f), ConvertToInchY(4.18f + ytoadd));
            cb.ShowText(digits);
            cb.SetFontAndSize(f_cn_bold, 10);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(5.45f + ytoadd));
            cb.ShowText(sig1);
            cb.SetFontAndSize(f_cn, 7);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(5.45f + ytoadd) - 8);
            cb.ShowText(sig2);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(5.45f + ytoadd) - 16);
            cb.ShowText(sig3);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(5.45f + ytoadd) - 24);
            cb.ShowText(sig4);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(5.45f + ytoadd) - 32);
            cb.ShowText(sig5);
            //ACTUAL CHECK CONTENTBYTE END
            //ACTUAL CHECK CONTENTBYTE START 2 
            cb.SetFontAndSize(f_cn_bold, 10);
            cb.SetTextMatrix(ConvertToInchX(4f), ConvertToInchY(3.52f + ytoadd2));
            cb.ShowText(bankname);
            cb.SetFontAndSize(f_cn_bold, 8);
            cb.SetTextMatrix(ConvertToInchX(4.1f), ConvertToInchY(3.52f + ytoadd2) - 8);
            cb.ShowText(accountnumber);
            cb.SetTextMatrix(ConvertToInchX(6.8f), ConvertToInchY(4.18f + ytoadd2));
            cb.ShowText(digits);
            cb.SetFontAndSize(f_cn_bold, 10);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(5.45f + ytoadd2));
            cb.ShowText(sig1);
            cb.SetFontAndSize(f_cn, 7);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(5.45f + ytoadd2) - 8);
            cb.ShowText(sig2);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(5.45f + ytoadd2) - 16);
            cb.ShowText(sig3);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(5.45f + ytoadd2) - 24);
            cb.ShowText(sig4);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(5.45f + ytoadd2) - 32);
            cb.ShowText(sig5);
            //ACTUAL CHECK CONTENTBYTE END 2
            //ACTUAL CHECK CONTENTBYTE START 3
            cb.SetFontAndSize(f_cn_bold, 10);
            cb.SetTextMatrix(ConvertToInchX(4f), ConvertToInchY(3.52f + ytoadd3));
            cb.ShowText(bankname);
            cb.SetFontAndSize(f_cn_bold, 8);
            cb.SetTextMatrix(ConvertToInchX(4.1f), ConvertToInchY(3.52f + ytoadd3) - 8);
            cb.ShowText(accountnumber);
            cb.SetTextMatrix(ConvertToInchX(6.8f), ConvertToInchY(4.18f + ytoadd3));
            cb.ShowText(digits);
            cb.SetFontAndSize(f_cn_bold, 10);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(5.45f + ytoadd3));
            cb.ShowText(sig1);
            cb.SetFontAndSize(f_cn, 7);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(5.45f + ytoadd3) - 8);
            cb.ShowText(sig2);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(5.45f + ytoadd3) - 16);
            cb.ShowText(sig3);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(5.45f + ytoadd3) - 24);
            cb.ShowText(sig4);
            cb.SetTextMatrix(ConvertToInchX(5f), ConvertToInchY(5.45f + ytoadd3) - 32);
            cb.ShowText(sig5);
            //ACTUAL CHECK CONTENTBYTE END 2
            cb.EndText();
            //Amount
            string a = "AMOUNT";
            Chunk c = new Chunk(amount2);
            float s = c.GetWidthPoint() + 6;
            PdfPTable table = new PdfPTable(1)
            {
                TotalWidth = s
            };
            PdfPCell cell = new PdfPCell(new Phrase(a, new Font(f_cn_8)))
            {
                HorizontalAlignment = 1,
                BackgroundColor = new BaseColor(0, 0, 0)
            };
            PdfPCell cell2 = new PdfPCell(new Phrase(amount2))
            {
                HorizontalAlignment = 1
            };
            table.AddCell(cell);
            table.AddCell(cell2);
            table.WriteSelectedRows(0, -1, ConvertToInch(6.8f), (ConvertToInchY(1.5f) + table.TotalHeight), cb);
            table.WriteSelectedRows(0, -1, ConvertToInch(6.8f), (ConvertToInchY(5.05f) + table.TotalHeight), cb);
            table.WriteSelectedRows(0, -1, ConvertToInch(6.8f), (ConvertToInchY(8.58f) + table.TotalHeight), cb);
            //Amount

            //Date
            string adate = "DATE";
            Chunk cdate = new Chunk(date);
            float sdate = cdate.GetWidthPoint() + 6;
            PdfPTable tabledate = new PdfPTable(1)
            {
                TotalWidth = sdate
            };
            PdfPCell celldate = new PdfPCell(new Phrase(adate, new Font(f_cn_8)))
            {
                HorizontalAlignment = 1,
                BackgroundColor = new BaseColor(0, 0, 0)
            };
            PdfPCell celldate2 = new PdfPCell(new Phrase(date))
            {
                HorizontalAlignment = 1
            };
            tabledate.AddCell(celldate);
            tabledate.AddCell(celldate2);
            tabledate.WriteSelectedRows(0, -1, ConvertToInch(4.0f), (ConvertToInchY(0.85f) + tabledate.TotalHeight), cb);
            tabledate.WriteSelectedRows(0, -1, ConvertToInch(4.0f), (ConvertToInchY(4.4f) + tabledate.TotalHeight), cb);
            tabledate.WriteSelectedRows(0, -1, ConvertToInch(4.0f), (ConvertToInchY(7.90f) + tabledate.TotalHeight), cb);
            //Date

            //ACTUAL CHECK START  
            document.Add(blanklineP);
            document.Add(printed2P);
            document.Add(payernamePactual);
            document.Add(payeraddressPactual);
            document.Add(blanklinePbig);
            document.Add(amountwordsP);
            document.Add(blanklineP);
            document.Add(payeePactual);
            document.Add(blanklineP);
            document.Add(memoPactual);
            document.Add(blanklinePbig3);
            document.Add(routingP);
            //ACTUAL CHECK END 
            //ACTUAL CHECK START 2
            document.Add(blanklinePbig2);
            document.Add(printed2P);
            document.Add(payernamePactual);
            document.Add(payeraddressPactual);
            document.Add(blanklinePbig);
            document.Add(amountwordsP);
            document.Add(blanklineP);
            document.Add(payeePactual);
            document.Add(blanklineP);
            document.Add(memoPactual);
            document.Add(blanklinePbig3);
            document.Add(routingP);
            //ACTUAL CHECK END 2 
            //ACTUAL CHECK START 3
            document.Add(blanklinePbig2);
            document.Add(printed2P);
            document.Add(payernamePactual);
            document.Add(payeraddressPactual);
            document.Add(blanklinePbig);
            document.Add(amountwordsP);
            document.Add(blanklineP);
            document.Add(payeePactual);
            document.Add(blanklineP);
            document.Add(memoPactual);
            document.Add(blanklinePbig3);
            document.Add(routingP);
            //ACTUAL CHECK END 3 
            document.Close();
            return workStream;
        }
        private MemoryStream PdfBatch(CheckInfoViewModel ci)
        {
            string cnumber = ci.CheckNumber;
            string pname = ci.PayerName;
            string pyname = ci.PayerName;
            string paddress = FormatAddressPhone(ci.PayerAddress, ci.PayerPhone);
            string pphone = ci.PayerPhone;
            string bname = ci.BankName;
            string rnumber = ci.RoutingNumber;
            string anumber = ci.AccountNumber;
            //string ppayee = ci.Payee;
            string ppayee = FormatAddressPhone(ci.Payee, ci.PayeePhone);
            string pdate = ci.Date;
            string pamount = ci.Amount;
            string cmemo = ci.Memo;
            string[] bankadd = ci.BankAddress?.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            string bankaddress = bankadd != null ? bankadd[0].ToString() : "";
            string bankcitystate = bankadd != null ? bankadd[1].ToString() : "";
            string payeename = ci.PayeeName;
            //string payeeaddress = ci.PayeeAddress;
            string payeeaddress = FormatAddressPhone(ci.PayeeAddress, ci.PayeePhone);
            int batchcount = ci.BatchCount;
            pamount = float.Parse(pamount).ToString("N", new CultureInfo("en-US"));
            string pamountwords = ci.AmountInWords;

            MemoryStream workStream = new MemoryStream();
            float ytoadd = -3.3f;
            float checkhor = ci.Test == "test" ? ci.CheckHor : GetCheckHor();
            float micrhor = ci.Test == "test" ? ci.MICRHor : GetMICRHor();
            float micrver = ci.Test == "test" ? ci.MICRVer : GetMICRVer();

            string path = Server.MapPath("~");
            Document document = new Document(PageSize.LETTER, 0f, 0f, 0f, 0f);
            PdfWriter writer = PdfWriter.GetInstance(document, workStream);
            writer.CloseStream = false;

            document.Open();
            //CONFIRMATION OF CHECK VARIABLES START
            BaseFont f_cn = BaseFont.CreateFont("c:\\windows\\fonts\\arial.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BaseFont f_cn_bold = BaseFont.CreateFont(path + "\\fonts\\ARIALBD.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            Font f_cn_bold_10 = new Font(f_cn_bold, 10);
            Font f_cn_10 = new Font(f_cn, 10);
            Font f_cn_bold_10_times = new Font(bfTimes, 10, Font.BOLD);
            Font f_cn_bold_12_times = new Font(bfTimes, 12, Font.BOLDITALIC);
            Font smallfontnormal = FontFactory.GetFont("Arial", 8, Font.NORMAL);
            Font fontnormalbold10 = FontFactory.GetFont("Arial", 10, Font.BOLD);
            Font f_cn_arial_7 = new Font(f_cn, 7);
            Font f_cn_arial_6 = new Font(f_cn, 6);
            Font f_cn_arial_6_bold = new Font(f_cn, 6, Font.BOLD);
            Font f_cn_bold_8 = new Font(f_cn_bold, 8);
            Font f_cn_8 = new Font(f_cn, 8, Font.NORMAL, BaseColor.WHITE);
            Font f_cn_9_bolditalic = new Font(f_cn, 9, Font.BOLDITALIC, BaseColor.WHITE);


            BaseFont f_micr = BaseFont.CreateFont(path + "\\fonts\\MCCH____.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            Font micrf = new Font(f_micr, 12, Font.BOLD);
            Font smallfont = FontFactory.GetFont("Arial", 8);
            Font bigfont10 = FontFactory.GetFont("Arial", 10);
            Font bigfont14 = FontFactory.GetFont("Arial", 14);
            Font bigfont = FontFactory.GetFont("Arial", 12);
            Font bigfontmcch = FontFactory.GetFont("MICR", 12);
            Chunk glue = new Chunk(new VerticalPositionMark());

            string blankline = "\n";
            string drawn = "Drawn to Pay To";
            string payee = pyname + Environment.NewLine + ppayee;
            string confirmationdate = "Date: " + pdate + " #: " + cnumber;
            string confirmationamount = "Amount: $" + pamount;
            string phone = @"Phone #: " + pphone;
            string memo = cmemo + Environment.NewLine +
@"As you instructed a check has been prepared for " + pamount;
            string payfrom = "Pay from the account of:";
            string payername = pname;
            string payeraddress = paddress;
            string bankname = bname;
            string accountnumber = anumber;
            string micr = "A" + rnumber + "A";
            string printed2 = "Printed using CHAX® check software - 2687708";
            //CONFIRMATION OF CHECK VARIABLES END

            //ACTUAL VARIABLES START
            string amountwords = pamountwords.Replace(" Dollars", "").Replace(" Cents", "");
            string tracking = "Tracking Code:  MKGT-MKG5-3456";
            string notvalid = "*** Not valid for use as a money order or cashier's check ***";
            string memoactual = "MEMO         " + cmemo;
            Chunk routing = new Chunk(TruncateAccountNumber(rnumber, anumber), bigfont);
            Chunk routing2 = new Chunk(anumber.Substring(anumber.Length - 4), micrf);
            string digits = cnumber;
            string date = pdate;
            string amount2 = "$" + pamount;
            string sig1 = "SIGNATURE NOT REQUIRED";
            string sig2 = "Your depositor has authorized this payment to payee.";
            string sig3 = "Payee to hold you harmless for payment of this document.";
            string sig4 = "This document shall be deposited only to credit of payee.";
            string sig5 = "Absence of endorsement is guaranteed by payee.";
            string asterisks = "******************************";
            string dollars = " Dollars";
            string tothe = "TO THE";
            string order = "ORDER";
            string of = "OF";
            string authorized = "AUTHORIZED SIGNATURE";
            string routingP = "A" + rnumber + "A " + anumber + "C";
            string cnumberP = "C" + cnumber + " ";

            //ACTUAL VARIABLES END

            //FILE COPY VARIABLES START
            string routingfilecopy = micr + " " + anumber + "C";
            //FILE COPY VARIABLES END

            //CONFIRMATION OF CHECK PARAGRAPHS START
            Paragraph blanklineP = new Paragraph(12, blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };
            Paragraph blanklineP2 = new Paragraph(10, blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph blanklinePbig = new Paragraph(ConvertToInch(.45f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph blanklinePbig2 = new Paragraph(ConvertToInch(.38f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph blanklinePbig3 = new Paragraph(ConvertToInch(.26f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph blanklinePbig4 = new Paragraph(ConvertToInch(.40f), blankline, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph drawnP = new Paragraph(9, drawn, smallfont)
            {
                IndentationLeft = ConvertToInch(.28f)
            };

            Paragraph payeeP = new Paragraph(9, payee, smallfont)
            {
                IndentationLeft = ConvertToInch(.28f)
            };

            Paragraph phoneP = new Paragraph(9, phone, smallfont)
            {
                IndentationLeft = ConvertToInch(.32f)
            };

            Paragraph memoP = new Paragraph(9, memo, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph payfromP = new Paragraph(9, payfrom, f_cn_bold_8)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph payernameP = new Paragraph(9, payername, f_cn_bold_8)
            {
                IndentationLeft = ConvertToInch(1f)
            };

            Paragraph payeraddressP = new Paragraph(9, payeraddress, smallfont)
            {
                IndentationLeft = ConvertToInch(1f)
            };

            Paragraph mcchP = new Paragraph(9, micr, micrf)
            {
                IndentationLeft = ConvertToInch(.12f),
            };

            Paragraph linep = new Paragraph(5,
                new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 5.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1))
                )
            {
                IndentationLeft = ConvertToInch(.12f)
            };
            //CONFIRMATION OF CHECK PARAGRAPHS END

            //ACTUAL CHECK PARAGRAPHS START
            Paragraph printed2P = new Paragraph(0, printed2, smallfont)
            {
                IndentationLeft = ConvertToInch(0f)
            };

            Paragraph payernamePactual = new Paragraph(11, payername, f_cn_bold_10_times)
            {
                IndentationLeft = ConvertToInch(1f + checkhor)
            };

            Paragraph payeraddressPactual = new Paragraph(8, payeraddress, f_cn_arial_7)
            {
                IndentationLeft = ConvertToInch(1f + checkhor)
            };

            Paragraph trackingCodePactual = new Paragraph(12, tracking, f_cn_arial_7)
            {
                IndentationLeft = ConvertToInch(.5f + checkhor)
            };

            Paragraph notValidPactual = new Paragraph(8, notvalid, f_cn_arial_7)
            {
                IndentationLeft = ConvertToInch(.5f + checkhor)
            };

            Paragraph amountwordsP = new Paragraph(9, "PAY          ", bigfont10)
            {
                IndentationLeft = ConvertToInch(.25f + checkhor)
            };
            amountwordsP.Add(new Chunk(amountwords, f_cn_bold_12_times));

            Paragraph payeePactual = new Paragraph(9, payeename + "\n", fontnormalbold10)
            {
                IndentationLeft = ConvertToInch(.89f + checkhor)
            };

            Paragraph payeeaddressPactual = new Paragraph(9, payeeaddress, smallfontnormal)
            {
                IndentationLeft = ConvertToInch(.89f + checkhor)
            };


            Paragraph phonePactual = new Paragraph(9, phone, smallfont)
            {
                IndentationLeft = ConvertToInch(.78f)
            };

            Paragraph memoPactual = new Paragraph(9, memoactual, smallfont)
            {
                IndentationLeft = ConvertToInch(.25f + checkhor)
            };

            //Paragraph routingP = new Paragraph(9, ("A" + rnumber + "A" + anumber + "C" + cnumber), micrf)
            //{
            //    Alignment = Element.ALIGN_CENTER
            //};

            Paragraph routingP2 = new Paragraph(9, "", micrf)
            {
                Alignment = Element.ALIGN_CENTER
            };

            Chunk signatureLinep = new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 5.0F, BaseColor.BLACK, Element.ALIGN_RIGHT, 1));

            //ACTUAL CHECK PARAGRAPHS END
            //FILE COPY PARAGRAPHS START
            Paragraph phonePfileCopy = new Paragraph(15, phone, bigfont10)
            {
                IndentationLeft = ConvertToInch(.78f)
            };

            Paragraph routingPfilecopy = new Paragraph(12, routingfilecopy, micrf)
            {
                IndentationLeft = ConvertToInch(.12f),
            };
            //FILE COPY PARAGRAPHS END
            int counter = 0;
            float tbly = 0;
            float liney = 0;
            float clearbandy = 3.5f;
            for (int i = 0; i < batchcount; i++)
            {
                if (counter == 1)
                {
                    document.Add(blanklinePbig4);
                    ytoadd = 0.25f;
                    tbly = 3.5f;
                    liney = 3.55f;
                    clearbandy = 7f;
                }
                else if (counter == 2)
                {
                    ytoadd = 0.25f + 3.5f;
                    tbly = 3.5f + 3.5f;
                    liney = 3.55f + 3.5f;
                    clearbandy = 10.5f;
                    document.Add(blanklinePbig4);
                }
                PdfContentByte cb = writer.DirectContent;
                cb.BeginText();
                //ACTUAL CHECK CONTENTBYTE START  
                cb.SetFontAndSize(f_cn_bold, 7);
                cb.SetTextMatrix(ConvertToInchX(4f + checkhor), ConvertToInchY(3.60f + ytoadd));
                cb.ShowText(bankname);
                cb.SetFontAndSize(f_cn, 6);
                cb.SetTextMatrix(ConvertToInchX(4f + checkhor), ConvertToInchY(3.60f + ytoadd) - 7);
                cb.ShowText(bankaddress);
                cb.SetTextMatrix(ConvertToInchX(4f + checkhor), ConvertToInchY(3.60f + ytoadd) - 13);
                cb.ShowText(bankcitystate);
                cb.SetFontAndSize(f_cn_bold, 18);
                cb.ShowTextAligned(Element.ALIGN_RIGHT, digits, ConvertToInchX(8f + checkhor), ConvertToInchY(3.8f + ytoadd), 0);
                cb.SetFontAndSize(f_cn_bold, 14);
                cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(4.76f + ytoadd));
                cb.ShowText(asterisks);
                cb.SetFontAndSize(f_cn_bold, 11);
                cb.SetTextMatrix(ConvertToInchX(7.4f + checkhor), ConvertToInchY(4.7f + ytoadd));
                cb.ShowText(dollars);
                cb.SetFontAndSize(f_cn, 7);

                cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.23f + ytoadd));
                cb.ShowText(sig1);
                cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.23f + ytoadd) - 8);
                cb.ShowText(sig2);
                cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.23f + ytoadd) - 16);
                cb.ShowText(sig3);
                cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.23f + ytoadd) - 24);
                cb.ShowText(sig4);
                cb.SetTextMatrix(ConvertToInchX(5f + checkhor), ConvertToInchY(5.23f + ytoadd) - 32);
                cb.ShowText(sig5);
                cb.SetFontAndSize(f_cn, 10);
                cb.SetTextMatrix(ConvertToInchX(.25f + checkhor), ConvertToInchY(5.13f + ytoadd));
                cb.ShowText(tothe);
                cb.SetTextMatrix(ConvertToInchX(.25f + checkhor), ConvertToInchY(5.13f + ytoadd) - 10);
                cb.ShowText(order);
                cb.SetTextMatrix(ConvertToInchX(.25f + checkhor), ConvertToInchY(5.13f + ytoadd) - 20);
                cb.ShowText(of);
                cb.SetFontAndSize(f_cn_bold, 10);
                cb.SetTextMatrix(ConvertToInchX(5.75f + checkhor), ConvertToInchY(5.89f + ytoadd));
                cb.ShowText(pyname);
                cb.SetFontAndSize(f_cn, 7);
                cb.SetTextMatrix(ConvertToInchX(5.75f + checkhor), ConvertToInchY(6.08f + ytoadd));
                cb.ShowText(authorized);

                //ACTUAL CHECK CONTENTBYTE END

                cb.EndText();

                cb.MoveTo(ConvertToInchX(7.8f + checkhor), ConvertToInchY(2.63f + liney));
                cb.LineTo(ConvertToInchX(5.25f + checkhor), ConvertToInchY(2.63f + liney));
                cb.Stroke();

                //Table
                string lDate = "Date";
                string lCheckNumber = "Check No.";
                string lAmount = "Amount";
                PdfPTable table = new PdfPTable(3)
                {
                    TotalWidth = 290
                };

                PdfPCell cellDateLabel = new PdfPCell(new Phrase(lDate, new Font(bfTimes, 8)))
                {
                    BackgroundColor = new BaseColor(192, 192, 192),
                    HorizontalAlignment = 1
                };
                PdfPCell cellCheckNoLabel = new PdfPCell(new Phrase(lCheckNumber, new Font(bfTimes, 8)))
                {
                    BackgroundColor = new BaseColor(192, 192, 192),
                    HorizontalAlignment = 1
                };
                PdfPCell cellAmountLabel = new PdfPCell(new Phrase(lAmount, new Font(bfTimes, 8)))
                {
                    BackgroundColor = new BaseColor(192, 192, 192),
                    HorizontalAlignment = 1
                };
                PdfPCell cellDate = new PdfPCell(new Phrase(date, new Font(f_cn, 10)))
                {
                    HorizontalAlignment = 2
                };
                PdfPCell cellCheckNo = new PdfPCell(new Phrase(cnumber, new Font(f_cn, 10)))
                {
                    HorizontalAlignment = 2
                };
                PdfPCell cellAmount = new PdfPCell(new Phrase(amount2, new Font(f_cn, 10)))
                {
                    HorizontalAlignment = 2
                };

                table.AddCell(cellDateLabel);
                table.AddCell(cellCheckNoLabel);
                table.AddCell(cellAmountLabel);
                table.AddCell(cellDate);
                table.AddCell(cellCheckNo);
                table.AddCell(cellAmount);
                table.WriteSelectedRows(0, -1, ConvertToInch(4.0f + checkhor), (ConvertToInchY(1.1f + tbly) + table.TotalHeight), cb);
                //Table

                //Clear Band
                float clearBandXPos = ConvertToInchX(0f);
                float clearBandYPos = ConvertToInchY(clearbandy);
                float clearBandWidth = ConvertToInch(8.5f);
                float clearBandHeight = ConvertToInch(0.625f);
                //cb.SetLineWidth(1);
                //cb.Rectangle(clearBandXPos, clearBandYPos, clearBandWidth, clearBandHeight);
                //cb.Stroke();
                //Clear Band
                //MICR Distance From Bottom
                float distanceXPos = ConvertToInchX(0f);
                float distanceYPos = ConvertToInchY(clearbandy);
                float distanceWidth = ConvertToInch(8.5f);
                float distanceHeight = ConvertToInch(0.150f);
                //cb.SetLineWidth(1);
                //cb.Rectangle(distanceXPos, distanceYPos, distanceWidth, distanceHeight);
                //cb.Stroke();
                //MICR Distance From Bottom
                //MICR Box
                float MICRXPos = ConvertToInchX(8.5f - 5.6875f);
                float MICRYPos = ConvertToInchY(clearbandy) + distanceHeight;
                float MICRWidth = ConvertToInch(5.6875f - 0.3125f);
                float MICRHeight = ConvertToInch(0.4375f);
                //cb.SetLineWidth(1);
                //cb.Rectangle(MICRXPos, MICRYPos, MICRWidth, MICRHeight);
                //cb.Stroke();
                //MICR Box
                //MICR 
                cb.BeginText();
                cb.SetFontAndSize(f_micr, 15);
                cb.ShowTextAligned(Element.ALIGN_LEFT, routingP, MICRXPos + ConvertToInch(micrhor), MICRYPos - ConvertToInch(micrver), 0);
                cb.ShowTextAligned(Element.ALIGN_RIGHT, cnumberP, MICRXPos + ConvertToInch(micrhor), MICRYPos - ConvertToInch(micrver), 0);
                cb.EndText();
                //MICR 

                //ACTUAL CHECK START  
                document.Add(blanklineP); //here
                document.Add(printed2P);
                document.Add(payernamePactual);
                document.Add(payeraddressPactual);
                document.Add(trackingCodePactual);
                document.Add(notValidPactual);
                document.Add(blanklineP);
                document.Add(amountwordsP);
                document.Add(blanklinePbig3);
                document.Add(payeePactual);
                document.Add(payeeaddressPactual);
                document.Add(blanklineP);
                document.Add(memoPactual);
                document.Add(blanklinePbig2);
                document.Add(routingP2);
                //ACTUAL CHECK END 

                if (counter == 2)
                {
                    counter = -1;
                    ytoadd = -3.3f;
                    tbly = 0f;
                    liney = 0f;
                    clearbandy = 3.5f;
                    document.NewPage();
                }
                if (counter < 2)
                {
                    counter++;
                }
            }

            document.Close();


            return workStream;
        }
        private float ConvertToInchX(float x)
        {
            return x = x * 72;
        }
        private float ConvertToInchY(float x)
        {
            return x = (11.0f - x) * 72;
        }
        private float ConvertToInch(float x)
        {
            return x = x * 72;
        }
        private string UpdatePrintedChecks(CheckInfoViewModel ci)
        {
            string r = "1";
            int id = ci.ID;
            string cnumber = ci.CheckNumber;
            string pname = ci.PayerName;
            string paddress = ci.PayerAddress;
            string pphone = ci.PayerPhone;
            string pyphone = ci.PayeePhone;
            string bname = ci.BankName;
            string rnumber = ci.RoutingNumber;
            string anumber = ci.AccountNumber;
            string ppayee = ci.Payee;
            string pdate = ci.Date;
            string pamount = ci.Amount;
            string cmemo = ci.Memo;
            string pamountwords = ci.AmountInWords;
            string pyname = ci.PayeeName;
            string bankaddress = ci.BankAddress;
            string bankcitystate = ci.BankCityStateZip;
            string checklayout = ci.CheckLayout;
            int batchCount = ci.BatchCount;
            var email = User.Identity.GetUserName();
            var payeremail = ci.PayerEmail;



                PrintedCheck p;
            using (var db = new CHAX_Context())
            {
                p = db.PrintedCheck.Where(x => x.ID == id).FirstOrDefault<PrintedCheck>();
            }
            if (p != null)
            {
                p.CheckNumber = cnumber;
                p.PayerName = pname;
                p.PayerAddress = paddress;
                p.PayerPhone = pphone;
                p.BankName = bname;
                p.RoutingNumber = rnumber;
                p.AccountNumber = anumber;
                p.Payee = ppayee;
                p.Date = DateTime.Parse(pdate);
                p.Amount = pamount;
                p.Memo = cmemo;
                p.AmountInWords = pamountwords;
                p.PayeeName = pyname;
                p.BankAddress = bankaddress;
                p.BankCityState = bankcitystate;
                p.CheckLayout = GetLayout(checklayout);
                p.BatchCount = batchCount;
                p.UserEmail = email;
                p.PayerEmail = payeremail;
                p.PayeePhone = pyphone;
                using (var db = new CHAX_Context())
                {
                    try
                    {
                        db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        r = ex.Message.ToString();
                    }
                }
            }
            else
            {
                r = AddPrintedCheck(ci);
            }

            return r;
        }
        private string GetLayout(string layout)
        {
            string xstr = "";
            using (var db = new CHAX_Context())
            {
                xstr = db.CheckLayout.Where(x => x.LayoutDescription == layout).FirstOrDefault().LayoutCode.ToString();
            }
            return xstr;
        }
        private string AddPrintedCheck(CheckInfoViewModel ci)
        {
            string r = "2";
            string cnumber = ci.CheckNumber;
            string pname = ci.PayerName;
            string paddress = ci.PayerAddress;
            string pphone = ci.PayerPhone;
            string pyphone = ci.PayeePhone;
            string bname = ci.BankName;
            string rnumber = ci.RoutingNumber;
            string anumber = ci.AccountNumber;
            string ppayee = ci.Payee;
            string pdate = ci.Date;
            string pamount = ci.Amount;
            string cmemo = ci.Memo;
            string pamountwords = ci.AmountInWords;
            string payeename = ci.PayeeName;
            string bankaddress = ci.BankAddress;
            string bankcitystate = ci.BankCityStateZip;
            string checklayout = ci.CheckLayout;
            int batchcount = ci.BatchCount;
            string payeremail = ci.PayerEmail;
            DateTime datecreated = DateTime.Parse(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff"));
            var email = User.Identity.GetUserName();
            var username = email;

            using (var db3 = new ApplicationDbContext())
            {
                var pp = db3.Users.Where(x => x.UserName == email).FirstOrDefault();
                if (pp.UserLevel == 1)
                {
                    username = pp.ParentUserName;
                }
            }

            var p = new PrintedCheck
            {
                CheckNumber = cnumber,
                PayerName = pname,
                PayerAddress = paddress,
                PayerPhone = pphone,
                BankName = bname,
                RoutingNumber = rnumber,
                AccountNumber = anumber,
                Payee = ppayee,
                Date = DateTime.Parse(pdate),
                Amount = pamount,
                Memo = cmemo,
                AmountInWords = pamountwords,
                BankAddress = bankaddress,
                BankCityState = bankcitystate,
                PayeeName = payeename,
                CheckLayout = GetLayout(checklayout),
                BatchCount = batchcount,
                UserEmail = username,
                DateCreated = datecreated,
                PayerEmail = payeremail,
                PayeePhone = pyphone
            };

            using (var db = new CHAX_Context())
            {
                try
                {
                    db.PrintedCheck.Add(p);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    r = ex.Message.ToString();
                }
            }
            return r;
        }


        public JsonResult DeletePdfFile(string file)
        {
            string p = Server.MapPath("~/Files/") + file;
            string r = "0";
            System.IO.File.Delete(p);
            if (System.IO.File.Exists(p))
            {
                r = "Cannot delete the file";
            }
            return Json(r);
        }
        public JsonResult PreviewCheck(string cnumber, string accnum)
        {
            var email = User.Identity.GetUserName();
            List<PrintedCheck> p = new List<PrintedCheck>();
            //using (var db = new CHAX_Context())
            //{
            //    //DateTime date = Convert.ToDateTime(DateTime.Now.Date.ToShortDateString()).Date;
            //    p = db.PrintedCheck.Where(x => x.CheckNumber == cnumber && x.UserEmail == email && x.AccountNumber == accnum).ToList();
            //}

            using (var db = new ApplicationDbContext())
            {
                var pp = db.Users.Where(x => x.UserName == email).FirstOrDefault();
                if (pp.UserLevel == 1)
                {
                    using (var db2 = new CHAX_Context())
                    {
                        p = db2.PrintedCheck.Where(x => x.CheckNumber == cnumber && (x.UserEmail == email || x.UserEmail == pp.ParentUserName) && x.AccountNumber == accnum).ToList();
                    }
                }
                else
                {
                    using (var db3 = new CHAX_Context())
                    {
                        p = db3.PrintedCheck.Where(x => x.CheckNumber == cnumber && x.UserEmail == email && x.AccountNumber == accnum).ToList();
                    }
                }
            }


            return Json(p);
        }

        public JsonResult PreviewCheckTest(string cnumber, string accnum)
        {
            var email = User.Identity.GetUserName();
            List<PrintedCheck> p = new List<PrintedCheck>();
            using (var db = new CHAX_Context())
            {
                //DateTime date = Convert.ToDateTime(DateTime.Now.Date.ToShortDateString()).Date;
                p = db.PrintedCheck.Where(x => x.CheckNumber == "999999" && x.UserEmail == "vennymartinez.dev@gmail.com" && x.AccountNumber == "888888").ToList();
            }

            return Json(p);
        }


        private bool CheckIfExisting(string checknumber, string account)
        {
            List<PrintedCheck> p = new List<PrintedCheck>();
            int intchecknumber = int.Parse(checknumber);
            bool xstr = false;
            var email = User.Identity.GetUserName();
            using (var db = new CHAX_Context())
            {
                p = db.PrintedCheck.Where(x => x.CheckNumber == intchecknumber.ToString() && x.AccountNumber == account && x.UserEmail == email && x.IsDeleted == false).ToList();
                if (p.Count > 0)
                {
                    xstr = true;
                }
            }
            return xstr;
        }


        public JsonResult GetMessage()
        {
            var p1 = new FormMessage();
            var email = User.Identity.GetUserName();
            string r = "";
            using (var dd = new CHAX_Context())
            {
                //p1 = dd.FormMessage.Where(x => x.UserName == email).FirstOrDefault();
                using (var db2 = new ApplicationDbContext())
                {
                    var p2 = db2.Users.Where(x => x.UserName == email).FirstOrDefault();
                    if (p2.UserLevel == 1)
                    {
                        p1 = dd.FormMessage.Where(x => x.UserName == p2.ParentUserName).FirstOrDefault();
                    }
                    else
                    {
                        p1 = dd.FormMessage.Where(x => x.UserName == email).FirstOrDefault();
                    }
                }
                if (p1 != null)
                {
                    r = p1.Message;
                }
                else
                {
                    r = "No Message!";
                }
            }

            return Json(r);
        }
    }
}