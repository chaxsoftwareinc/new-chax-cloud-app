﻿using Ghostscript.NET;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static CHAXWebApp.Classes.CHAX;

namespace CHAXWebApp.Controllers
{
    public class PreviewCheckController : Controller
    {
        // GET: PreviewCheck
        public ActionResult PreviewCheck(string path)
        {
            ViewBag.ImageFile = "~/Files/" + path;
            Preview(path);
            return PartialView();
        }


        [DeleteFileAttribute]
        public void Preview(string file)
        {

            //get the temp folder and file path in server
            string fullPath = Path.Combine(Server.MapPath("~/Files"), file);
            ConvertToImage(file);
            //return the file for download, this is an Excel 
            //so I set the file content type to "application/vnd.ms-excel"

            //return Json(fullPath);
             
        }

        public void ConvertToImage(string flupload)
        {
            String PdfFolderPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Files/" + flupload);
            Pdf2ImageConversion(flupload, PdfFolderPath);
        }

        private void Pdf2ImageConversion(string FileName, string PdfFolderPath)
        {
            String FileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(FileName);
            String ImgFolderPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Files/"
                + FileNameWithoutExtension + ".jpeg");
            var info = new System.IO.FileInfo(ImgFolderPath);
            if (info.Exists.Equals(false))
            {
                GhostscriptPngDevice img = new GhostscriptPngDevice(GhostscriptPngDeviceType.Png16m)
                {
                    GraphicsAlphaBits = GhostscriptImageDeviceAlphaBits.V_4,
                    TextAlphaBits = GhostscriptImageDeviceAlphaBits.V_4,
                    ResolutionXY = new GhostscriptImageDeviceResolution(200, 200)
                };
                img.InputFiles.Add(PdfFolderPath);
                img.Pdf.FirstPage = 1;
                img.Pdf.LastPage = 1;
                img.PostScript = string.Empty;
                img.OutputPath = ImgFolderPath;
                img.Process();
            }
        }
    }
}