﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CHAXWebApp.Controllers
{
    public class TransmitACHController : Controller
    {
        // GET: TransmitACH
        [Authorize]
        [RequireHttps]
        public ActionResult TransmitACH()
        {
            return View();
        }
    }
}