﻿using CHAXWebApp.Models.DBContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CHAXWebApp.Models.DBTables;
using Microsoft.AspNet.Identity;

namespace CHAXWebApp.Controllers
{
    public class PayeeController : Controller
    {
        // GET: Payee
        [Authorize]
        [RequireHttps]
        public ActionResult Payee()
        {
            return View();
        }

        public JsonResult GetAllPayee()
        {
            var email = User.Identity.GetUserName();
            using (var db = new CHAX_Context())
            {
                var payee = db.Payee.Where(x => x.UserEmail == email).OrderByDescending(x => x.LastDateUsed).ToList();
                return Json(payee, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetTopPayee()
        {
            var email = User.Identity.GetUserName();
            List<Payee> p = new List<Payee>();
            using (var db = new CHAX_Context())
            {
                p = db.Payee.Where(x => x.UserEmail == email).OrderByDescending(x => x.LastDateUsed).Take(20).ToList();
            }
            return Json(p);
        }

        public JsonResult GetSpecificPayee(int id)
        {
            var email = User.Identity.GetUserName();
            using (var db = new CHAX_Context())
            {
                var payee = db.Payee.Where(x => x.ID == id && x.UserEmail == email).FirstOrDefault();
                return Json(payee);
            }
        }
        private string ValidateNull(string str)
        {
            string xstr = str;
            if (str == null || str == "null")
            {
                xstr = "";
            }
            return xstr;
        }
        public JsonResult UpdatePayee(int id, string name, string line1, string line2, string city, string zip, string phone, string memo, string payeeid, string address)
        {
            var email = User.Identity.GetUserName();
            string r = "1";
            Payee p;
            using (var db = new CHAX_Context())
            {
                p = db.Payee.Where(x => x.ID == id && x.UserEmail == email).FirstOrDefault<Payee>();
            }
            if (p != null)
            {
                p.Name = name;
                p.AddressLine1 = ValidateNull(line1);
                p.AddressLine2 = ValidateNull(line2);
                p.CityState = ValidateNull(city);
                p.ZipCode = ValidateNull(zip);
                p.PhoneNumber = phone;
                p.Memo = memo;
                p.Address = address;
                using (var db = new CHAX_Context())
                {
                    try
                    {
                        db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        r = ex.Message.ToString();
                    }
                }
            }
            else
            {
                r = AddPayee(name, line1, line2, city, zip, phone, memo, address);
            }

            return Json(r);
        }

        public JsonResult UpdatePayeeViaCheck(int id, string name, string line1, string line2, string city, string zip, string phone, string memo, string address)
        {
            var email = User.Identity.GetUserName();
            string r = "1";
            Payee p;
            using (var db = new CHAX_Context())
            {
                p = db.Payee.Where(x => x.Name == name && x.ID == id && x.UserEmail == email).FirstOrDefault<Payee>();
            }
            if (p != null)
            {
                p.Name = name;
                p.AddressLine1 = ValidateNull(line1);
                p.AddressLine2 = ValidateNull(line2);
                p.CityState = ValidateNull(city);
                p.ZipCode = ValidateNull(zip);
                p.PhoneNumber = phone;
                p.Memo = memo;
                p.Address = address;
                using (var db = new CHAX_Context())
                {
                    try
                    {
                        db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        r = ex.Message.ToString();
                    }
                }
            }
            else
            {
                r = AddPayee(name, line1, line2, city, zip, phone, memo, address);
            }

            return Json(r);
        }

        public JsonResult DeletePayee(int id)
        {
            var email = User.Identity.GetUserName();
            string r = "3";
            using (var db = new CHAX_Context())
            {
                try
                {
                    var singleRec = db.Payee.SingleOrDefault(x => x.ID == id && x.UserEmail == email);// object your want to delete
                    db.Payee.Remove(singleRec);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    r = ex.Message.ToString();
                }
            }
            return Json(r);
        }

        private string AddPayee(string name, string line1, string line2, string city, string zip, string phone, string memo, string address)
        {
            var email = User.Identity.GetUserName();
            string r = "2";
            var p = new Payee
            {
                Name = name,
                AddressLine1 = ValidateNull(line1),
                AddressLine2 = ValidateNull(line2),
                CityState = ValidateNull(city),
                ZipCode = ValidateNull(zip),
                PhoneNumber = phone,
                LastDateUsed = DateTime.Now.Date,
                UserEmail = email,
                Memo = memo,
                Address = address
            };

            using (var db = new CHAX_Context())
            {
                try
                {
                    db.Payee.Add(p);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    r = ex.Message.ToString();
                }
            }
            return r;
        }
    }
}