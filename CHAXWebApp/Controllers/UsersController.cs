﻿using CHAXWebApp.Models;
using CHAXWebApp.Models.DBContexts;
using CHAXWebApp.Models.DBTables;
using CHAXWebApp.Models.ViewModels;
using Microsoft.AspNet.Identity;
using SendGrid;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CHAXWebApp.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        [Authorize]
        [RequireHttps]
        public ActionResult Users()
        {
            return View();
        }

        public JsonResult GetAllUsers()
        {
            var email = User.Identity.GetUserName();
            List<ApplicationUser> p = new List<ApplicationUser>();
            List<UsersOnAccountViewModel> pp = new List<UsersOnAccountViewModel>();
            using (var adb = new ApplicationDbContext())
            {

                p = adb.Users.Where(x => x.ParentUserName == email && x.UserLevel == 1).Take(20).ToList();
                if (p != null)
                {
                    foreach (var item in p)
                    {
                        UsersOnAccountViewModel p2 = new UsersOnAccountViewModel
                        {
                            UserFirstName = item.FirstName,
                            UserLastName = item.LastName,
                            UserName = item.UserName,
                            UserEmail = item.Email
                        };
                        pp.Add(p2);
                    }
                }
            }
            return Json(pp);
        }


        public JsonResult GetAllUsersAll()
        {
            var email = User.Identity.GetUserName();
            List<ApplicationUser> p = new List<ApplicationUser>();
            List<UsersOnAccountViewModel> pp = new List<UsersOnAccountViewModel>();
            using (var adb = new ApplicationDbContext())
            {
                
                p = adb.Users.Where(x => x.ParentUserName == email && x.UserLevel == 1).ToList();
                if (p != null) {
                    foreach (var item in p) {
                        UsersOnAccountViewModel p2 = new UsersOnAccountViewModel
                        {
                            UserFirstName = item.FirstName,
                            UserLastName = item.LastName,
                            UserName = item.UserName,
                            UserEmail = item.Email
                        };
                        pp.Add(p2);
                    }
                }
            }
            return Json(pp);
        }

        //public JsonResult GetAllUsers()
        //{
        //    var email = User.Identity.GetUserName();
        //    List<AccountRelation> p = new List<AccountRelation>();
        //    List<AccountRelation> p2 = new List<AccountRelation>();
        //    List<UsersOnAccountViewModel> pp = new List<UsersOnAccountViewModel>();
        //    using (var db = new CHAX_Context())
        //    {
        //        p = db.AccountRelation.Where(x => x.UserEmail == email).Take(20).ToList();
        //        p2 = db.AccountRelation.Where(x => x.InvitedEmail == email).Take(20).ToList();
        //        foreach (var item in p)
        //        {
        //            UsersOnAccountViewModel vm = new UsersOnAccountViewModel();
        //            using (var adb = new ApplicationDbContext())
        //            {
        //                var xx = adb.Users.Where(x => x.UserName == item.InvitedEmail).FirstOrDefault();
        //                vm.UserEmail = xx.UserName;
        //                vm.UserFirstName = xx.FirstName;
        //                vm.UserLastName = xx.LastName;
        //                vm.Status = item.Status;
        //                pp.Add(vm);
        //            }
        //        }
        //        foreach (var item in p2)
        //        {
        //            UsersOnAccountViewModel vm = new UsersOnAccountViewModel();
        //            using (var adb = new ApplicationDbContext())
        //            {
        //                var xx = adb.Users.Where(x => x.UserName == item.UserEmail).FirstOrDefault();
        //                vm.UserEmail = xx.UserName;
        //                vm.UserFirstName = xx.FirstName;
        //                vm.UserLastName = xx.LastName;
        //                vm.Status = item.Status;
        //                pp.Add(vm);
        //            }
        //        }
        //    }

        //    return Json(pp, JsonRequestBehavior.AllowGet);
        //}

        //public JsonResult GetAllUsersAll()
        //{
        //    var email = User.Identity.GetUserName();
        //    List<AccountRelation> p = new List<AccountRelation>();
        //    List<AccountRelation> p2 = new List<AccountRelation>();
        //    List<UsersOnAccountViewModel> pp = new List<UsersOnAccountViewModel>();
        //    using (var db = new CHAX_Context())
        //    {
        //        p = db.AccountRelation.Where(x => x.UserEmail == email).ToList();
        //        p2 = db.AccountRelation.Where(x => x.InvitedEmail == email).ToList();
        //        foreach (var item in p)
        //        {
        //            UsersOnAccountViewModel vm = new UsersOnAccountViewModel();
        //            using (var adb = new ApplicationDbContext())
        //            {
        //                var xx = adb.Users.Where(x => x.UserName == item.InvitedEmail).FirstOrDefault();
        //                vm.UserEmail = xx.UserName;
        //                vm.UserFirstName = xx.FirstName;
        //                vm.UserLastName = xx.LastName;
        //                vm.Status = item.Status;
        //                pp.Add(vm);
        //            }
        //        }
        //        foreach (var item in p2)
        //        {
        //            UsersOnAccountViewModel vm = new UsersOnAccountViewModel();
        //            using (var adb = new ApplicationDbContext())
        //            {
        //                var xx = adb.Users.Where(x => x.UserName == item.UserEmail).FirstOrDefault();
        //                vm.UserEmail = xx.UserName;
        //                vm.UserFirstName = xx.FirstName;
        //                vm.UserLastName = xx.LastName;
        //                vm.Status = item.Status;
        //                pp.Add(vm);
        //            }
        //        }
        //    }

        //    return Json(pp, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult GetCertainUser(string email)
        {
            UsersOnAccountViewModel p = new UsersOnAccountViewModel();
            using (var adb = new ApplicationDbContext())
            {
                var xx = adb.Users.Where(x => x.UserName == email).FirstOrDefault();
                if (xx != null)
                {
                    p.UserFirstName = xx.FirstName;
                    p.UserLastName = xx.LastName;
                    p.UserEmail = xx.Email;
                    p.Status = ConfirmRelation(email);
                }
                else {
                    p.UserFirstName = "null";
                    p.UserLastName = "null";
                    p.UserEmail = "null";
                    p.Status = 2;
                }
            }

            return Json(p, JsonRequestBehavior.AllowGet);
        }

        public int ConfirmRelation(string email2)
        {
            var xint = 2;
            var email1 = User.Identity.GetUserName();
            AccountRelation p = new AccountRelation();
            AccountRelation p2 = new AccountRelation();
            using (var db = new CHAX_Context())
            {
                p = db.AccountRelation.Where(x => x.UserEmail == email1 && x.InvitedEmail == email2).FirstOrDefault();
                p2 = db.AccountRelation.Where(x => x.InvitedEmail == email1 && x.UserEmail == email2).FirstOrDefault();
                if (p != null) {
                    xint = p.Status;
                } else if (p2 != null) {
                    xint = p2.Status;
                } else if (email1 == email2) {
                    xint = 3;
                }
            }

            return xint;
        }

        public JsonResult AddUsers(string invitedemail)
        {
            var p1 = new AccountRelation();
            var email = User.Identity.GetUserName();
            string r = "";
            using (var dd = new CHAX_Context())
            {
                p1 = dd.AccountRelation.Where(x => x.InvitedEmail == invitedemail && x.UserEmail == email).FirstOrDefault();
                if (p1 != null)
                {
                    r = "0";
                }
                else {                    
                    r = "1";
                    var p = new AccountRelation
                    {
                        UserEmail = email,
                        InvitedEmail = invitedemail,
                        Status = 0
                    };

                    using (var db = new CHAX_Context())
                    {
                        try
                        {
                            db.AccountRelation.Add(p);
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            r = ex.Message.ToString();
                        }
                    }
                }
            }

            return Json(r);
        }

        public JsonResult AcceptRequests(string useremail)
        {
            var invitedemail = User.Identity.GetUserName();
            string r = "1";
            AccountRelation p;
            using (var db = new CHAX_Context())
            {
                p = db.AccountRelation.Where(x => x.UserEmail == useremail && x.InvitedEmail == invitedemail).FirstOrDefault<AccountRelation>();
            }
            if (p != null)
            {
                p.Status = 1;

                using (var db = new CHAX_Context())
                {
                    try
                    {
                        db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        r = ex.Message.ToString();
                    }
                }
            }
            else
            {
                r = "Request not found!";
            }

            return Json(r);
        }

        public JsonResult GetAllRequests()
        {
            var email = User.Identity.GetUserName();
            List<AccountRelation> p = new List<AccountRelation>();
            List<UsersOnAccountViewModel> pp = new List<UsersOnAccountViewModel>();
            using (var db = new CHAX_Context())
            {
                p = db.AccountRelation.Where(x => x.InvitedEmail == email && x.Status == 0).ToList();
                foreach (var item in p)
                {
                    UsersOnAccountViewModel vm = new UsersOnAccountViewModel();
                    using (var adb = new ApplicationDbContext())
                    {
                        var xx = adb.Users.Where(x => x.UserName == item.UserEmail).FirstOrDefault();
                        vm.UserEmail = xx.UserName;
                        vm.UserFirstName = xx.FirstName;
                        vm.UserLastName = xx.LastName;
                        pp.Add(vm);
                    }
                }
            }
            return Json(pp, JsonRequestBehavior.AllowGet);
        }

        private string GetFullName()
        {
            string fullname = ""; 
            string email = User.Identity.GetUserName();
            ApplicationUser p;
            using (var db = new ApplicationDbContext())
            {
                p = db.Users.Where(x => x.UserName == email).FirstOrDefault();
            }
            if (p != null)
            {
                fullname = p.FirstName + " " + p.LastName;
            }

            return fullname;
        }


        public async Task<ActionResult> SendAccess(string emailto, string username, string password) {
            var r = await SendEmailAsync(emailto, username, password);
            return Json(r.Data);
        }

        private async Task<JsonResult> SendEmailAsync(string emailto, string username, string password)
        {
            string name = GetFullName();
            string email = User.Identity.GetUserName();
            var r = "0";
            var myMessage = new SendGridMessage();
            myMessage.AddTo(emailto);
            myMessage.From = new System.Net.Mail.MailAddress(
                                email, name);
            myMessage.Subject = "CHAX Access";
            myMessage.Text = name + " has given you an access to his/her account.<br><br>Username: " + username + "<br>Password: " + password + @"<br><br>Please click here to login.";
            myMessage.Html = name + " has given you an access to his/her account.<br><br>Username: " + username + "<br>Password: " + password + @"<br><br>Please click here to login.";

            // Create a Web transport for sending email.
            var transportWeb = new Web("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");

            // Send the email.
            if (transportWeb != null)
            {
                await transportWeb.DeliverAsync(myMessage);
                r = "1";
            }
            else
            {
                Trace.TraceError("Failed to create Web transport.");
                await Task.FromResult(0);
            }
            return Json(r);
        }
    }
}