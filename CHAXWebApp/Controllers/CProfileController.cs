﻿using CHAXWebApp.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CHAXWebApp.Models.ViewModels;
using System.Threading.Tasks;

namespace CHAXWebApp.Controllers
{
    public class CProfileController : Controller
    {

        // GET: CProfile
        public ActionResult CProfile()
        {
            return View();
        }

        public JsonResult GetCurrentUserId()
        {
            UserViewModel userVM = new UserViewModel();
            string currentUserId = User.Identity.GetUserId();
            using (var db = new ApplicationDbContext())
            {
                if (!(currentUserId == null))
                {
                    ApplicationUser currentUser = db.Users.FirstOrDefault(x => x.Id == currentUserId);
                    userVM.FirstName = currentUser.FirstName;
                    userVM.LastName = currentUser.LastName;
                    userVM.EmailAddress = currentUser.Email;
                    userVM.UserName = currentUser.UserName;
                }
            }
            return Json(userVM);
        }
    }
}