﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CHAXWebApp.Controllers
{
    public class SetupController : Controller
    {
        // GET: Setup
        [Authorize]
        [RequireHttps]
        public ActionResult Setup()
        {
            return View();
        }
    }
}