﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CHAXWebApp.Models.DBContexts;
using CHAXWebApp.Models.DBTables;
using CHAXWebApp.Models.ViewModels;
using Microsoft.AspNet.Identity;

namespace CHAXWebApp.Controllers
{
    public class ChecksController : Controller
    {
        // GET: Dashboard
        [Authorize]
        [RequireHttps]
        public ActionResult Checks()
        {
            return View();
        }

        public JsonResult GetPayee(int id)
        {
            var email = User.Identity.GetUserName();
            List<Payee> p = new List<Payee>();
            using (var db = new CHAX_Context())
            {
                p = db.Payee.Where(x => x.ID == id && x.UserEmail == email).ToList();
            }
            return Json(p);
        }

        public JsonResult GetPayer(int id)
        {
            var email = User.Identity.GetUserName();
            List<Payer> p = new List<Payer>();
            using (var db = new CHAX_Context())
            {
                p = db.Payer.Where(x => x.ID == id && x.UserEmail == email).ToList();
            }
            return Json(p);
        }

        public JsonResult GetBank(int id)
        {
            List<Bank> p = new List<Bank>();
            using (var db = new CHAX_Context())
            {
                p = db.Bank.Where(x => x.ID == id).ToList();
            }
            return Json(p);
        }

        public JsonResult GetAllPayer()
        {
            var email = User.Identity.GetUserName();
            List<PayerViewModel> p = new List<PayerViewModel>();
            using (var db = new CHAX_Context())
            {
                string que = @"select *, a.Address as PayerAddress from Payer a
                        left join Bank b
                        on a.BankID = b.ID
                        where a.BankID = b.ID and UserEmail = '" + email + "'";
                p = db.Database.SqlQuery<PayerViewModel>(que).ToList();
            }
            return Json(p, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllPayee()
        {
            var email = User.Identity.GetUserName();
            List<Payee> p = new List<Payee>();
            using (var db = new CHAX_Context())
            {
                p = db.Payee.Where(x => x.UserEmail == email).ToList();
            }
            return Json(p, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllBank()
        {
            List<Bank> p = new List<Bank>();
            using (var db = new CHAX_Context())
            {
                p = db.Bank.Where(x => x.ID > 1).ToList();
            }
            var jsonResult = Json(p, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public JsonResult GetSpecificBank(string routing)
        {
            List<Bank> p = new List<Bank>();
            using (var db = new CHAX_Context())
            {
                p = db.Bank.Where(x => x.RoutingNumber == routing).ToList();
            }
            return Json(p);
        }
    }
}