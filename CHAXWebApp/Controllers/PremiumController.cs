﻿using CHAXWebApp.Models;
using CHAXWebApp.Models.DBContexts;
using CHAXWebApp.Models.DBTables;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CHAXWebApp.Controllers
{
    public class PremiumController : Controller
    {
        // GET: Premium
        public ActionResult Premium()
        {
            return View();
        }

        public JsonResult UpgradeToPremium()
        {
            string r = "0";
            string email = User.Identity.GetUserName();
            ApplicationUser p;
            using (var db = new ApplicationDbContext())
            {
                p = db.Users.Where(x => x.UserName == email).FirstOrDefault();
            }
            if (p != null)
            {
                p.SubscriptionID = 1;

                using (var db = new ApplicationDbContext())
                {
                    try
                    {
                        db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        r = ex.Message.ToString();
                    }
                }

                r = p.SubscriptionID.ToString();
            }

            return Json(r);
        }

        public JsonResult GetSubscription() {
            string r = "";
            string email = User.Identity.GetUserName();
            using (var db = new ApplicationDbContext())
            {
                r = db.Users.Where(x => x.UserName == email).FirstOrDefault().SubscriptionID.ToString();
            }
            return Json(r);
        }
    }
}