﻿using CHAXWebApp.Models.DBContexts;
using CHAXWebApp.Models.DBTables;
using CHAXWebApp.Models.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CHAXWebApp.Controllers
{
    public class ReportsController : Controller
    {
        // GET: Reports
        [Authorize]
        [RequireHttps]
        public ActionResult Reports()
        {
            return View();
        }

        public JsonResult GetPrinted()
        {
            var email = User.Identity.GetUserName();
            int year = DateTime.Now.Date.Year;
            int[] series = new int[12];
            for (int i = 0; i < 12; i++)
            {
                string que = @"select count(ID) as TotalPrintedThisMonth from PrintedCheck where
                            IsDeleted = 0 and Date between '" + (year + "-" + (i + 1) + "-" + "01") + "' and '" + (year + "-" + (i + 1) + "-" + DateTime.DaysInMonth(year, (i + 1))) + "' and UserEmail = '" + email + "'";
                using (var db = new CHAX_Context())
                {
                    series[i] = db.Database.SqlQuery<int>(que).FirstOrDefault();
                }
            }
            return Json(series);
        }

        public JsonResult GetAmount()
        {
            var email = User.Identity.GetUserName();
            int year = DateTime.Now.Date.Year;
            decimal[] series = new decimal[12];
            for (int i = 0; i < 12; i++)
            {
                string que = @"select isnull(sum(cast(Amount as numeric(18,2))), 0) as TotalAmountThisMonth from PrintedCheck where
                            IsDeleted = 0 and Date between '" + (year + "-" + (i + 1) + "-" + "01") + "' and '" + (year + "-" + (i + 1) + "-" + DateTime.DaysInMonth(year, (i + 1))) + "' and UserEmail = '" + email + "'";
                using (var db = new CHAX_Context())
                {
                    series[i] = db.Database.SqlQuery<decimal>(que).FirstOrDefault();
                }
            }
            return Json(series);
        }

        public JsonResult GetRecentPayer()
        {
            var email = User.Identity.GetUserName();
            List<PrintedCheck> p = new List<PrintedCheck>();
            using (var db = new CHAX_Context())
            {
                p = db.PrintedCheck.Where(x => x.IsDeleted == false && x.UserEmail == email).OrderByDescending(x => x.Date).Take(10).ToList();
            }
            return Json(p);
        }

        public JsonResult GetAllPayer()
        {
            var email = User.Identity.GetUserName();
            List<PrintedCheck> p = new List<PrintedCheck>();
            using (var db = new CHAX_Context())
            {
                p = db.PrintedCheck.Where(x => x.IsDeleted == false && x.UserEmail == email).OrderByDescending(x => x.Date).ToList();
            }
            return Json(p, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRecentPayee()
        {
            var email = User.Identity.GetUserName();
            List<PrintedCheck> p = new List<PrintedCheck>();
            using (var db = new CHAX_Context())
            {
                p = db.PrintedCheck.Where(x => x.IsDeleted == false && x.UserEmail == email).OrderByDescending(x => x.Date).Take(10).ToList();
            }
            return Json(p);
        }

        public JsonResult GetAllPayee()
        {
            var email = User.Identity.GetUserName();
            List<PrintedCheck> p = new List<PrintedCheck>();
            using (var db = new CHAX_Context())
            {
                p = db.PrintedCheck.Where(x => x.IsDeleted == false && x.UserEmail == email).OrderByDescending(x => x.Date).ToList();
            }
            return Json(p, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetReportByMonth()
        {
            var email = User.Identity.GetUserName();
            List<ChecksViewModel> p = new List<ChecksViewModel>();
            using (var db = new CHAX_Context())
            {
                string que = @"SELECT DATEname(month, Date) AS Month, sum(cast(Amount as numeric(18,2))) AS TotalAmount, Count(ID) as TotalCount
                                FROM PrintedCheck
                                WHERE IsDeleted = 0" +
                                "and UserEmail = '" + email + "'" +
                                "GROUP BY DATEname(month, Date)";
                p = db.Database.SqlQuery<ChecksViewModel>(que).ToList();
            }
            return Json(p, JsonRequestBehavior.AllowGet);
        }
    }
}