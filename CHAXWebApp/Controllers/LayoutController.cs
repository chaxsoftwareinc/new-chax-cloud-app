﻿using CHAXWebApp.Models;
using CHAXWebApp.Models.DBContexts;
using CHAXWebApp.Models.DBTables;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CHAXWebApp.Controllers
{
    public class LayoutController : Controller
    {
        // GET: Layout
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetTotalCount()
        {
            var email = User.Identity.GetUserName();
            int str = 0;
            //string que = @"select COUNT(ID) from PrintedCheck where cast(DateCreated as date) = Convert(date, getdate()) and UserEmail = '" + email + "'";
            string que = @"select COUNT(ID) from PrintedCheck where UserEmail = '" + email + "' and IsPrinted = 0 and IsDeleted = 0 and IsGenerated = 1";
            using (var db = new CHAX_Context())
            {
                str = db.Database.SqlQuery<int>(que).FirstOrDefault();
            }
            return Json(str.ToString());
        }

        public JsonResult GetNotification()
        {
            var email = User.Identity.GetUserName();
            List<PrintedCheck> p = new List<PrintedCheck>();
            using (var db = new CHAX_Context())
            {
                p = db.Database.SqlQuery<PrintedCheck>("select * from PrintedCheck where IsPrinted = 0 and IsGenerated = 1 and UserEmail = '" + email + "' and IsDeleted = 0").ToList();
            }
            return Json(p);
        }

        public JsonResult GetUnprintedCheck(int id)
        {
            var email = User.Identity.GetUserName();
            List<PrintedCheck> p = new List<PrintedCheck>();
            using (var db = new CHAX_Context())
            {
                p = db.Database.SqlQuery<PrintedCheck>("select * from PrintedCheck where IsPrinted = 0 and IsDeleted = 0 and UserEmail = '" + email + "' and ID = " + id).ToList();
            }
            return Json(p);
        }

        public JsonResult GetSubscription()
        {
            var email = User.Identity.GetUserName();
            int subs = 0;
            using (var db = new ApplicationDbContext())
            {
                subs = db.Users.Where(x => x.UserName == email).FirstOrDefault().SubscriptionID;
            }
            return Json(subs);
        }
    }
}