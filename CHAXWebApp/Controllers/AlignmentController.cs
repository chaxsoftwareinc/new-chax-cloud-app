﻿using CHAXWebApp.Models;
using CHAXWebApp.Models.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CHAXWebApp.Controllers
{
    public class AlignmentController : Controller
    {
        // GET: Alignment
        [Authorize]
        [RequireHttps]
        public ActionResult Alignment()
        {
            return View();
        }

        public JsonResult SaveAlignment(float checkhor, float checkver, float micrhor, float micrver)
        {
            string r = "0";
            string email = User.Identity.GetUserName();
            ApplicationUser p;
            using (var db = new ApplicationDbContext())
            {
                p = db.Users.Where(x => x.UserName == email).FirstOrDefault();
            }
            if (p != null)
            {
                p.CheckHor = checkhor;
                p.CheckVer = checkver;
                p.MICRHor = micrhor;
                p.MICRVer = micrver;

                using (var db = new ApplicationDbContext())
                {
                    try
                    {
                        db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        r = "1";
                    }
                    catch (Exception ex)
                    {
                        r = ex.Message.ToString();
                    }
                }                
            }

            return Json(r);
        }

        public JsonResult GetAlignment()
        {
            AlignmentViewModel vm = new AlignmentViewModel();
            string email = User.Identity.GetUserName();
            ApplicationUser p;
            using (var db = new ApplicationDbContext())
            {
                p = db.Users.Where(x => x.UserName == email).FirstOrDefault();
            }
            if (p != null)
            {
                using (var db = new ApplicationDbContext())
                {
                    try
                    {
                        vm.CheckHor = p.CheckHor;
                        vm.CheckVer = p.CheckVer;
                        vm.MICRHor = p.MICRHor;
                        vm.MICRVer = p.MICRVer;
                    }
                    catch 
                    {

                    }
                }
            }

            return Json(vm, JsonRequestBehavior.AllowGet);
        }
    }
}