﻿using CHAXWebApp.Models.DBContexts;
using CHAXWebApp.Models.DBTables;
using CHAXWebApp.Models.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CHAXWebApp.Controllers
{
    public class PayerController : Controller
    {
        // GET: Payer
        [Authorize]
        [RequireHttps]
        public ActionResult Payer()
        {
            return View();
        }

        public JsonResult GetPayer(string name)
        {
            CHAX_Context db2 = new CHAX_Context();
            var fullname = db2.Payer.Where(x => x.Name == name).FirstOrDefault();
            return Json(fullname.Name);
        }

        public JsonResult GetAllPayer()
        {
            var email = User.Identity.GetUserName();
            using (var db = new CHAX_Context())
            {
                var payer = db.Payer.Where(x => x.UserEmail == email).ToList();
                return Json(payer, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetTopPayer()
        {
            var email = User.Identity.GetUserName();
            List<Payer> p = new List<Payer>();
            using (var db = new CHAX_Context())
            {
                p = db.Payer.Where(x => x.UserEmail == email).OrderByDescending(x => x.LastDateUsed).Take(20).ToList();
            }
            return Json(p);
        }

        public JsonResult GetAllBank()
        {
            List<Bank> p = new List<Bank>();
            using (var db = new CHAX_Context())
            {
                p = db.Bank.Where(x => x.ID > 1).ToList();
            }
            var jsonResult = Json(p, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public JsonResult GetSpecificPayer(int id)
        {
            var email = User.Identity.GetUserName();
            List<PayerViewModel> p = new List<PayerViewModel>();
            string que = @"select *, a.Address as PayerAddress from Payer a
                    left join Bank b
                    on a.BankID = b.ID
                    where a.BankID = b.ID
                    and a.ID = " + id + " and a.UserEmail = '" + email + "'";
            //string quenobank = "select* from Payer where ID = " + id + " and UserEmail = '" + email + "'";
            using (var db = new CHAX_Context())
            {
                p = db.Database.SqlQuery<PayerViewModel>(que).ToList();
            }
            return Json(p, JsonRequestBehavior.AllowGet);
        }

        private string ValidateNull(string str) {
            string xstr = str;
            if (str == null || str == "null") {
                xstr = "";
            }
            return xstr;
        }

        public JsonResult UpdatePayer(int id, string name, string line1, string line2, string city, string zip, string phone, string bankid, string accountnumber, string address, string emailadd)
        {
            var email = User.Identity.GetUserName();
            string r = "1";
            Payer p;
            using (var db = new CHAX_Context())
            {
                p = db.Payer.Where(x => x.BankAccount == accountnumber && x.UserEmail == email).FirstOrDefault<Payer>();
            }
            if (p != null)
            {
                p.Name = name;
                p.AddressLine1 = ValidateNull(line1);
                p.AddressLine2 = ValidateNull(line2);
                p.CityState = ValidateNull(city);
                p.ZipCode = ValidateNull(zip);
                p.PhoneNumber = phone;
                p.BankID = int.Parse(bankid);
                p.BankAccount = accountnumber;
                p.UserEmail = email;
                p.Address = address;
                p.EmailAddress = emailadd;
                using (var db = new CHAX_Context())
                {
                    try
                    {
                        db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        r = ex.Message.ToString();
                    }
                }
            }
            else
            {
                r = AddPayer(name, line1, line2, city, zip, phone, bankid, accountnumber, address, emailadd);
            }

            return Json(r);
        }

        private string AddPayer(string name, string line1, string line2, string city, string zip, string phone, string bankid, string accountnumber, string address, string emailadd)
        {
            var email = User.Identity.GetUserName();
            string r = "2";
            bankid = bankid == null || bankid == "" ? bankid = "0" : bankid;
            var p = new Payer
            {
                Name = name,
                AddressLine1 = ValidateNull(line1),
                AddressLine2 = ValidateNull(line2),
                CityState = ValidateNull(city),
                ZipCode = ValidateNull(zip),
                PhoneNumber = phone,
                BankID = int.Parse(bankid),
                LastDateUsed = DateTime.Now.Date,
                BankAccount = accountnumber,
                UserEmail = email,
                Address = address,
                EmailAddress = emailadd
            };

            using (var db = new CHAX_Context())
            {
                try
                {
                    db.Payer.Add(p);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    r = ex.Message.ToString();
                }
            }
            return r;
        }

        public JsonResult DeletePayer(int id)
        {
            var email = User.Identity.GetUserName();
            string r = "3";
            using (var db = new CHAX_Context())
            {
                try
                {
                    var singleRec = db.Payer.SingleOrDefault(x => x.ID == id && x.UserEmail == email);// object your want to delete
                    db.Payer.Remove(singleRec);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    r = ex.Message.ToString();
                }
            }
            return Json(r);
        }
    }
}