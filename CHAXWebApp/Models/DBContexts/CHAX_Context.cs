﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using CHAXWebApp.Models.DBTables;
using Microsoft.AspNet.Identity.EntityFramework;


namespace CHAXWebApp.Models.DBContexts
{
    public class CHAX_Context : DbContext
    {
        public CHAX_Context() : base("CHAXConnection")
        {

        }

        public DbSet<Payee> Payee { get; set; }
        public DbSet<Payer> Payer { get; set; }
        public DbSet<Bank> Bank { get; set; }
        public DbSet<PrintedCheck> PrintedCheck { get; set; }
        public DbSet<CheckLayout> CheckLayout { get; set; }
        public DbSet<AccountRelation> AccountRelation { get; set; }
        public DbSet<FormMessage> FormMessage { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}