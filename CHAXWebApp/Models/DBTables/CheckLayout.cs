﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAXWebApp.Models.DBTables
{
    public class CheckLayout
    {
        public int ID { get; set; }
        public string LayoutDescription { get; set; }
        public int LayoutCode { get; set; }
    }
}