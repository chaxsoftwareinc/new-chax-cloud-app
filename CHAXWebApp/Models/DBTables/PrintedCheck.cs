﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAXWebApp.Models.DBTables
{
    public class PrintedCheck
    {
        public int ID { get; set; }
        public string CheckNumber { get; set; }
        public string CheckLayout { get; set; }
        public string PayerName { get; set; }
        public string PayerAddress { get; set; }
        public string PayerAddressLine1 { get; set; }
        public string PayerAddressLine2 { get; set; }
        public string CityState { get; set; }
        public string ZipCode { get; set; }
        public string PayerPhone { get; set; }
        public string BankName { get; set; }
        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }
        public string Payee { get; set; }
        public DateTime Date { get; set; }
        public DateTime DateCreated { get; set; }
        public string Amount { get; set; }
        public string AmountInWords { get; set; }
        public string Memo { get; set; }
        public string PayeeName { get; set; }
        public bool IsMemorized { get; set; }
        public bool IsDeleted { get; set; }
        public string BankAddress { get; set; }
        public string BankCityState { get; set; }
        public int BatchCount  { get; set; }
        public string UserEmail { get; set; }
        public bool IsPrinted { get; set; }
        public string PayerEmail { get; set; }
        public string PayeePhone { get; set; }
        public bool IsGenerated { get; set; }
        public bool IsMarkedOk { get; set; }
        public bool IsMarkedBanned { get; set; }
    }
}