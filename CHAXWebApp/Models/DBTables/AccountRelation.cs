﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAXWebApp.Models.DBTables
{
    public class AccountRelation
    {
        public int ID { get; set; }
        public string UserEmail { get; set; }
        public string InvitedEmail { get; set; }
        public int Status { get; set; }
    }
}