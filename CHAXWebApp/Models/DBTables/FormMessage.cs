﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAXWebApp.Models.DBTables
{
    public class FormMessage
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string Message { get; set; }
    }
}