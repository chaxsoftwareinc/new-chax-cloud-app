﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAXWebApp.Models.DBTables
{
    public class Payee
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string CityState { get; set; }
        public string ZipCode { get; set; }
        public string Default { get; set; }
        public string RoutingNumber { get; set; }
        public string BankAccount { get; set; }
        public string SendingCompanyName { get; set; }
        public string DiscretionaryData { get; set; }
        public string ACHClassEntry { get; set; }
        public string EntryDescription { get; set; }
        public int CRoutingNumberCheckDigit { get; set; }
        public int BatchNumber { get; set; }
        public string DefaultACHClass { get; set; }
        public int ClientID { get; set; }
        public bool IsDefaultPayee { get; set; }
        public int CompanyID { get; set; }
        public DateTime? LastDateUsed { get; set; }
        public string UserEmail { get; set; }
        public string EmailAddress { get; set; }
        public string Memo { get; set; }
    }
}