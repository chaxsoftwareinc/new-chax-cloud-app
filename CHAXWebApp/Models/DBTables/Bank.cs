﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAXWebApp.Models.DBTables
{
    public class Bank
    {
        public int ID { get; set; }
        public string BankName { get; set; }
        public string Branch { get; set; }
        public string BankAddress { get; set; }
        public string BankCityStateZip { get; set; }
        public string RoutingNumber { get; set; }
        public string TransitSymbol { get; set; }
        public string FileDate { get; set; }
        public string Type { get; set; }
        public string BranchCode { get; set; }
    }
}