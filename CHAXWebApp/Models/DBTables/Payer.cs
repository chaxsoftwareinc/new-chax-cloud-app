﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAXWebApp.Models.DBTables
{
    public class Payer
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string BankAccount { get; set; }
        public int BankID { get; set; }
        public string TransitNumber { get; set; }
        public DateTime? LastDate { get; set; }
        public string LastCheck { get; set; }
        public double LastAmount { get; set; }
        public string FrequencyType { get; set; }
        public int FrequencyDays { get; set; }
        public string AccountType { get; set; }
        public string ACHRoutingNumber { get; set; }
        public string ACHAccountNumber { get; set; }
        public string CameFromACH { get; set; }
        public int ClientID { get; set; }
        public string CityState { get; set; }
        public int CompanyID { get; set; } 
        public DateTime? LastDateUsed { get; set; }
        public string UserEmail { get; set; }
        public string EmailAddress { get; set; }
    }
}