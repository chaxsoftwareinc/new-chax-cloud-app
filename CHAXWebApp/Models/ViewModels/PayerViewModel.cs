﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAXWebApp.Models.ViewModels
{
    public class PayerViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string BankAccount { get; set; }
        public string CityState { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string BankCityStateZip { get; set; }
        public int BankID { get; set; }
        public string RoutingNumber { get; set; }
        public DateTime? LastDateUsed { get; set; }
        public string PayerAddress { get; set; }
        public string EmailAddress { get; set; }
    }
}