﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CHAXWebApp.Models.ViewModels
{
    public class CheckInfoViewModel
    {
        public int ID { get; set; }
        public string CheckLayout { get; set; }
        public string CheckNumber { get; set; }
        public string PayerName { get; set; }
        public string PayerAddress { get; set; }
        public string PayerAddressLine1 { get; set; }
        public string PayerAddressLine2 { get; set; }
        public string CityState { get; set; }
        public string ZipCode { get; set; }
        public string PayerPhone { get; set; }
        public string BankName { get; set; }
        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }
        public string Payee { get; set; }
        public string Date { get; set; }
        public string Amount { get; set; }
        public string AmountInWords { get; set; }
        public string Memo { get; set; }
        public string PayeeName { get; set; }
        public string PayeeAddress { get; set; }
        public string ProcessType { get; set; }
        public string BankAddress { get; set; }
        public string BankCityStateZip { get; set; }
        public int BatchCount { get; set; }
        public string PayerEmail { get; set; }
        public string PayeePhone { get; set; }
        public string Test { get; set; }
        public float CheckHor { get; set; }
        public float CheckVer { get; set; }
        public float MICRHor { get; set; }
        public float MICRVer { get; set; }
    }
}