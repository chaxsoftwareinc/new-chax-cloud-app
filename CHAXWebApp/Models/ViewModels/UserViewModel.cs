﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAXWebApp.Models.ViewModels
{
    public class UserViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
    }
}
