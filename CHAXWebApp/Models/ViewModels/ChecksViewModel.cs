﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAXWebApp.Models.ViewModels
{
    public class ChecksViewModel
    {
        public string Month { get; set; }
        public decimal TotalAmount { get; set; }
        public int TotalCount { get; set; }
    }
}