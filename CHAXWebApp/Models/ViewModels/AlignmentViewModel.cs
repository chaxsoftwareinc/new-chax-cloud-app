﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAXWebApp.Models.ViewModels
{
    public class AlignmentViewModel
    {
        public float CheckHor { get; set; }
        public float CheckVer { get; set; }
        public float MICRHor { get; set; }
        public float MICRVer { get; set; }
    }
}