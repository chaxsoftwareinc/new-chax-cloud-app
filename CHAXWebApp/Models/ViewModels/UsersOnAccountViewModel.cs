﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CHAXWebApp.Models.ViewModels
{
    public class UsersOnAccountViewModel
    {
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserAddress { get; set; }
        public string UserEmail { get; set; }
        public string UserName { get; set; }
        public int Status { get; set; }
    }
}