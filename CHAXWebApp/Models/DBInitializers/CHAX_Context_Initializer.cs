﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using CHAXWebApp.Models.DBContexts;
using CHAXWebApp.Models.DBTables;

namespace CHAXWebApp.Models.DBInitializers
{
    public class CHAX_Context_Initializer : DropCreateDatabaseIfModelChanges<CHAX_Context>
    {
        protected override void Seed(CHAX_Context context)
        {
            //payee
            var payee = new List<Payee>() {
                new Payee
                {
                    Name = "Venny Martinez",
                    AddressLine1 = "The White House",
                    AddressLine2 = "1600 Pennsylvania Avenue",
                    CityState = "Washington, DC",
                    ZipCode = "20500",
                    PhoneNumber = "34343434",
                    LastDateUsed = DateTime.Now.Date,
                    UserEmail = "vennymartinez.dev@gmail.com"
                }
            };

            foreach (var item in payee)
            {
                context.Payee.Add(item);
            }

            context.SaveChanges();

            //payer
            var payer = new List<Payer>() {
                new Payer
                {
                    Name = "Venny Martinez",
                    Address = "Alabang",
                    PhoneNumber = "34343434",
                    BankAccount = "233253252118",
                    BankID = 1,
                    AddressLine1 = "The White House",
                    AddressLine2 = "1600 Pennsylvania Avenue",
                    CityState = "Washington, DC",
                    ZipCode = "20500",
                    LastDateUsed = DateTime.Now.Date,
                    UserEmail = "vennymartinez.dev@gmail.com"
                }
            };

            foreach (var item in payer)
            {
                context.Payer.Add(item);
            }

            context.SaveChanges();

            //bank
            var bank = new List<Bank>() {
                new Bank {
                    ID = 0,
                    BankName = "",
                    BankAddress = "",
                    BankCityStateZip = "",
                    RoutingNumber = ""
                }
            };

            foreach (var item in bank)
            {
                context.Bank.Add(item);
            }

            context.SaveChanges();

            //layout
            var layout = new List<CheckLayout>() {
                new CheckLayout{LayoutDescription = "Top",LayoutCode = 1},
                new CheckLayout{LayoutDescription = "Middle",LayoutCode = 2},
                new CheckLayout{LayoutDescription = "Bottom",LayoutCode = 3},
                new CheckLayout{LayoutDescription = "Batch",LayoutCode = 4}
            };

            foreach (var item in layout)
            {
                context.CheckLayout.Add(item);
            }

            context.SaveChanges();
        }
    }
}